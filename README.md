# Quorum Strategic Investments Landing Page Repository

This repository contains all the code and resources for the landing page of Quorum Strategic Investments (QSI), a specialised division of Quorum IO Limited focusing on property reselling and realty advisory. 

## Overview
In 2023, a client asked us to assist them with financial forecasts and sales pitch for their hotel property sale. After the initial success, my partner and I decided to establish a separate division to provide services of real estate investment advisory. The landing page has been designed to highlight our services, team, and successful projects, thereby providing an evocative and comprehensive look into our functioning and offerings. I'm also looking to experiment with SSG and programmatic SEO a bit. 

## Installation
To clone and run this application, you'll need Git and Node.js (which comes with npm) installed on your computer. From your command line:

```bash
# Run the app
$ npm run dev
```

## Production
Visit [investments.quorum.ltd.](https://investments.quorum.ltd/)

## File Structure
This repository is structured as follows:

- `src/`: This directory contains all the source code for the application.
  - `components/`: This directory includes all React components.
    - `atoms/`: Contains atomic elements used commonly.
    - `common/`: Components used in multiple contexts.
    - `home/`: Components mostly used by the home page.
    - `icon/`: SCG icons as functional components.
    - `illustrations/`: Other SCG images as as functional components.
    - `properties/`: Components mostly used by the properties pages.
    - `services/`:  Components mostly used by the services pages.
  - `misc/`: Other assets not used in the app directly.
  - `pages/`: Pages entry points (directory-based routing)
  - `server/`: TRPC endpoints to fetch data.
  - `stores/`: Shared Zustand stores.
  - `styles/`: Explicit CSS files.
  - `types/`: Commonly shared type declarations.
  - `utils/`: Commonly shared utility functions.
- `public/`: Static files.
- `package.json`: This file contains metadata about the application and its dependencies.

## How to Use

Once the server is up and running, simply open your web browser and navigate to `http://localhost:3000/` (or whatever port you specified).

## License

Property of Quorum IO Limited, all rights reserved. The public repo is for showcase purposes only.

## Contact

For any queries, feel free to reach us at our [official email](mailto:admin@quorum.ltd) or to me directly at [official email](mailto:anton@quorum.ltd).
