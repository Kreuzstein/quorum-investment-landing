/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import { StaticImageData } from "next/image";
import { Block } from "./content";
import { ContactName } from "~/components/home/Contact";

export type FilterOption = {
    label: string;
    value: string;
};

export type Filter = {
    label: string;
    options: FilterOption[];
    selectedOptions: string[];
    type: "select" | "multi";
} | {
    label: string;
    unit: string | undefined;
    type: "range";
    from: number | undefined;
    to: number | undefined;
};

const propertyFiltersConst = {
    assetClass: {
        label: "Asset Class",
        options: [
            { label: "Hospitality", value: "hospitality" },
            { label: "Commercial", value: "commercial" },
            { label: "Residential", value: "residential" },
            { label: "Digital", value: "digital" },
        ],
        selectedOptions: [],
        type: "multi",
    },
    deal: {
        label: "Deal Structure",
        options: [
            { label: "Sale / Freehold", value: "sale" },
            { label: "Syndicated Investment", value: "syndicate" },
            { label: "Stake / Partial Ownership", value: "stake" },
            { label: "Growth Investment", value: "vc" },
        ],
        selectedOptions: [],
        type: "multi",
    },
    // TODO make a price selector
    price: {
        label: "Price Range",
        options: [
            { label: "Under USD 200,000", value: "under-200K" },
            { label: "USD 200,000 to USD 1,000,000", value: "200K-to-1M" },
            { label: "USD 1,000,000 to USD 5,000,000", value: "1M-to-5M" },
            { label: "USD 5,000,000 to USD 10,000,000", value: "5M-to-10M" },
            { label: "Above USD 10,000,000", value: "above-10M" },
        ],
        selectedOptions: [],
        type: "multi",
    },
    residency: {
        label: "Residency Eligibility",
        options: [
            { label: "EU Permanent Residence", value: "eu-pr" },
            { label: "EU Citizenship", value: "eu-passport" },
        ],
        selectedOptions: [],
        type: "multi",
    },
} as const;

export const propertyFilters = propertyFiltersConst as unknown as Record<string, Filter>;

export type PropertyInfo = {
    slug: string;
    pageTitle: string;
    propTitle: string;
    tagline: string;
    addressShort: {
        flag?: string;
        location: string;
    };
    addressLong: string;
    coords: {
        x: number;
        y: number;
    };
    heroHighlights: string[];
    descShort: string;
    descLong: string;
    featuredImage: string | StaticImageData;
};

const dealValues = propertyFiltersConst.deal.options.map(o => o.value);
const residencyValues = propertyFiltersConst.residency.options.map(o => o.value);
const priceValues = propertyFiltersConst.price.options.map(o => o.value);
const assetClassValues = propertyFiltersConst.assetClass.options.map(o => o.value);

export type Opportunity = {
    title: string;
    details?: string[];
    bullets?: {
        label: string;
        value: string;
    }[];
    priceLabel: string;
    priceLabelLong?: string;
    priceAmount: string;
    tags?: string[];
    dealType: typeof dealValues[number];
    residenceOption: typeof residencyValues[number];
    priceBracket: typeof priceValues[number];
    blurb: string;
};

const ctaMeta = {
    propTitle: "House of Benjamins",
    addressLong: "12 Krišjāņa Barona, Rīga",
    contact: "oscar",
};

const asset = {
    assetClass: "Hotel", 
    tags: ["HOTEL", "CASINO", "EU RESIDENCY"],
    listingBadge: "For Sale",
    highlightsShort: "60 beds | furniture included",
};

export type PropertyListingFull = {
    content: Block[];
    assetClassLabel: string | undefined; // Used for desc not filters
    assetClass: typeof assetClassValues[number][];
    tags: string[];
    listingBadge: string;
    highlightsShort: string;
    contact: ContactName;
    opportunities: Opportunity[];
    images: (StaticImageData | string)[];
} & PropertyInfo;

export type PropertyCta = Pick<PropertyListingFull, "propTitle" | "addressLong" | "contact" | "opportunities">;
export type PropertyCardLong = Pick<PropertyListingFull,
    "slug" | "propTitle" | "assetClass" | "opportunities" | "featuredImage" | "assetClassLabel">;
export type PropertyCardShort = Omit<PropertyCardLong, "assetClass">;
export type PropertyHero = Pick<
    PropertyListingFull,
    | "propTitle"
    | "tagline"
    | "addressLong"
    | "heroHighlights"
    | "featuredImage"
>;
