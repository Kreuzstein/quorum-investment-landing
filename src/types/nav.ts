/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import { type Url } from "next/dist/shared/lib/router/router";

export const navGroupLabels = {
    props: "List with us",
    invest: "Invest",
    // content: "Insight",
    // about: "About QSI",
} as const;

export type NavGroup = (typeof navGroupLabels)[keyof typeof navGroupLabels];

export type NavItem = {
    group: NavGroup;
    title: string;
    desc: string;
    href: Url;
} & ({
    type: "regular";
} | { type: "featured"; icon: React.FC; gradientClasses?: string; });
