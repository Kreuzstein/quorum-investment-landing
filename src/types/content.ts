import { type StaticImageData } from "next/image";
import { PropertyInfo } from "./properties";

export type Block = {
    body: string; // block's body in markdown
    title: string;
    tag: string;
} & (
        | {
            contentType: "opportunity";
            priceLabel: string;
            priceValue: string;
        }
        | { contentType: "about" }
        | { contentType: "generic" }
    );

export type AboutMeta = Pick<PropertyInfo, "propTitle" | "heroHighlights" | "featuredImage">;

export type IllustratedBlockContent = {
    tag?: string;
    illustration: React.FC;
    header: string;
    subtitle: string;
    body: string;
} & ({
    cta: true;
    ctaHref: string;
    ctaLabel: string;
} | {
    cta: false;
});

export type LandingPageHero = ({
    segmentedTitle: true;
    title: {
        line: string;
        highlighted?: boolean;
    }[]
} | {
    segmentedTitle?: false;
    title: string;
}) & ({
    cta: true;
    ctaText: string;
    ctaHref: string;
} | {
    cta?: false
}) & {
    heroHighlights: {
        line: string;
        highlighted?: boolean;
    }[];
    featuredImage: string | StaticImageData;
    illustration?: React.FC;
    waveSub?: React.FC;
    hideWave?: boolean;
};

export type FilterPageHero = {
    title: string
} & Pick<LandingPageHero, "featuredImage" | "hideWave" | "waveSub">;