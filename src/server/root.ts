import { router } from "./trpc";
import { helpersRouter } from "./routers/helpers";
import { contentRouter } from "./routers/content";
import { propertiesRouter } from "./routers/properties";

export const appRouter = router({
    helpers: helpersRouter,
    content: contentRouter,
    properties: propertiesRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
