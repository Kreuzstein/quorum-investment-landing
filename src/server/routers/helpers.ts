import { z } from 'zod';
import { publicProcedure, router } from '../trpc';
import { env } from "~/env.mjs";

export const helpersRouter = router({
    hello: publicProcedure
        .input(
            z.object({
                text: z.string(),
            }),
        )
        .query((opts) => {
            return {
                greeting: `hello ${opts.input.text}`,
            };
        }),
    getMapStyle: publicProcedure
        .input(
            z.object({
                styleId: z.string(),
            }),
        )
        .query(async ({ input }) => {
            const res = await fetch(`https://api.maptiler.com/maps/${input.styleId}/style.json?key=${env.MAPTILER_API}`);
            return (await res.json());
        }),
});

// export type definition of API
export type HelpersRouter = typeof helpersRouter;
