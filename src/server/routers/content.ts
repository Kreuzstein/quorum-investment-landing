import { z } from 'zod';
import { publicProcedure, router } from '../trpc';
import { env } from "~/env.mjs";

import { AboutMeta, type Block } from '~/types/content';
import featuredImage from "~/../public/hero.jpg";

// TODO make it into actual CMS request

const body0 = `Welcome to the website of Quorum Strategic Investments, a department of Quorum IO Limited (referred to herein as "Quorum," "QSI," "we," "us," or "our"). Quorum Strategic investments specialises in listing and facilitating asset sales, primarily in the field of real estate, and provides opportunities in holding/managing company share investments and residencies schemes.

Please read these Terms and Conditions ("Terms") carefully before using the website (the "Service") operated by Quorum Strategic Investments. Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users, and others who wish to access or use the Service.

By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.

### 1. Services 
Quorum Strategic Investments provides an online platform through which users can browse and find investments opportunities. We act as an intermediary for the sale and management of assets, particularly property and holdings in various companies.

### 2. Eligibility 
The Service is intended solely for persons who are 18 or older. Any access to or use of the Service by anyone under 18 is expressly prohibited. By accessing or using the Service, you represent and warrant that you are 18 or older.

### 3. Use of Service 
Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is for your personal and non-commercial use only.

### 4. Accounts 
When you create an account with us, you guarantee that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.

### 5. Intellectual Property 
The Service and its original content (excluding content provided by users), features, and functionality are and will remain the exclusive property of Quorum Strategic investments and its licencors.

### 6. Links to Other Web Sites 
Our Service may contain links to third-party web sites or services that are not owned or controlled by Quorum Strategic investments. 

Quorum Strategic investments has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third-party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.
  
### 7. Termination 
We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, at our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.

### 8. Indemnification 
You agree to defend, indemnify, and hold harmless Quorum Strategic investments and its licensee and licencors, and their employees, contractors, agents, officers, and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, or b) a breach of these Terms.

### 9. Limitation of Liability 
In no event shall Quorum Strategic investments, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential, or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from:

- (i) your access to or use of or inability to access or use the Service; 
- (ii) any conduct or content of any third party on the Service; 
- (iii) any content obtained from the Service; 
- and (iv) unauthorised access, use, or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence), or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.

### 10. Disclaimer 
Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and “AS AVAILABLE” basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement, or course of performance. 

Quorum Strategic Investments, its subsidiaries, affiliates, and its licencors do not warrant that 
- a) the Service will function uninterrupted, secure, or available at any particular time or location; 
- b) any errors or defects will be corrected; 
- c) the Service is free of viruses or other harmful components; 
- or d) the results of using the Service will meet your requirements.

### 11. Governing Law 
These Terms shall be governed and construed in accordance with the laws of the United Kingdom of Great Britain and Northern Ireland, without regard to its conflict of law provisions. Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect.

### 12. Changes to Terms 
We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material, we will provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.

By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.

Contact Us If you have any questions about these Terms, please contact us.
`;
const body1 = `**Privacy Policy for Quorum Strategic Investment**

At Quorum Strategic Investment, a department of Quorum IO Limited ("Quorum," "QSI," "we," "us," or "our"), we are committed to protecting your privacy. This Privacy Policy explains how we collect, store, use, and disclose your personal information when you use our website and services (the "Service"). We understand that by using our Service, you are placing your trust in us to handle your personal information with care and in accordance with applicable laws.

This policy is designed to comply with the UK General Data Protection Regulation (UK GDPR) and the Data Protection Act 2018.

### **1. Information We Collect** 
We may collect various types of information in connection with the Service, including:

- **Personal Identification Information**: Name, email address, phone number, and other contact details.
- **Financial Information**: Bank account details or other payment information for transactions conducted on our platform.
- **Usage Information**: Information about how you use our Service and interact with us.
- **Technical Information**: IP address, browser type, device information, and other technical details about your visit to our site.

### **2. How We Use Your Information** 
We use your personal information to:

- Provide, operate, and maintain our Service.
- Process transactions and send related information, such as confirmations and invoices.
- Communicate with you, either directly or through one of our partners, including for customer service, to provide you with updates and other information related to the Service, and for marketing and promotional purposes.
- Understand and analyse how you use our Service to improve and enhance your experience.
- Detect and prevent fraud and other unauthorised or illegal activities.

### **3. How We Share Your Information** 
We may share the information we collect in the following circumstances:

- With service providers and partners who assist us in operating the Service, conducting our business, or serving our users, so long as those parties agree to keep this information confidential.
- For compliance purposes, including when required by law, legal process, or governmental request.
- In the context of a merger, acquisition, or asset sale, your personal information may be transferred.

### **4. Data Retention** 
We will retain your personal information only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use your information to the extent necessary to comply with our legal obligations, resolve disputes, and enforce our policies.

### **5. Your Data Protection Rights** 
Under UK GDPR, you have rights that enable you to access, correct, update, or request deletion of your personal information. You also have the right to object to the processing of your personal information, to ask for the restriction of processing, and to request portability of your personal information.

### **6. Security of Your Information** 
We take the security of your personal information seriously and implement a variety of security measures to maintain the safety of your personal information. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure, and we cannot guarantee its absolute security.

### **7. Cookies and Tracking Technologies** 
We use cookies and similar tracking technologies to track activity on our Service and store certain information. Tracking technologies employed are beacons, tags, and scripts to collect and track information and to improve and analyse our Service.

### **8. Links to Other Websites** 
Our Service may contain links to other websites that are not operated by us. If you click on a third-party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.

### **9. Changes to This Privacy Policy** 
We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page and updating the "effective date" at the top of this Privacy Policy.

### **10. Contact Us** 
If you have any questions or concerns about our Privacy Policy or the practices of this site, please contact us.

Your continued use of our Service will be regarded as acceptance of our practices around privacy and personal information.
`

export const contentRouter = router({
    getAboutPageBySlug: publicProcedure
        .input(z.string())
        .query(({ input }) => {
            const defaultBlock: Block = {
                body: "",
                title: "Page Not Found",
                tag: "About QSI",
                contentType: "about",
            };
            const defaultsMeta: AboutMeta = {
                propTitle: "Page Not Found",
                heroHighlights: [],
                featuredImage,
            };

            let block = defaultBlock;
            let meta = defaultsMeta;

            if (input === "terms-and-conditions") {
                block = {
                    body: body0,
                    title: "Quorum Strategic investments Website Terms and Conditions",
                    tag: "About QSI",
                    contentType: "about",
                };
                meta = {
                    propTitle: "Terms & Conditions",
                    heroHighlights: ["Updated: 13 Feb 2024"],
                    featuredImage,
                };
            }

            if (input === "privacy-policy") {
                block = {
                    body: body1,
                    title: "Quorum Strategic investments Website Privacy Policy",
                    tag: "About QSI",
                    contentType: "about",
                };
                meta = {
                    propTitle: "Privacy Policy",
                    heroHighlights: ["Updated: 13 Feb 2024"],
                    featuredImage,
                };
            }

            return { block, meta };
        }),
});

export type HelpersRouter = typeof contentRouter;
