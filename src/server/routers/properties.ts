import { z } from 'zod';
import { publicProcedure, router } from '../trpc';
import { env } from "~/env.mjs";

import { PropertyListingFull } from '~/types/properties';
import { Block } from '~/types/content';

// House of Benjamin imgs
import featuredImageHouseOfBen from "~/../public/dtf5836459591.jpg";
import img1 from "~/../public/album/1.jpg";
import img2 from "~/../public/album/2.jpg";
import img3 from "~/../public/album/3.jpg";
import img4 from "~/../public/album/4.jpg";
import img5 from "~/../public/album/5.jpg";
import img6 from "~/../public/album/6.jpg";
import img7 from "~/../public/album/7.jpg";
import img8 from "~/../public/album/8.jpg";
import img9 from "~/../public/album/9.jpg";
import imgA from "~/../public/album/a.jpg";
import imgB from "~/../public/album/b.jpg";
import imgC from "~/../public/album/c.jpg";
import imgD from "~/../public/album/d.jpg";
import imgE from "~/../public/album/e.jpg";
import imgF from "~/../public/album/f.jpg";
import img10 from "~/../public/album/10.jpg";
import img11 from "~/../public/album/11.jpg";
import img12 from "~/../public/album/12.jpg";
import img13 from "~/../public/album/13.jpg";

const body0 = `
Nestled within the vibrant heart of Riga, the storied "House of Benjamins" graces the esteemed address of 12 Kr. Barona street, presenting an exclusive opportunity to acquire a property magnificently poised with panoramic vistas of the adjoining central park. This coveted location ensures a continuous engagement with the tranquillity of lush greenery amidst the bustling urban environment.

The majestic "House of Benjamins," constructed with grandeur in **1885** and subsequently re-imagined in 1930, underwent a meticulous restoration in 2006, shaping its transformation into the illustrious "Europa Royale" hotel, blending historical allure with contemporary comforts.

Spanning a substantial total area of **4,695 sq. m** over several floors, the property is securely nested within a private land plot of **2,346 sq. m**, guaranteeing an expanse of personal space and seclusion. The hotel facility boasts an array of 60 elegantly appointed hotel rooms, each radiating a sense of luxury and relaxation, complemented by an array of balconies and terraces that offer serene views and areas of repose, facing the opulent central park.

Serviceable amenities within this grand establishment include a fine dining restaurant, versatile banquet halls poised to host a variety of prestigious events, and a discreet casino located in the basement, delivering an array of entertainment options for guests and visitors alike.

Presently, the property stands unencumbered and not subject to any leasing agreements, offering immediate entree and operational autonomy to the discerning acquirer. This valuable asset is poised for vast potential, readily awaiting a visionary to harness its unrestrained possibilities.

### Additional Information:

#### Property Proposition: 
* Historical Hotel and Leisure Facility

#### Features of Note:
* Magnificent park-side setting with several outdoor spaces
* Unobstructed panoramic park views
* Architectural legacy coupled with modern renovations
* Comprehensive accommodation facilities
* Convenient, exclusive central city location

The "House of Benjamins," with its unrivalled position and comprehensive features, is unequivocally a trophy asset, promising a lucrative venture for those with an appreciation of heritage, luxury, and a strategic sense of real estate investment.
`;
const block0: Block = {
    body: body0,
    title: "Commercial Asset For Sale | Freehold",
    contentType: "opportunity",
    tag: "Sale Opportunity",
    priceLabel: "Offers are invited in the region of",
    priceValue: "€ 7,000,000",
};
const body1 = `
An unparalleled opportunity beckons for investors pursuing a prestigious European residency through a Golden Visa program, augmented by the prospect of consistent financial returns. The esteemed "House of Benjamins" at 12 Kr. Barona street affords investors the chance to partake in an impeccable operational stake in company managing the hotel, situated within the heart of an exclusive central park area.

This remarkable asset not only delivers investors a substantial foothold in the European hotel market but also serves as a gateway to acquiring Latvian residency. Blessed with the principles of jus soli, Latvia offers residency without the prerequisite of habitual presence, representing a significant advantage for global investors.

Investment into the hotel's managing entity is an attractive proposition, promising an anticipated **annual return on investment within the 5% range**, complementing the intrinsic benefits of **EU residency for all investors**.

### Investment Highlights:
- Ownership stake in the "House of Benjamins," a historically significant building renovated to a luxurious "Europa Royale" hotel
- Guaranteed duty to manage the facility with 60 hotel rooms, restaurant, banquet halls, and inserted casino
- Prospective annual return on investment (ROI) intended to be approximately 5%
- Average Occupancy Rate High and Low: 32% to 68%
- Investor Equity Stake: Ranges from 0.5% to 100% based on investment size
- Entitlement to Latvian (EU) residency, with no mandatory habitation requisite
- Intrinsic value appreciation of a landmark property in a coveted location
- Total Asset Valuation: €7,000,000
- Current Market Capitalization: €10,000,000
- Projected Intrinsic Value Growth: 3% per annum
- Share Transfer Terms: Shares can be transferred or sold after a holding period of 5 years

### Exclusive Offer for Astute Investors:
- **Limited-time inducement**: Invest before the end of quarter, and benefit from premium terms
- Early Bird Discount: A reduction of 2% on the initial investment stake
- Priority Booking Privileges: First choice of hotel room bookings for personal stays, subject to availability and advanced reservation requirements
- Exclusive Access: Private tours of the "House of Benjamins" and personal meetings with management to discuss strategic insights and future developments
- Investor Recognition: Your name or company acknowledged in a dedicated 'Investors' section on hotel's website, correspondence and literature
- Additional Bonus: After the first year of successful operation, an extra 0.5% of net profit will be distributed to early investors

#### Investment Structure:

- Investors will acquire equity shares in the property-holding entity, granting them an ownership stake proportional to their investment
- The investment will be locked for a minimum period of 5 years to allow for growth and stabilisation of the asset's revenue-generating capabilities
- Performance of the investment will be reviewed annually, with comprehensive financial reports provided to all stakeholders

### Exit Strategies:
- Share Resale: After the minimum holding period, investors may resell their shares either on the private market or back to the company, depending on the terms of shareholder agreement and available buy-back programme
- Buy-Back Option: The company may offer a buy-back option after 5 years at a pre-agreed formula or current market value, whichever is higher
- Asset Liquidation: In the event of a strategic liquidation, investors will receive priority compensation proportional to their shareholding before any other financial considerations
- Merger or Acquisition: Should an M&A opportunity arise that is beneficial to the shareholders and aligns with the long-term goals of the "House of Benjamins," investors will have the option to vote and potentially benefit from any premium valuation.
- Internal Portfolio Sale: **During the mandatory holding period**, should an investor wish to exit their position, they may sell their shares at face value plus a fixed premium fee to another pre-qualified investor within the QSI portfolio who is interested in participating in the Latvian residency by investment scheme. This internal marketplace facilitates a smoother and potentially quicker transfer of ownership while offering a predefined exit premium to the selling investor. 

#### The investment affords immediate qualification for the Latvian residency by investment programme.

This the golden ticket for discerning investors who place a premium on the fusion of luxury real estate with strategic immigration benefits. The House of Benjamins offers a rare admixture of historical significance, modern luxury accommodation, and a straightforward pathway to European residency, all underscored by a resilience of capital value and a promising income forecast.
`;
const block1: Block = {
    body: body1,
    title: "Invest into the Managing Company",
    contentType: "opportunity",
    tag: "Investment Opportunity",
    priceLabel: "No investment ceiling. We welcome all offers starting at",
    priceValue: "€ 80,000",
};
const data: PropertyListingFull[] = [
    {
        slug: "house-of-benjamins",
        pageTitle: "House of Benjamins Hotel Opportunity | For Sale | Riga, Latvia",
        propTitle: "House of Benjamins",
        tagline: "Baptism Of Brilliance",
        addressShort: {
            flag: "🇱🇻",
            location: "12 Kr. Barona, Riga",
        },
        addressLong: "12 Krišjāņa Barona, Rīga",
        coords: { x: 24.120383, y: 56.950531 },
        heroHighlights: [
            "60 hotel rooms, restaurant, banquet halls, casino.",
            "Furniture & fixtures included.",
        ],
        descShort:
            "Buy Hotel Asset | Invest in Managing Company | Get EU Residency",
        descLong:
            "House of Benjamins Hotel, Riga: Ace Opportunity with Quorum Strategic Investments",
        featuredImage: featuredImageHouseOfBen,
        images: [
            img1,
            img2,
            img3,
            img4,
            img5,
            img6,
            img7,
            img8,
            img9,
            imgA,
            imgB,
            imgC,
            imgD,
            imgE,
            imgF,
            img10,
            img11,
            img12,
            img13,
        ],
        content: [block0, block1],
        assetClassLabel: "Hotel",
        tags: ["HOTEL", "CASINO", "EU RESIDENCY"],
        listingBadge: "For Sale",
        highlightsShort: "60 beds | furniture included",
        contact: "oscar",
        opportunities: [
            {
                title: "Sale opportunity",
                bullets: [
                    {
                        label: "Total area",
                        value: "4,695 m²",
                    },
                ],
                priceLabel: "Asking price",
                priceAmount: "€7,000,000",
                dealType: "sale",
                residenceOption: "eu-pr",
                priceBracket: "5M-to-10M",
                blurb: "Fully furnished, elegant hotel in the heart of Rigs, with 60 luxury rooms, fine dining, casino, and event spaces, overlooking a lush park.",
            },
            {
                title: "Investment opportunity",
                details: ["Stake in the managing company"],
                bullets: [
                    {
                        label: "Returns range",
                        value: "5% ROI",
                    },
                ],
                priceLabel: "Starting at",
                priceAmount: "€80,000",
                tags: ["EU Residency"],
                dealType: "stake",
                residenceOption: "eu-pr",
                priceBracket: "under-200K",
                blurb: `Invest in "House of Benjamins", a historical Riga hotel offering a stake with a 5% ROI & EU residency. Limited slots, premium early terms.`,
                // blurb:""
            },
        ],
        assetClass: ["hospitality", "commercial"],
    }
]

export const propertiesRouter = router({
    getPropertyList: publicProcedure
        .input(z.object({
            filters: z.optional(z.record(
                z.string(),
                z.array(z.string())
            )),
            page: z.optional(z.number()),
        }))
        .query(({ input }) => {
            const { filters, page } = input;
            return data.filter(property => {
                if (!filters) return true;
                if (filters.assetClass && filters.assetClass.length) {
                    let match = false;
                    for (const value of property.assetClass) {
                        if (filters.assetClass.includes(value)) match = true;
                    }
                    if (!match) return false;
                }
                if (filters.deal && filters.deal.length) {
                    let match = false;
                    for (const { dealType } of property.opportunities) {
                        if (filters.deal.includes(dealType)) match = true;
                    }
                    if (!match) return false;
                }
                if (filters.price && filters.price.length) {
                    let match = false;
                    for (const { priceBracket } of property.opportunities) {
                        if (filters.price.includes(priceBracket)) match = true;
                    }
                    if (!match) return false;
                }
                if (filters.residency && filters.residency.length) {
                    let match = false;
                    for (const { residenceOption } of property.opportunities) {
                        if (filters.residency.includes(residenceOption)) match = true;
                    }
                    if (!match) return false;
                }
                return true;
            })
        }),
    getPropertyBySlug: publicProcedure
        .input(z.string())
        .query(({ input: slug }) => {
            return data[0];
        }),
});

export type PropertiesRouter = typeof propertiesRouter;
