/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import {
    Block,
    IllustratedBlockContent,
    type LandingPageHero,
} from "~/types/content";
import featuredImage from "~/../public/conf.jpg";
import Header2 from "~/components/atoms/Header2";
import ServicePageLayout from "~/components/services/ServicePageLayout";
import TextBlock from "~/components/common/TextBlock";
import IllustratedBlock from "~/components/atoms/IllustratedBlock";
import Echelons from "~/components/illustrations/Echelons";
import TopDown from "~/components/illustrations/TopDown";

const pageTitle = "Sale & Transaction Facilitation";

const hero: LandingPageHero = {
    segmentedTitle: true,
    title: [
        {
            line: "Sale Process.",
            highlighted: false,
        },
        {
            line: "Transaction Due Diligence.",
            highlighted: false,
        },
        {
            line: "Streamlined.",
            highlighted: true,
        },
    ],
    heroHighlights: [
        {
            line: "Support your asset sale with our legal and compliance network",
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/BgCzxAUZ46QJ4Pwb9",
    ctaText: "Elevate Your Sale Strategy".toUpperCase(),
    illustration: TopDown,
};

const tag = "Sale & Transaction";

const approachBlock: Block = {
    contentType: "generic",
    tag,
    title: "Our Transaction Management Approach",
    body: `
### Mastering the Art of the Deal
Our approach to Transaction Management is rooted in a deep understanding of the real estate and investment landscapes. We take a holistic view, considering not just the immediate transaction, but also how it fits into your broader investment strategy. Our team of experienced professionals is your steadfast ally throughout the entire process, from initial due diligence to final closing.

#### What to expect

At QSI, we tailor our Transaction Management services to suit the unique needs of each deal. We know that each transaction brings its own set of complexities and challenges, and we're equipped to handle them all. Here’s what you can expect when working with us:

1. **Comprehensive Due Diligence**: We conduct thorough due diligence to uncover any potential risks or issues, ensuring you have all the information you need to make informed decisions.
2. **Expert Negotiation**: Our seasoned negotiators strive to secure the best possible terms for you, balancing firm advocacy with diplomatic finesse.
3. **Seamless Coordination**: From liaising with stakeholders to managing timelines and documentation, we coordinate all aspects of the transaction process to ensure a smooth and stress-free experience.
4. **Rigorous Risk Management**: Identifying, analyzing, and mitigating risks is paramount to safeguarding your interests throughout the transaction.
5. **Strategic Closing**: We manage the closing process with strategic precision, ensuring every legal and financial detail is in place for a successful transaction completion.

### Why QSI for Transaction Management?

With QSI, you gain a partner uniquely capable of turning complex transactions into successful closes. We're known for our operational excellence, laser-focused on achieving results that align with your strategic objectives. Our transaction management is synonymous with integrity, innovation, and the relentless pursuit of client success.
`,
};

const fullServiceBlock: IllustratedBlockContent = {
    tag,
    header: "Full-Service Sale Facilitation with QSI",
    subtitle: "Your trusted guide and ally",
    body: "When partnering with QSI, you don't just get transaction managers — you gain access to a tactical team equipped to handle every element of sale facilitation. Our approach is designed to provide a seamless, end-to-end service that leaves no detail unattended. From the strategic preparation to the final signatures, we ensure each facet of the sale is executed with precision and expertise.",
    illustration: Echelons,
    cta: false,
};

const expandBlock: Block = {
    contentType: "generic",
    tag,
    title: "Expanding the Scope of Sale Facilitation",
    body: `
### QSI Sale Facilitation: Beyond Just Transactions
At QSI, our sale facilitation services extend far beyond the boundaries of traditional transaction management. We provide a consolidated suite of services to ensure each sale is not just a transaction, but a strategic advancement towards achieving your financial goals.

Let QSI be your trusted guide and ally in the realm of sales facilitation. Together, we'll navigate the complexities of the selling process, ensuring a successful transaction and the enduring prosperity of your investments.

### Complementary Sale Facilitation Services

- **Legal and Regulatory Compliance**: Ensuring a smooth transaction means having a keen eye on all legal and regulatory requirements. Our team offers expert legal counsel and compliance support to navigate the complex landscape of real estate transactions, protecting your interests at every stage.
- **Market Positioning and Branding**: A compelling narrative can significantly influence the success of a sale. We create bespoke marketing campaigns that effectively position your asset in the market, using branding strategies that resonate with potential buyers and set your property apart.
- **Post-Sale Asset Transitioning**: The conclusion of a sale marks the beginning of a new relationship with the asset. We offer services to facilitate a gentle handover process, providing continuous support through transition planning, tenant communication strategies, and operational guidance.
- **Investment Recasting**: After a successful sale, we work with you to reinvest the capital in a manner that aligns with your evolving investment strategy. Our team assists in portfolio recasting to ensure your continued growth trajectory in the market.
- **Financial Structuring**: At the heart of every sale is its financial architecture. We provide sophisticated financial structuring services, modeling cash flows and analyzing taxation implications to optimise your financial outcome.
- **Custom Closing Solutions**: Recognising that each client's needs are distinct, we develop tailored closing solutions to address your specific challenges and facilitate a smooth, efficient transaction tailored to your asset's unique profile.

`,
};

// TODO add success cases and showcase like pitchdecks and sutff

const SaleAndTxPage: React.FC = () => {
    return (
        <ServicePageLayout
            pageTitle={pageTitle}
            hero={hero}
            showMotto
            showContacts
            contactsHeader="Start Your Transaction Journey"
            contactsBlurb="Choose QSI for a transaction experience defined by strategic insight, expert execution, and a commitment to maximising the full potential of your investment."
            ctaContact="oscar"
            ctaTitle="Excellence in Execution"
            ctaLabel="LET'S BEGIN"
            ctaChildren={
                <div className="">
                    <p>
                        Together, we'll navigate the complexities of the selling
                        process, ensuring a successful transaction and the
                        enduring prosperity of your investments.
                    </p>
                </div>
            }
        >
            <div className="mb-10 px-2">
                <Header2 title="Navigating Complex Transactions with Precision and Skill" />
                <p className="mx-auto mt-5 w-fit px-5 font-body text-2xl font-[400] lg:px-0">
                    The closing of a deal is a defining moment in the lifecycle
                    of any asset. It is the culmination of strategic planning,
                    market analysis, and negotiation. At QSI, our Transaction
                    Management services are designed to ensure that this
                    critical phase is executed flawlessly, with meticulous
                    attention to detail and unwavering commitment to our
                    clients’ objectives.
                </p>
            </div>
            <TextBlock block={approachBlock} paddedLists />
            <IllustratedBlock
                content={fullServiceBlock}
                illustrationPosition="right"
            />
            <TextBlock block={expandBlock} />
        </ServicePageLayout>
    );
};

export default SaleAndTxPage;
