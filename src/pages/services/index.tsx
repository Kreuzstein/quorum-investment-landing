/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { useEffect, useRef, useState } from "react";
import Head from "next/head";

import { Element } from "react-scroll";

import { type IllustratedBlockContent } from "~/types/content";
import featuredImage from "~/../public/london-bridge.jpg";
import Header from "~/components/common/Header";
import CompactHero from "~/components/common/CompactHero";
import IllustratedBlock from "~/components/atoms/IllustratedBlock";
import Motto from "~/components/home/Motto";
import Contact from "~/components/home/Contact";
import Footer from "~/components/home/Footer";
import Stonks from "~/components/illustrations/Stonks";
import Arrow from "~/components/illustrations/Arrow";
import Balance from "~/components/illustrations/Balance";
import Reinforce from "~/components/illustrations/Reinforce";
import Echelons from "~/components/illustrations/Echelons";
import TopDown from "~/components/illustrations/TopDown";
import Feasibility from "~/components/illustrations/Feasibility";
import Followup from "~/components/illustrations/Followup";
import Concord from "~/components/illustrations/Concord";
import Orbit from "~/components/illustrations/Orbit";
import Finance from "~/components/illustrations/Finance";
import Networking from "~/components/illustrations/Networking";
import Revelation from "~/components/illustrations/Revelation";
import Digital from "~/components/illustrations/Digital";
import Pitch from "~/components/illustrations/Pitch";
import { scrollToElement } from "~/utils/common";
import Chevron from "~/components/icons/Chevron";

const data: IllustratedBlockContent[] = [
    {
        illustration: () => (
            <div className="h-[20rem] w-[20rem] scale-75">
                <Stonks />
            </div>
        ),
        header: "Listing Commercial Property",
        subtitle: "Premier Property Exposure",
        body: "List your commercial property to access a vast network of active investors and maximise your asset's reach.",
        ctaHref: "/list-with-us",
        ctaLabel: "List Commercial Assets With Us",
        cta: true,
    },
    {
        illustration: Arrow,
        header: "Listing Non-Commercial Real Estate",
        subtitle: "Targetted Real Estate Listings",
        body: "Expand the visibility of your residential or specialty properties through our specialised non-commercial listing services.",
        ctaHref: "/list-with-us",
        ctaLabel: "List Real Estate With Us",
        cta: true,
    },
    {
        illustration: Orbit,
        header: "Sales Pitch & BDM Advisory",
        subtitle: "Winning Strategies",
        body: "Refine your sales pitch and foster business development with our custom-tailored strategies and skillful execution.",
        ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        ctaLabel: "Book a Free Consultation",
        cta: true,
    },
    {
        illustration: Pitch,
        header: "Valuation Services",
        subtitle: "Accurate Market Valuations",
        body: "Gain precise market value assessments for informed decision-making and optimal pricing strategies.",
        ctaHref: "/services/valuation-services",
        ctaLabel: "Learn More",
        cta: true,
    },
    {
        illustration: Reinforce,
        header: "Listing Digital Businesses",
        subtitle: "Digital Market Integration",
        body: "Connect your digital business with investors specialising in tech and e-commerce for faster growth and innovation.",
        ctaHref: "/list-with-us",
        ctaLabel: "List Digital Businesses With Us",
        cta: true,
    },
    {
        illustration: Echelons,
        header: "Sale & Transaction Facilitation",
        subtitle: "Streamlined Sale Processes",
        body: "Our team ensures a smooth and efficient transaction from listing to close, with comprehensive sale facilitation services.",
        ctaHref: "/services/transaction-management",
        ctaLabel: "Learn More",
        cta: true,
    },
    {
        illustration: Feasibility,
        header: "Selling Stakes in Your Company",
        subtitle: "Equity Investment Strategies",
        body: "Attract strategic investors by offering equity stakes, with our support in structuring equitable transactions.",
        ctaHref: "/services/sell-stakes-in-commercial-assets",
        ctaLabel: "Learn More",
        cta: true,
    },
    {
        illustration: Followup,
        header: "Raising VC Funds",
        subtitle: "Fuel for Growth",
        body: "Accelerate your business growth with venture capital fundraising, guided by our seasoned experts.",
        ctaHref: "/services/alternative-markets",
        ctaLabel: "Learn More",
        cta: true,
    },
    {
        illustration: Digital,
        header: "Digital Transformation of the Asset/Business",
        subtitle: "Embrace Technological Evolution",
        body: "Transform your asset or business with innovative digital solutions, aligning with the evolving market demands.",
        ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        ctaLabel: "Book a Free Consultation",
        cta: true,
    },
    {
        illustration: Concord,
        header: "Supply Chain Integration / Partners Lookup",
        subtitle: "Operational Synergy",
        body: "Connect with key supply chain partners to streamline operations and enhance business efficacy.",
        ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        ctaLabel: "Book a Call to Discover Synergies",
        cta: true,
    },
    {
        illustration: TopDown,
        header: "Legal and Compliance",
        subtitle: "Regulatory Mastery",
        body: "Navigating complex legal landscapes is our specialty, ensuring your assets meet all compliance mandates.",
        ctaHref: "/qsi-partner-network",
        ctaLabel: "See our partners in compliance",
        cta: true,
    },
    {
        illustration: Finance,
        header: "Financial Modeling",
        subtitle: "Financial Insight and Forecasting",
        body: "Develop robust financial models for scenario planning and investment analysis to predict and enhance financial performance.",
        ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        ctaLabel: "Book a Free Consultation",
        cta: true,
    },
    {
        illustration: Balance,
        header: "Commercial Property Management Advisory",
        subtitle: "Strategic Asset Optimisation",
        body: "Leverage our expert advisory services to enhance your asset management approach and operational efficiency.",
        ctaHref: "/services/commercial-property-management",
        ctaLabel: "Learn More",
        cta: true,
    },
    {
        illustration: Networking,
        header: "Feasibility Study",
        subtitle: "Project Viability Assessments",
        body: "Assess new ventures or expansions with our comprehensive feasibility studies, laying the groundwork for successful business decisions.",
        ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        ctaLabel: "Book a Free Consultation",
        cta: true,
    },
    {
        illustration: Revelation,
        header: "Asset Audit",
        subtitle: "Comprehensive Asset Review",
        body: "A thorough audit of your assets to ensure accurate reporting, risk management, and value maximisation.",
        ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        ctaLabel: "Book a Free Consultation",
        cta: true,
    },
];

// Portfolio Restructuring
// Subtitle: Strategic Alignment
// Description: Realign your investment portfolio with evolving goals and market conditions for continued success.
// [Button: Learn More]

// Market Entry Strategies
// Subtitle: Entry and Expansion Consulting
// Description: Expert guidance to penetrate new markets or expand within existing ones, with a focus on sustainable growth.
// [Button: Learn More]

const ServicesPage: React.FC = () => {
    const pageTitle = "Asset Management Services | Quorum Strategic Investment";
    const meta = {
        propTitle: "Asset Management Services",
        heroHighlights: [
            "We forge connections between investors and upscale properties",
            "Learn how QSI can elevate your assets' performance",
        ],
        featuredImage,
    };
    const dataWithRefs = data.map((item) => ({
        ...item,
        ref: useRef(),
    }));

    const [currentService, setCurrentService] = useState("");
    useEffect(() => {
        const observerCallback = (
            entries: IntersectionObserverEntry[],
            _observer: any,
        ) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    // Entry is intersecting the threshold, handle logic here.
                    setCurrentService(entry.target.id);
                }
            });
        };

        const options = {
            root: null, // observing with respect to the viewport
            rootMargin: "0px 0px -80% 0px", // sets a margin from the top of the viewport (100px)
            threshold: 0.01, // minimum threshold to trigger observerCallback (use 0 for onEnter)
        };

        const observer = new IntersectionObserver(observerCallback, options);

        for (const { ref } of dataWithRefs) {
            if (ref.current) observer.observe(ref.current);
        }

        return () => {
            for (const { ref } of dataWithRefs) {
                if (ref.current) observer.unobserve(ref.current);
            }
        };
    }, []);

    return (
        <>
            <Head>
                <title>{pageTitle}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial Investment | EU Residency By Investment | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip bg-white">
                <Header view="content" />
                <CompactHero asset={meta} />
                <article
                    id="container"
                    className="z-[25] mx-auto my-10 flex h-fit min-h-[20rem] w-full max-w-7xl flex-row font-body text-black"
                >
                    {/* Service list */}
                    <section className="mt-5 flex lg:w-2/3 w-full flex-col md:gap-y-5 gap-y-10 px-2">
                        {dataWithRefs.map((item, index) => {
                            return (
                                <div
                                    id={item.header}
                                    ref={
                                        item.ref as unknown as React.LegacyRef<HTMLDivElement>
                                    }
                                    key={index}
                                >
                                    <Element name={item.header}>
                                        <IllustratedBlock
                                            content={item}
                                            illustrationPosition={
                                                index % 2 ? "right" : "left"
                                            }
                                        />
                                    </Element>
                                </div>
                            );
                        })}
                    </section>

                    {/* Sticky ToC */}
                    <section className="sticky top-[6rem] h-fit w-1/3 lg:block hidden">
                        <p className="mb-3 w-fit bg-violet-500 px-2 text-xl font-semibold text-white">
                            Asset Management Services:
                        </p>
                        <ul className="flex flex-col gap-y-1">
                            {data.map(({ header }, index) => {
                                const activeBlock = currentService === header;
                                return (
                                    <li
                                        key={index}
                                        onClick={() => scrollToElement(header)}
                                        className={
                                            "flex flex-row font-body text-lg" +
                                            " " +
                                            (activeBlock
                                                ? "font-bold"
                                                : "cursor-pointer ")
                                        }
                                    >
                                        <div
                                            className={
                                                "rotate-180 my-auto" +
                                                " " +
                                                (activeBlock
                                                    ? ""
                                                    : "opacity-0")
                                            }
                                        >
                                            <Chevron />
                                        </div>
                                        <p>{header}</p>
                                    </li>
                                );
                            })}
                        </ul>
                    </section>
                </article>
                <Motto />
                <Contact />
                <footer className="flex bg-black">
                    <Footer />
                </footer>
            </main>
        </>
    );
};

export default ServicesPage;
