/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Head from "next/head";

import featuredImage from "~/../public/costa-rica.jpg";
import Header2 from "~/components/atoms/Header2";
import IllustratedBlock from "~/components/atoms/IllustratedBlock";
import ContentHero from "~/components/common/ContentHero";
import Header from "~/components/common/Header";
import StickyCta from "~/components/common/StickyCta";
import TextBlock from "~/components/common/TextBlock";
import Contact from "~/components/home/Contact";
import Footer from "~/components/home/Footer";
import Motto from "~/components/home/Motto";
import Balance from "~/components/illustrations/Balance";
import Kaizen from "~/components/illustrations/Kaizen";
import ServicePageLayout from "~/components/services/ServicePageLayout";
import {
    Block,
    IllustratedBlockContent,
    LandingPageHero,
} from "~/types/content";

const pageTitle =
    "Strategic Asset Management for Maximum Returns";
const hero: LandingPageHero = {
    segmentedTitle: true,
    title: [
        {
            line: "Strategic",
            highlighted: false,
        },
        {
            line: "Asset Management",
            highlighted: false,
        },
        {
            line: "for Maximum Returns",
            highlighted: true,
        },
    ],
    heroHighlights: [
        {
            line: "Excellence in Commercial Asset Management",
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/BgCzxAUZ46QJ4Pwb9",
    ctaText: "GET STARTED",
    illustration: Balance,
};
const tag = "Property Management";

const integratedServiceModel: Block = {
    contentType: "generic",
    tag,
    title: "Integrated Service Model",
    body: `
**Tailored Management Plans**: Every commercial property has its unique character and demands. We devise personalised management strategies that cater to the specific needs of your asset, whether it's a bustling retail space, a state-of-the-art office building, or an expansive hotel complex.

**Tenant Retention & Optimization**: The lifeblood of commercial real estate is its tenants. Our comrades from QSI Partner Network help us execute on targetted tenant engagement and superior facilities management, we keep occupancy high, and turnover low, ensuring a stable income stream for property owners.

**Operational Excellence**: From routine maintenance to advanced security measures, our operational excellence framework ensures that every aspect of your property functions flawlessly, elevating the experience for tenants and visitors alike.

**Financial Stewardship**: With a keen eye on performance metrics and cash flows, we administer the financial aspects of your property with the utmost diligence, striving for transparency, accuracy, and profitability.
    `,
};

const esg: IllustratedBlockContent = {
    tag,
    header: "ESG Advisory for Sustainable Real Estate!",
    subtitle: "ESG strategies on an asset and portfolio level.",
    body: `
We align commercial property services with the principles of sustainability and social responsibility. Our ESG initiatives focus on energy efficiency, resource conservation, and creating spaces that positively contribute to the environment and the community.  We are actively working with all our clients on helping them to achieve Net Zero and ESG targets.
    `,
    illustration: Kaizen,
    cta: false,
};

const specialisedServices: Block = {
    contentType: "generic",
    tag,
    title: "Specialised Services",
    body: `
**Lease Administration & Advisory**: Navigating lease agreements can be complex; our expertise simplifies the process. We offer comprehensive lease administration services, including negotiations, renewals, and compliance checks, for optimal contract performance.

**Marketing & Leasing Strategies**: Attracting the right tenants begins with a compelling marketing narrative. QSI employs innovative marketing strategies backed by in-depth market analyses to ensure your property stands out.

**Technology Integration**: In today's digital era, technology plays a pivotal role in property management. We incorporate state-of-the-art tech solutions to enhance property functions, from energy management systems to tenant communication platforms.

**Risk Mitigation**: Managing risk is an integral component of our service. We implement proactive measures to minimise exposure and ensure that your investment is safeguarded against unforeseen circumstances.
    `,
};

// TODO add beyound prop mangement
// TODO add news and isnight block
// TODO add newsletter opton to motto element

const AssetManagementPage: React.FC = () => {
    return (
        <ServicePageLayout
            pageTitle={pageTitle}
            hero={hero}
            showMotto
            showContacts
            contactsBlurb="Ready to elevate your commercial property to new heights of success? Reach out to the QSI team and forge a partnership that brings expertise, innovation, and tangible results to every square foot of your commercial space."
            ctaContact="oscar"
            ctaTitle="Ready to Win?"
            ctaLabel="LET'S DISCUSS"
        >
            <div className="mb-10 px-2">
                <Header2 title="Pioneering Commercial Property Services" />
                <p className="mx-auto mt-5 w-fit px-5 font-body text-2xl font-[400] lg:px-0">
                    At QSI, commercial property services represent more than
                    just management; they're about enhancing asset value through
                    strategic innovation and bespoke solutions. With a seasoned
                    team of experts and comprehensive market knowledge, we
                    assure you of management perfection that aligns with your
                    commercial aspirations.
                </p>
            </div>
            <TextBlock block={integratedServiceModel} />
            <IllustratedBlock content={esg} illustrationPosition="right" />
            <TextBlock block={specialisedServices} />
        </ServicePageLayout>
    );
};

export default AssetManagementPage;
