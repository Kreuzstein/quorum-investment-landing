/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import {
    Block,
    IllustratedBlockContent,
    type LandingPageHero,
} from "~/types/content";
import featuredImage from "~/../public/digi-biz.jpg";
import Header2 from "~/components/atoms/Header2";
import ServicePageLayout from "~/components/services/ServicePageLayout";
import TextBlock from "~/components/common/TextBlock";
import IllustratedBlock from "~/components/atoms/IllustratedBlock";
import Followup from "~/components/illustrations/Followup";
import Reinforce from "~/components/illustrations/Reinforce";

const pageTitle = "Listing Digital Businesses & Alternative Market Assets";

const hero: LandingPageHero = {
    segmentedTitle: true,
    title: [
        {
            line: "Digital Businesses & ",
            highlighted: false,
        },
        {
            line: "Alternative Market Assets",
            highlighted: false,
        },
    ],
    heroHighlights: [
        {
            line: "Sale and fund-raising for SaaS, startups, and digital assets",
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
    ctaText: "LET'S MAKE SOME MONEY",
    illustration: Followup,
};

const tag = "Digital Businesses";

const specialisedApproachBlock: Block = {
    contentType: "generic",
    tag,
    title: "Seamless Listing & Investment Attraction",
    body: `
### Our Specialised Approach
Digital businesses are a hallmark of modern investment. From established e-commerce platforms to innovative tech startups, our tailored listing services amplify your digital business in a crowded market. QSI understands the digital pulse and investor appetite, facilitating connections that enhance your reach and market value.
#### How it works?
We begin by understanding your digital business model, its growth trajectory, and potential. With a deep dive into analytics, performance metrics, and competitive positioning, we create compelling narratives that resonate with savvy tech investors who value innovation and potential for scale.

Our approach is to meticulously craft a strategic plan that encompasses a targeted promotion to the right investor segments, from angel investors to venture capitalists and strategic corporate entities. We ensure that confidentiality, critical in the tech space, is maintained while providing you with a spectrum of qualified investors. 
    `,
};

const bespokeMarketingBlock: IllustratedBlockContent = {
    tag,
    header: "Bespoke Marketing for Digital Asset Sales",
    subtitle: "Tailored Premium Listings",
    body: "We believe in a partnership approach. This means you'll have a dedicated team working alongside you every step of the way – from the initial consideration to sell stakes through to the final execution and beyond. We prioritise clear communication and transparency, ensuring you're informed and in control at every stage.",
    illustration: Reinforce,
    cta: false,
};

const vcBlock: Block = {
    contentType: "generic",
    tag,
    title: "Secure Venture Capital with Expert Guidance",
    body: `
### Elevating Innovative Ventures

In the quest for venture capital, a strategic approach is vital. At QSI, we specialise in empowering businesses to secure funding by showcasing their potential to the right investors. We understand the intricacies of investment structures, value propositions, and the art of the pitch, which we tailor to ensure your venture stands out in a competitive ecosystem.

#### How to Raise Venture Capital?
Our services in raising venture capital are comprehensive—we assist in refining investment structures, creating compelling investor value propositions, and developing persuasive pitch decks. Our team collaborates with you to understand your business model, market opportunities, and competitive advantages, translating this understanding into a robust funding strategy.

We prepare you for the journey ahead, from navigating investor negotiations to finalising deal closings. With our guidance, your search for venture capital transforms into an organised and compelling narrative, attracting investors who are eager to be part of your story and financial future.
`,
};
const altMarketsBlock: Block = {
    contentType: "generic",
    tag,
    title: "Alternative Markets: Beyond The Conventional",
    body: `
### Diversification through Non-Traditional Assets

Expanding the horizons of investment opportunities, QSI leads in listing services for alternative market assets. Whether you have a unique real estate project, a green energy initiative, or a rare collectible, QSI's service is designed to curate and promote your asset within specialised investment circles.

Our expertise in alternative markets is grounded in a keen understanding of niche investment dynamics and the unique narratives that they command. We work with you to highlight the distinctiveness and potential returns of your assets, crafting bespoke marketing campaigns that attract a strategic investor base inclined towards diversification.

We connect your non-traditional asset to a curated network of investors who see the long-term value and differentiation that such assets bring to their portfolios. Our advisory team supports you through every step – valuation, marketing strategy, investor outreach, and transaction support – ensuring a service that exceeds the conventional.
    `,
};

// TODO add success cases and showcase like pitchdecks and sutff

const AltMarketsPage: React.FC = () => {
    return (
        <ServicePageLayout
            pageTitle={pageTitle}
            hero={hero}
            showMotto
            showContacts
            contactsHeader="Quorum. Your Bridge to the Future of Investment"
            contactsBlurb="Embrace the future of investments with QSI as your strategic partner. Our forward-thinking strategies and extensive network position your digital and alternative assets to capitalise on market trends and investor interest."
            ctaContact="anton"
            ctaTitle="Full Spectrum Support"
            ctaLabel="LIST WITH US"
            ctaChildren={
                <div className="">
                    <div className="border-b-2 py-1">
                        QSI's full spectrum support ensures that listing your
                        digital businesses or alternative market assets is a
                        strategic, well-managed process.
                    </div>
                    <div className="pt-1">
                        From initial appraisal to closing the deal, we offer
                        comprehensive guidance encompassing market analysis,
                        deal structuring, and post-sale advisory.
                    </div>
                </div>
            }
        >
            <div className="mb-10 px-2">
                <Header2 title="Navigating the Digital Frontier & Alternative Investments" />
                <p className="mx-auto mt-5 w-fit px-5 font-body text-2xl font-[400] lg:px-0">
                    In the rapidly evolving landscape of digital commerce and
                    alternative investments, QSI stands at the forefront,
                    offering services unique in scope and execution. Whether
                    you're looking to sell a cutting-edge digital enterprise or
                    attract investors for non-traditional assets, our team has
                    the expertise to position your proposition for success.
                </p>
            </div>
            <TextBlock block={specialisedApproachBlock} />
            <IllustratedBlock
                content={bespokeMarketingBlock}
                illustrationPosition="right"
            />
            <TextBlock block={vcBlock} />
            <TextBlock block={altMarketsBlock} />
        </ServicePageLayout>
    );
};

export default AltMarketsPage;
