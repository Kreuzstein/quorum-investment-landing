/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import {
    Block,
    IllustratedBlockContent,
    type LandingPageHero,
} from "~/types/content";
import featuredImage from "~/../public/diamond.jpg";
import Header2 from "~/components/atoms/Header2";
import ServicePageLayout from "~/components/services/ServicePageLayout";
import TextBlock from "~/components/common/TextBlock";
import IllustratedBlock from "~/components/atoms/IllustratedBlock";
import Echelons from "~/components/illustrations/Echelons";
import TopDown from "~/components/illustrations/TopDown";
import Pitch from "~/components/illustrations/Pitch";

const pageTitle = "Valuation Services For Properties and Investments";

const hero: LandingPageHero = {
    segmentedTitle: true,
    title: [
        {
            line: "Valuations",
            highlighted: true,
        },
        {
            line: "for properties",
            highlighted: false,
        },
        {
            line: "and investments",
            highlighted: false,
        },
    ],
    heroHighlights: [
        {
            line: "Precision Meets Market Acumen",
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
    ctaText: "Request a Valuation".toUpperCase(),
    illustration: Pitch,
};

const tag = "Asset Valuation";

const approachBlock: Block = {
    contentType: "generic",
    tag,
    title: "Our Approach to Asset Valuation",
    body: `
### Reliable Data, Strategic Insights

At QSI, we understand that valuation is both a science and an art. Our approach is rooted in rigorous methodology, leveraging market insights and comprehensive data sets to provide valuations that stand up to scrutiny and align with current market trends.

- **Market Analysis**: Our valuations begin with a thorough market analysis, considering both macroeconomic indicators and micro-market conditions to ensure a nuanced and up-to-date assessment.
- **Comprehensive Methodology**: We employ a blend of industry-standard and innovative valuation methods, from income capitalization to discounted cash flows analysis, tailored to the nature of the asset.
- **In-Depth Reporting**: Our valuation reports are detailed and transparent, providing clear explanations behind each valuation figure, ensuring that clients are well-informed about their assets' worth.
- **Multi-Disciplinary Expertise**: Our team is composed of multi-disciplinary experts with specialised knowledge in various asset types, from commercial real estate to unique investment opportunities.
- **Technology-Driven Processes**: Utilising the latest valuation technology, we deliver assessments with enhanced accuracy and speed, allowing for valuable time and cost efficiencies.
`,
};

const suiteBlock: Block = {
    contentType: "generic",
    tag,
    title: "Valuation Services Suite",
    body: `
**Asset and Portfolio Valuations**. From single assets to diversified portfolios, we offer valuations that reflect the current and potential value of your property investments.

**Financial Reporting**. We provide valuation services for financial reporting purposes, including fair value assessments and support for regulatory compliance.

**Litigation Support and Expert Testimony**. Our valuation experts are equipped to provide litigation support and serve as credible expert witnesses on valuation disputes.

**Feasibility Studies and Investment Analysis**. Beyond mere valuations, we can conduct feasibility studies and investment analyses to support strategic decision-making for development projects and acquisitions.

With QSI Valuation Services, clients gain a strategic advantage: our commitment to delivering valuations that are not only accurate and reliable but also insightful and actionable. We empower clients to navigate the market with confidence, armed with valuations that enhance their strategic moves and steward financial successes.
`,
};

const SaleAndTxPage: React.FC = () => {
    return (
        <ServicePageLayout
            pageTitle={pageTitle}
            hero={hero}
            showMotto
            showContacts
            contactsHeader="The QSI Difference"
            contactsBlurb="Choose QSI for valuations that go beyond numbers to provide market wisdom and strategic foresight."
            ctaContact="anton"
            ctaTitle="Don't miss out"
            ctaLabel="LET'S TALK"
            ctaChildren={
                <div className="">
                    <p>
                        Gain precise market value assessments for informed
                        decision-making and optimal pricing strategies.
                    </p>
                </div>
            }
        >
            <div className="mb-10 px-2">
                <Header2 title="Expert Valuations to Power Informed Decisions" />
                <p className="mx-auto mt-5 w-fit px-5 font-body text-2xl font-[400] lg:px-0">
                    In the complex world of real estate and asset investment,
                    accurate valuations are the cornerstone of informed
                    decision-making. QSI Valuation Services merges deep industry
                    expertise with precision analytics to deliver valuations
                    that investors and owners can trust for strategic planning,
                    transactions, and compliance.
                </p>
            </div>
            <TextBlock block={approachBlock} paddedLists />
            <TextBlock block={suiteBlock} paddedLists />
        </ServicePageLayout>
    );
};

export default SaleAndTxPage;
