/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import Head from "next/head";

import {
    Block,
    IllustratedBlockContent,
    type LandingPageHero,
} from "~/types/content";
import featuredImage from "~/../public/gold-coast.jpg";
import Feasibility from "~/components/illustrations/Feasibility";
import Header2 from "~/components/atoms/Header2";
import ServicePageLayout from "~/components/services/ServicePageLayout";
import TextBlock from "~/components/common/TextBlock";
import IllustratedBlock from "~/components/atoms/IllustratedBlock";
import Cogs from "~/components/illustrations/Cogs";

const pageTitle =
    "Equity Investment Into Commercial Assets";

const hero: LandingPageHero = {
    segmentedTitle: true,
    title: [
        {
            line: "Equity Investment",
            highlighted: false,
        },
        {
            line: "Strategies",
            highlighted: false,
        },
    ],
    heroHighlights: [
        {
            line: "Sell Stakes in Your Commercial Assets",
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
    ctaText: "LET'S MAKE SOME MONEY",
    illustration: Feasibility,
};

const tag = "Equity Investment";

const tailoringEquitySolutionsBlock: Block = {
    contentType: "generic",
    tag,
    title: "Tailoring Equity Solutions",
    body: `
The process begins with a detailed analysis of your asset and a valuation that reflects its true market potential. We then craft compelling investment propositions that highlight your asset's strengths and future potential, ensuring that it stands out in a competitive landscape.

Our team of experts will guide you through structuring the transaction, offering bespoke solutions that meet your specific needs. We take every factor into account – from market trends to investor profiles to legal frameworks – to ensure that the sale of equity stakes is not only equitable but also optimal for all parties involved.

With QSI, you'll have access to an extensive network of investors, from institutional entities to private equity firms, all looking for compelling investment opportunities. Our established relationships and reputation in the market act as a beacon, drawing in investors with the capacity and desire to invest in high-quality commercial assets.
    `,
};

const partnershipApproachBlock: IllustratedBlockContent = {
    tag,
    header: "A Partnership Approach",
    subtitle: "Let's work through this together!",
    body: "We believe in a partnership approach. This means you'll have a dedicated team working alongside you every step of the way – from the initial consideration to sell stakes through to the final execution and beyond. We prioritise clear communication and transparency, ensuring you're informed and in control at every stage.",
    illustration: Cogs,
    cta: false,
};

const seamlessBlock: Block = {
    contentType: "generic",
    tag,
    title: "Seamless and Efficient Transactions",
    body: `
Our goal is to facilitate a seamless and efficient transaction. To achieve this, we leverage the latest in digital transaction platforms and provide a range of support services, including legal due diligence, compliance checks, and post-sale transition management, so that you can sell with confidence and ease.
    `,
};

const SellStakesPage: React.FC = () => {
    return (
        <ServicePageLayout
            pageTitle={pageTitle}
            hero={hero}
            showMotto
            showContacts
            contactsBlurb="Choose QSI for an unmatched blend of sophisticated strategic planning, deep market knowledge, and a commitment to delivering outcomes that amplify value and drive investment growth."
            ctaContact="anton"
            ctaTitle="Equity Strategies?"
            ctaLabel="LET'S THINK TOGETHER"
            ctaChildren={
                <div className="">
                    <div className="border-b-2 py-1">
                        At QSI, we recognise that selling stakes in your
                        commercial assets is a nuanced endeavour that requires
                        careful planning and execution.
                    </div>
                    <div className="py-1">
                        Our approach is designed to identify and attract the
                        right strategic investors, those who are aligned with
                        your vision for the property and who can contribute to
                        its long-term success.
                    </div>
                </div>
            }
        >
            <div className="mb-10 px-2">
                <Header2 title="Strategic Equity Stake Sales" />
                <p className="mx-auto mt-5 w-fit px-5 font-body text-2xl font-[400] lg:px-0">
                    When the time comes to diversify your investment portfolio
                    or unlock capital tied up in your commercial assets, selling
                    equity stakes can be a powerful strategy. At QSI, we
                    specialise in formulating and executing equity sale
                    strategies that align with your business objectives and
                    market conditions.
                </p>
            </div>
            <TextBlock block={tailoringEquitySolutionsBlock} />
            <IllustratedBlock content={partnershipApproachBlock} illustrationPosition="left"/>
            <TextBlock block={seamlessBlock} />
        </ServicePageLayout>
    );
};

export default SellStakesPage;
