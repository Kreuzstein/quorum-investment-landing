/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic investments website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Head from "next/head";
import { useRouter } from "next/router";
import Header from "~/components/common/Header";

import TextBlock from "~/components/common/TextBlock";
import Footer from "~/components/home/Footer";
import CompactHero from "~/components/common/CompactHero";
import Motto from "~/components/home/Motto";
import Contact from "~/components/home/Contact";
import { trpc } from "~/utils/trpc";

// TODO add better handling of loading and error

const AboutPage: React.FC = () => {
    const nextRouter = useRouter();
    const slugToCheck = nextRouter.query.aboutSlug;
    const slug: string =
        typeof slugToCheck === "string"
            ? slugToCheck
            : "";
    const { data, isLoading, error } =
        trpc.content.getAboutPageBySlug.useQuery(slug);
    if (!data) return <></>;
    const { block, meta } = data;
    return (
        <>
            <Head>
                <title>{block.title + " | " + block.tag}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial investments | EU Residency By investments | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip">
                <Header view="content" />
                <CompactHero asset={meta} />
                <div className="mx-auto my-10 flex min-h-[20rem] h-fit w-full max-w-7xl flex-col gap-y-5">
                    <TextBlock block={block} />
                </div>
                <Motto />
                <Contact />
                <div className="flex bg-black">
                    <Footer />
                </div>
            </main>
        </>
    );
};

export default AboutPage;
