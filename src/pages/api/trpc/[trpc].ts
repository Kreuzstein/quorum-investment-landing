import * as trpcNext from "@trpc/server/adapters/next";
import { appRouter } from "~/server/root";
import { env } from "~/env.mjs";

// export API handler
// @link https://trpc.io/docs/v11/server/adapters
export default trpcNext.createNextApiHandler({
    router: appRouter,
    //   don't need context before adding session/privateProcs and db abstraction
    createContext: () => ({}),
    onError:
    env.NODE_ENV === "development"
      ? ({ path, error }) => {
          console.error(
            `❌ tRPC failed on ${path ?? "<no-path>"}: ${error.message}`,
          );
        }
      : undefined,

});
