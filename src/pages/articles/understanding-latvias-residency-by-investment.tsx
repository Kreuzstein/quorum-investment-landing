/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Head from "next/head";
import { useRouter } from "next/router";

import featuredImage from "~/../public/riga.jpg";
import Header from "~/components/common/Header";
import TextBlock from "~/components/common/TextBlock";
import Footer from "~/components/home/Footer";
import CompactHero from "~/components/common/CompactHero";
import Motto from "~/components/home/Motto";
import Contact from "~/components/home/Contact";
import { trpc } from "~/utils/trpc";
import { Block } from "~/types/content";

const block: Block = {
    tag: "Residency by Investment",
    title: "Understanding Latvian Residency by Investment",
    contentType: "generic",
    body: `
### Embrace the Opportunity for EU Residency
Latvia offers an enticing pathway to residency for investors through its well-structured program. By investing in Latvia, you not only gain a foothold in a vibrant European economy but also open the door to the possibility of residency. Let’s delve into the specifics of Latvia’s program and what it could mean for you.

#### Legal and Regulatory Landscape
[Latvia’s Immigration Law](https://likumi.lv/ta/en/en/id/68522-immigration-law) outlines the requirements for obtaining a temporary residency permit. Investments in equity capital, increasing or founding a new capital company, and real estate are among the qualifying routes. According to Section 23(1)the investment avenues are detailed as follows:
- A minimum of €50,000 in a small capital company, paying €10,000 to the state budget.
- A minimum of €100,000 in a larger company, with the potential for over 50 employees and an annual turnover/balance exceeding €10 million.
- Real estate investments of at least €250,000, under certain conditions, including a 5% contribution of the property value to the State budget.

> Section 23(1). Paragraph 28. [A foreigner has the right to request a temporary residence permit in accordance with the procedures laid down in this Law] for a period not exceeding five years if he or she has invested in the equity capital of the capital company, increasing it, or has invested in the equity capital of the capital company, founding a new capital company, and, upon requesting the first temporary residence permit, has paid EUR 10 000 into the State budget and also the investment is at least:
> 
> a) EUR 50 000 and it has been invested in a capital company which employs not more than 50 employees and the annual turnover or annual balance thereof does not exceed EUR 10 million. In relation to investment in the equity capital of the capital company a temporary residence permit shall be issued to not more than 10 foreigners if each of them has made the investment specified in this Clause and has paid EUR 10 000 into the State budget;
> 
> b) EUR 100 000 and it has been performed in a capital company which employs more than 50 employees and the annual turnover or annual balance of which exceeds EUR 10 million;
> 
> c) EUR 100 000 and it has been invested in the equity capital of the capital company which together with one or more subsidiary merchants registered in the Republic of Latvia employ more than 50 employees and the total annual turnover or annual balance of which exceeds EUR 10 million;

> Section 23(1). Paragraph 29. [A foreigner has the right to request a temporary residence permit in accordance with the procedures laid down in this Law] for a period not exceeding five years if he or she has acquired in Rīga, Jūrmala, Ikšķile, or Saulkrasti city or Ādaži, Ķekava, Mārupe, Olaine, or Salaspils municipality, or Garkalne, Ropaži, Saulkrasti, Stopiņi, or Tīnūži rural territory and he or she owns one functionally linked and built-up immovable property (except for the case where the immovable property is a vacant land) the value of which is not less than EUR 250 000, or outside the above mentioned administrative territories or territorial division units included in the administrative territory - not more than two immovable properties (except for the case where the immovable property is a vacant land) and each of them is a functionally linked and built-up immovable property the total value of which is at least EUR 250 000 if the following conditions exist concurrently:
> 
> a) he or she does not have payment debts of immovable property tax;
> 
> b) the total value of immovable properties was paid for by a non-cash settlement;
> 
> c) immovable property which has been acquired from a legal person registered in the Republic of Latvia or a European Union Member State, a country of the European Economic Area, or the Swiss Confederation which is a taxpayer within the meaning of the laws and regulations governing the field of taxes of the Republic of Latvia, or from a natural person who is a citizen of Latvia, a non-citizen of Latvia, a citizen of the Union, or a foreigner who is staying in the Republic of Latvia with a valid residence permit issued by the Republic of Latvia;
> 
> d) the total cadastral value of immovable property at the time of acquisition thereof was not less than EUR 80 000. If a foreigner has acquired two immovable properties outside Rīga, Jūrmala, Ikšķile, or Saulkrasti city or Ādaži, Ķekava, Mārupe, Olaine, or Salaspils municipality, or Garkalne, Ropaži, Saulkrasti, Stopiņi, or Tīnūži rural territory, the cadastral value of each immovable property was not less than EUR 40 000 at the time of the acquisition thereof. If the cadastral value is less than the value indicated in this Sub-clause, the value of immovable property may not be less than EUR 250 000 according to the market value of immovable property determined by a certified assessor of immovable property, or if a foreigner has acquired two immovable properties - the market value of each immovable property may not be less than EUR 125 000;
> 
> e) in requesting the first temporary residence permit, he or she pays five per cent of the value of immovable property into the State budget;
> 
> f) the composition of the immovable property does not include land for agricultural use or forest land.

#### Foreign Ownership Regulations

Foreigners may also invest in Latvian businesses and own shares without restrictions. However, foreign-owned businesses must include a certain number of residents on their Management Boards.

>There are no restrictions on the ownership of Latvian companies by foreigners. 100% foreign owned enterprises are allowed. Yet there are requirements for certain number of residents to be in the Management Board. Latvian businesses and foreign owned business are mainly organised as limited liability companies (SIA). In some cases, entering the Latvian market may be realized without actually having the foreign business entity itself carry on business in Latvia. The entering into of a distribution or commercial agency agreement in Latvia does not in itself constitute carrying on business in Latvia – neither for tax purposes. Accordingly, it is often appropriate for a foreign entity to consider the use of a Latvian agent or distributor as an alternative to expanding its business operations into Latvia by setting up a company or branch. Please see below in section 3.3. 3.1 Limited Liability and Stock Companies The minimum requirements for the registered share capital in limited liability and stock companies are LVL 2,000 (approx. EUR 3,060) and LVL 25,000 (approx. EUR 38,350) respectively. Higher capital requirements apply in respect of stock companies in the banking and insurance sector. The share capital in either form of company may be paid up in cash or in-kind contributions, subject however to an independent expert’s evaluation. Both limited liabilities and stock companies are vested with the status of a legal entity, distinct from the personality of their shareholders. "One shareholder" companies are permissible. One important advantage of having a subsidiary in either form in Latvia is the limited liability (the Latvian subsidiary's creditors do not have access to the foreign shareholder's funds). Shares in a company represent a portion of the corporate equity. There is no minimum or maximum amount of shares that a company is allowed to issue, unless otherwise specified in the incorporating documents. 

#### Investment Compliance and Due Diligence

Adhering to Customer Due Diligence (CDD) procedures is a key element of Latvia’s commitment to anti-money laundering initiatives. Investors must fulfill the necessary checks and verification processes when forging business relationships, engaging in high-value transactions, or using virtual currencies.

#### Financial and Tax Considerations
Capital gains from disposing assets like real estate or shares are taxed at a rate of 20%. However, exemptions are available for individuals who meet certain criteria, such as holding the property for 60 months. Income generated from investments, like dividends, is subject to a 33% tax rate, with tax credit available based on foreign tax paid and income earned abroad.

Latvia has double tax avoidance agreements with many countries, ensuring that tax residents don't pay PIT on income earned and taxed in other EU/EEA member states or countries with effective tax treaties.

An initial contribution of €10,000 to the government is required for the residency application process, marking the only significant transfer fee for establishing residency through investment.

### Your Gateway to Europe

The Latvian residency by investment program stands out for its accessibility and the value it offers. By meeting the investment requirements and abiding by the legal and financial regulations, investors can enjoy the significant advantages of EU residency, including freedom of movement, expansive market access, and a stable social environment.
    `,
};

const meta = {
    propTitle: "Understanding Latvian Residency by Investment",
    heroHighlights: ["One EU Residency Opportunity You Missed"],
    featuredImage,
};

const ArticlePage: React.FC = () => {
    return (
        <>
            <Head>
                <title>{block.title + " | " + block.tag}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial investments | EU Residency By investments | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip bg-white text-black">
                <Header view="content" />
                <CompactHero asset={meta} imagePosition="50% 50%" hideWave/>
                <div className="mx-auto my-10 flex h-fit min-h-[20rem] w-full max-w-7xl flex-col gap-y-5">
                    <TextBlock block={block} />
                </div>
                <Motto hideWave/>
                <Contact />
                <div className="flex bg-black">
                    <Footer />
                </div>
            </main>
        </>
    );
};

export default ArticlePage;
