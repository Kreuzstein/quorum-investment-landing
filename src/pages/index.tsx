/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Head from "next/head";
import Header from "~/components/common/Header";
import Coverage from "~/components/home/Coverage";
import Hero from "~/components/home/Hero";
import Services from "~/components/home/Services";
import About from "~/components/home/About";
import Contact from "~/components/home/Contact";
import News from "~/components/home/News";
import Motto from "~/components/home/Motto";
import Footer from "~/components/home/Footer";

import { Element } from "react-scroll";

export const homeBlocks = {
  hero: "hero",
  services: "services",
  coverage: "coverage",
  news: "news",
  motto: "motto",
  contact: "contact",
  about: "about",
} as const;
export type HomeBlock = typeof homeBlocks[keyof typeof homeBlocks];

export default function Home() {
  const divisionName = "Quorum Strategic Investment";

  return (
    <>
      <Head>
        <title>{divisionName}</title>
        <meta name="description" content="Forging connections between investors and upscale, high potential properties." />
      </Head>
      <main className="flex min-h-screen flex-col bg-black overflow-x-clip ">
        <Header view="home"/>
        <Element name={homeBlocks.hero}>
          <Hero />
        </Element>
        <Element name={homeBlocks.services}>
          <Services />
        </Element>
        <Element name={homeBlocks.coverage}>
          <Coverage />
        </Element>
        <Element name={homeBlocks.about}>
          <About />
        </Element>
        {/* <News /> */}
        <Motto />
        <Element name={homeBlocks.contact}>
          <Contact />
        </Element>
        <Footer />
      </main>
    </>
  );
}
