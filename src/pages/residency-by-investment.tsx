/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import Head from "next/head";

import featuredImage from "~/../public/dubrovnik.jpg";

import { titleBuilder } from "~/utils/common";
import { Block, LandingPageHero } from "~/types/content";
import Header2 from "~/components/atoms/Header2";
import Header from "~/components/common/Header";
import ContentHero from "~/components/common/ContentHero";
import Concord from "~/components/illustrations/Concord";
import Motto from "~/components/home/Motto";
import Contact from "~/components/home/Contact";
import Footer from "~/components/home/Footer";
import TextBlock from "~/components/common/TextBlock";
import Tag from "~/components/atoms/Tag";
import AssetCardShort from "~/components/common/AssetCardShort";
import Accordion, { AccordionItem } from "~/components/atoms/Accordion";

const pageTitle = "Residency by Investments";
const tag = pageTitle;

const hero: LandingPageHero = {
    segmentedTitle: true,
    title: [
        {
            line: "Unlock",
            highlighted: false,
        },
        {
            line: "European Residency",
            highlighted: true,
        },
        {
            line: "Through Investment",
            highlighted: false,
        },
    ],
    heroHighlights: [
        {
            line: "Discover how QSI Partner Network paves your path to a new residency",
        },
        {
            line: "by investing in prime real estate and commercial enterprise.",
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/v5v1ovLGY685HFjp7",
    ctaText: "BEGIN YOUR JOURNEY",
    illustration: Concord,
};

const intro = {
    title: "Residency by Investment",
    subtitle:
        "Embark on a transformative journey toward European residency through strategic property investment with QSI.",
    body: [
        "By channeling capital into the lucrative realm of real estate, investors open the door to an array of benefits that come with EU residency — all while elevating your portfolio.",
        "Our bespoke investment opportunities are key to unlocking residency rights in several EU countries, offering a seamless blend of security, profitability, and mobility. EU residency unlocks a wealth of opportunities, including unrestricted access to all Schengen Zone countries, offering an incredible degree of freedom of movement.",
        "Residency by investment also presents substantial business incentives, granting investors the liberty to operate enterprises across diverse and thriving European markets. Furthermore, investors enjoy an enhanced quality of life, experiencing the cultural richness, stability, and comprehensive healthcare systems that come with living in the European Union.",
        "With QSI, secure a future in the European landscape through investment decisions that couple tangible assets with intangible rewards.",
    ],
} as const;

const howItWorks: Block = {
    contentType: "generic",
    body: `
Embarking on the residency by investment process with QSI is a journey marked by clarity and expert guidance. Below is a simple breakdown of how you can transition from investor to EU resident.

### How it works: 
1. **Initial Consultation**: Connect with a QSI residency specialist to discuss your objectives and explore viable investment options tailored to your goals.
2. **Selection of Investment**: Choose from a range of vetted commercial assets or real estate that qualifies for the residency by investment program in your chosen country.
3. **Due Diligence and Compliance**: QSI conducts thorough due diligence, ensuring all investments meet the legal standards and compliance requirements for residency eligibility.
4. **Transaction and Investment**: Once a suitable investment is selected, QSI facilitates the transaction, guiding you through every step from financial procedures to legal verification.
5. **Residency Application**: With the investment in place, QSI assists with the submission of your residency application, ensuring it is complete and accurate for approval.
6. **Approval and Residency Rights**: Upon approval, you'll be granted residency rights, opening the door to the myriad benefits EU residency provides.
7. **Post-Residency Support**: QSI continues to provide support after residency is obtained, including portfolio management, asset maximization, and renewal applications.

### The Fine Print
For a more comprehensive understanding of specific residency by investment programs, explore our in-depth articles:
- [Understanding Latvia's Residency by Investment](/articles/understanding-latvias-residency-by-investment) 

These resources offer detailed insight into the requirements, benefits, and unique aspects of various residency by investment schemes available within the EU.
`,
    title: "Residency by Investment explained",
    tag,
};

const benefits: Block = {
    tag,
    contentType: "generic",
    title: "Advantages of Residency by Investment with QSI",
    body: `
When you choose QSI for your residency by investment journey, you are not just acquiring a new residency; you are gaining a strategic partner dedicated to your success. Here are the unparalleled advantages of partnering with us:

- **Exclusive Property Opportunities**: Gain access to handpicked real estate and commercial assets not available on the open market.
- **Expert Legal and Financial Navigation**: Benefit from our in-depth knowledge in navigating the complexities of cross-border investment and residency laws.
- **Personalized Investment Planning**: Receive tailored advice that aligns with your personal investment goals, family considerations, and desired lifestyle.
- **Streamlined Process**: Enjoy a simplified and transparent application process with our team guiding you every step of the way.
- **Continued Support**: Encounter unwavering support throughout your investment lifecycle, including after obtaining residency, with services ensuring your asset continues to perform and comply with residency regulations.
`,
};

const faq: AccordionItem[] = [
    {
        title: "How much do I need to invest to qualify for residency?",
        content: "Investment thresholds vary by country and programme, typically ranging from amounts equivalent to USD 250,000 for property investments and USD 75,000 for commercial investments.",
        defaultOpen: true,
    },
    {
        title: "What is the typical time frame for obtaining residency?",
        content: "The process usually takes from a few months up to a year, depending on the specific residency programme and country.",
    },
    {
        title: "Can my family also gain residency through my investment?",
        content: "Most programmes allow for family inclusion, providing residency rights for spouses, children, and sometimes dependent parents. Note that in certain jus soli countries, like Latvia, a newborn child would be entitled to that region's citizenship.",
    },
    {
        title: "What are the renewals requirements for maintaining residency?",
        content: "Conditions for renewal can include maintaining the investment property, spending a certain amount of time in the country, or proof of sustainable financial means.",
    },
    {
        title: "Does residency by investment lead to citizenship?",
        content: "The vast majority of residency programmes offer pathways to citizenship after a period of residency, subject to additional criteria such as language proficiency and integration measures.",
    },
    {
        title: "What sets QSI apart in the residency by investment market?",
        content: "QSI stands out for our personalised service, exclusive investment opportunities, and a comprehensive approach that caters to investors' wealth and lifestyle aspirations. Our Partners Network, encompassing legal and financial professionals is also yours to leverage.",
    },
    {
        title: "Are there any ongoing costs associated with maintaining residency?",
        content: "Additional costs can include property maintenance, taxes, and renewal fees for residency permits. QSI provides clear guidance on these to ensure investors stay informed.",
    },
];

const ResidencyByInvestmentPage: React.FC = () => {
    return (
        <>
            <Head>
                <title>{titleBuilder(pageTitle)}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial Investment | EU Residency By Investment | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip bg-white">
                <Header view="content" />
                <ContentHero hero={hero} />
                <article className="mx-auto my-10 flex h-fit min-h-[20rem] w-full max-w-7xl flex-col gap-y-5 font-body text-black">
                    {/* intro */}
                    <section className="mb-10 px-2">
                        <Header2 title={intro.title} />
                        <p className="mx-auto mt-5 w-fit px-5 font-body text-2xl font-[400] lg:px-0">
                            {intro.subtitle}
                        </p>
                        {intro.body.map((line, index) => (
                            <p className="my-3 px-1 text-lg" key={index}>
                                {line}
                            </p>
                        ))}
                    </section>
                    {/* How it works */}
                    <TextBlock block={howItWorks} paddedLists />
                </article>
                {/* featured props */}
                <section className="flex h-fit min-h-[20rem] w-full flex-col  gap-y-5 bg-[#0c0e11] py-10 font-body text-white">
                    <div className="mx-auto flex h-full w-full max-w-7xl flex-col px-2">
                        <Tag label={tag} />
                        <Header2 title="Featured Investment Opportunities" />
                        <div className="mx-auto mt-6 h-[23rem] w-[30rem] max-w-[90vw]">
                            <AssetCardShort />
                        </div>
                    </div>
                </section>
                <section className="flex h-fit min-h-[20rem] w-full flex-col  gap-y-5 bg-white py-10 font-body text-black">
                    <div className="mx-auto flex h-full w-full max-w-7xl flex-col">
                        {/* TODO eligibility */}
                        {/* Benefits */}
                        <TextBlock block={benefits} />
                        {/* FAQ */}
                        <div className="px-2">
                            <Tag label={tag} />
                            <Header2 title="FAQ" />
                            <Accordion
                                data={faq}
                                accordionId="residency-by-investment-faq"
                            />
                        </div>
                        {/* TODO Insight */}
                        {/* TODO Testimonials */}
                    </div>
                </section>

                <div className="z-30">
                    <Motto hideWave />
                    <Contact blurb="QSI experts are ready to guide them through every step of the residency by investment process." header="Your Journey Starts Here" />
                </div>
                <footer className="z-30 flex bg-black">
                    <Footer />
                </footer>
            </main>
        </>
    );
};

export default ResidencyByInvestmentPage;
