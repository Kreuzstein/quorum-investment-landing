/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Head from "next/head";

import Header from "~/components/common/Header";
import TextBlock from "~/components/common/TextBlock";
import Footer from "~/components/home/Footer";
import ContentHero from "~/components/common/ContentHero";
import featuredImage from "~/../public/chengdu0.jpg";
import Tag from "~/components/atoms/Tag";
import Header2 from "~/components/atoms/Header2";
import Accordion, { type AccordionItem } from "~/components/atoms/Accordion";
import { Block, LandingPageHero } from "~/types/content";
import Motto from "~/components/home/Motto";
import Pitch from "~/components/illustrations/Pitch";
import StickyCta from "~/components/common/StickyCta";
import ServicePageLayout from "~/components/services/ServicePageLayout";

const faq: AccordionItem[] = [
    {
        title: "What types of properties can I list with QSI?",
        content:
            "Our platform is tailored to accommodate a diverse property portfolio. You can list various commercial assets, including office spaces, retail locations, industrial warehouses, and specialty properties. You are also welcome to list premium residential real estate assets. Apart from real estate, we failiate resale and investment into digital businesses and startups.",
        defaultOpen: true,
    },
    {
        title: "How do I start the listing process?",
        content:
            "Initiating the listing process is straightforward: click the 'Get List' button which will open the live schedule of our team's availability for a call. Select the timeslot that suits you best and we'll have a 30-minute discovery call with you. During our call we'll ask you about your goals and the asset you're looking to list. We'll ask you to share any relevant materials: be they photos, info, or other docs. Afterwards, we'll run a short verification and prepare a sale strategy and compile a besopke listing for your property the following business day.",
    },
    {
        title: "What information do I need to provide to list my property?",
        content:
            "Basic information about your property is required, such as location, size, type, current use, occupancy rates, and any unique selling points. All assets are unique, as such we'll share a detailed list of items we would need of you in the follow-up email after our initial call.",
    },
    {
        title: "Are there any fees for listing my property with QSI?",
        content:
            "Getting listed with QSI is free, but we don't list just anybody. Once we have approved your request, our team will prepare an investment breakdown structure, a bespoke listing description, and sale strategies free of charge. Assets not sold after the first month will incur a flat extension fee starting from $50.00 depending on the popularity of the listing. Note that for certain assets, there might be associated costs for additional services; we will disclose those during our initial consultation.",
    },
    {
        title: "How does QSI promote my listed property?",
        content:
            "QSI utilises a multi-channel marketing strategy that includes digital marketing, direct outreach to our vast network of investors, and promotion through industry events and publications.",
    },
    {
        title: "Can I list a property that's outside Europe?",
        content:
            "Yes, QSI operates internationally, and you can list properties located in various regions across the globe.",
    },
    {
        title: "How long does it take for my property to go live on the site after listing?",
        content:
            "Once you submit all the required information, your property can typically go live the next businesses day. However, the verification process may occassional slow it down to 2-3 business days.",
    },
    {
        title: "What happens once my property is sold?",
        content:
            "After a successful sale, QSI will facilitate the transaction and paperwork process. We also provide post-sale support and can offer advice on reinvesting or managing your new assets.",
    },
    {
        title: "How can I track the performance of my listing?",
        content:
            "We believe it is our job to do the grunt work; as such, we'll be emailing you weekly or monthly reports on your listing's performance.",
    },
    {
        title: "Who can I contact if I have more questions?",
        content:
            "Our dedicated listing specialists are available to answer any further questions. Direct contact information can be found on the 'Contact Us' page, or you can use the contact form provided on the 'List with Us' page.",
    },
];

const introBlocks: Block[] = [
    {
        tag: "List With Us",
        title: "Where Assets Meet Opportunity",
        body: `
When it comes to maximising the visibility and value of your asset, choosing the right partner is crucial. At QSI, we provide an unrivaled platform that connects sellers with a global network of eager investors. Listing with us means more than just putting your property on the market; it's about leveraging the collective strength of strategic marketing, in-depth analytics, and a tailored approach to showcase your asset to the right audience.

### Why List with QSI?
- **QSI Partner Network**: QSI connects you with experts in legal, compliance, governance, advisory, and more as the part of our core service offering.
- **Expertise**: Our team consists of seasoned investment professionals with extensive knowledge in commercial asset marketing and sales.
- **Global Reach**: With QSI, your listing gains international exposure, tapping into a breadth of potential investors worldwide.
- **Personalised Service**: Every property is unique. We offer custom-tailored strategies designed to highlight the distinctive aspects of your asset. For every approved asset, we also prepare a bespoke premium-feel listing that will elevate your property above the competition.
- **Efficient Process**: Our streamlined listing procedure ensures your property is visible to prospective buyers within days.

By partnering with QSI, you're not just listing a property; you're igniting a process that's aligned with achieving the best possible outcomes. Our commitment to your success is reflected in every step, from the initial consultation to the closing transaction. We understand the nuances of commercial real estate and harness this insight to turn your listing into a success story.

Ready to position your property in the spotlight it deserves? Connect with a QSI listing specialist today and embark on a path to superior results.
        `,
        contentType: "generic",
    },
];

// TODO add testinmoyn and links to cool examples

const hero: LandingPageHero = {
    title: "List Your Property With Us!",
    heroHighlights: [
        {
            line: "Maximise the return on your assets.",
            highlighted: true,
        },
        {
            line: "Expert-made breakdown of investment terms that best suits you.",
            highlighted: true,
        },
        {
            line: "Free premium-feel listing for your assets.",
            highlighted: true,
        },
    ],
    featuredImage,
    cta: true,
    ctaHref: "https://calendar.app.google/BgCzxAUZ46QJ4Pwb9",
    ctaText: "GET STARTED",
    illustration: Pitch,
};

const howTo = {
    header: "How to list with QSI:",
    texts: [
        "Press 'Get Listed' below to book a call with our team.",
        "Provide any materials, photos, info you'd like featured.",
        "We prepare a bespoke page for you ready the next business day.",
    ],
};

const ListWithUsPage: React.FC = () => {
    const pageTitle = "List Your Property with QSI";

    return (
        <ServicePageLayout
            pageTitle={pageTitle}
            hero={hero}
            showMotto
            hideMottoWave
            ctaContact="oscar"
            ctaLabel="GET LISTED"
            ctaTitle="Ready to List?"
            ctaChildren={<CtaChildren />}
        >
            {/* Intro content */}
            {introBlocks.map((block, index) => (
                <TextBlock block={block} key={index} />
            ))}
            {/* FAQ */}
            <div className="px-2">
                <Tag label="List With Us" />
                <Header2 title="FAQ" />
                <Accordion data={faq} accordionId="list-with-us-faq" />
            </div>
        </ServicePageLayout>
    );
};

export default ListWithUsPage;

const CtaChildren: React.FC = () => {
    return (
        <>
            <div className="flex font-body text-lg font-bold">
                {howTo.header}
            </div>
            <ol className="list-decimal">
                {howTo.texts?.map((label, index) => (
                    <li
                        className="mt-auto list-item list-inside flex-row gap-3 font-body"
                        key={index}
                    >
                        {label}
                    </li>
                ))}
            </ol>
        </>
    );
};
