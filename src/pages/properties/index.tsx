/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import Head from "next/head";

import featuredImage from "~/../public/chongqing-skyline.jpg";
import { usePropertySearchStore } from "~/stores/usePropertySearchStore";
import FilterHero from "~/components/common/FilterHero";
import Header from "~/components/common/Header";
import Contact from "~/components/home/Contact";
import Footer from "~/components/home/Footer";
import Motto from "~/components/home/Motto";
import { titleBuilder } from "~/utils/common";
import { trpc } from "~/utils/trpc";
import AssetCardShort from "~/components/common/AssetCardShort";
import AssetCardFull from "~/components/common/AssetCardFull";

// TODO Propery ot found / sold page and redirect

const pageTitle = "Properties and Assets For Sale";
const hero = {
    title: pageTitle,
    featuredImage,
};

const PropertiesPage: React.FC = () => {
    const filtersState = usePropertySearchStore((state) => state.filters);
    const selectedOptions = new Map();
    for (const [fieldId, filter] of Object.entries(filtersState)) {
        if (filter.type !== "multi") continue;
        selectedOptions.set(fieldId, filter.selectedOptions);
    }
    const { data, isLoading, error } = trpc.properties.getPropertyList.useQuery(
        { filters: Object.fromEntries(selectedOptions) },
    );

    return (
        <>
            <Head>
                <title>{titleBuilder(pageTitle)}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial Investment | EU Residency By Investment | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip bg-black bg-opacity-60">
                <Header view="content" />
                <FilterHero hero={hero} />
                <article className="mx-auto my-10 flex h-fit min-h-[20rem] w-full max-w-7xl flex-col gap-y-5 font-body text-black">
                    <section className="w-full grid md:grid-cols-3 grid-cols-1 gap-6 px-6 md:px-0">
                        {data?.map((property, index) => (
                            // todo fix auto sizing by adding fixed spacers on the asset card behind the image
                            <div key={index} className="min-h-[40rem]">
                                <AssetCardFull property={property} />
                            </div>
                        ))}
                    </section>
                </article>
                <Motto hideWave />
                <Contact />
                <footer className="z-30 flex bg-black">
                    <Footer />
                </footer>
            </main>
        </>
    );
};

export default PropertiesPage;
