/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Head from "next/head";
import { useEffect } from "react";
import Modal from "~/components/atoms/Modal";

import Header from "~/components/common/Header";
import ShareDialog from "~/components/common/ShareDialog";
import TextBlock from "~/components/common/TextBlock";
import Footer from "~/components/home/Footer";
import Album from "~/components/properties/Album";
import AssetHero from "~/components/properties/AssetHero";
import CtaCard, { ctaCardViewOptions } from "~/components/properties/CtaCard";
import Gallery from "~/components/properties/Gallery";
import Map from "~/components/properties/Map";
import { useAlbumStore } from "~/stores/useAlbumStore";
import { useShareStore } from "~/stores/useShareStore";
import img from "~/../public/dtf5836459591.jpg";
import featuredImage from "~/../public/dtf5836459591.jpg";
import { Block } from "~/types/content";
import { PropertyInfo } from "~/types/properties";
import { trpc } from "~/utils/trpc";
import { useRouter } from "next/router";

const body0 = `
Nestled within the vibrant heart of Riga, the storied "House of Benjamins" graces the esteemed address of 12 Kr. Barona street, presenting an exclusive opportunity to acquire a property magnificently poised with panoramic vistas of the adjoining central park. This coveted location ensures a continuous engagement with the tranquillity of lush greenery amidst the bustling urban environment.

The majestic "House of Benjamins," constructed with grandeur in **1885** and subsequently re-imagined in 1930, underwent a meticulous restoration in 2006, shaping its transformation into the illustrious "Europa Royale" hotel, blending historical allure with contemporary comforts.

Spanning a substantial total area of **4,695 sq. m** over several floors, the property is securely nested within a private land plot of **2,346 sq. m**, guaranteeing an expanse of personal space and seclusion. The hotel facility boasts an array of 60 elegantly appointed hotel rooms, each radiating a sense of luxury and relaxation, complemented by an array of balconies and terraces that offer serene views and areas of repose, facing the opulent central park.

Serviceable amenities within this grand establishment include a fine dining restaurant, versatile banquet halls poised to host a variety of prestigious events, and a discreet casino located in the basement, delivering an array of entertainment options for guests and visitors alike.

Presently, the property stands unencumbered and not subject to any leasing agreements, offering immediate entree and operational autonomy to the discerning acquirer. This valuable asset is poised for vast potential, readily awaiting a visionary to harness its unrestrained possibilities.

### Additional Information:

#### Property Proposition: 
* Historical Hotel and Leisure Facility

#### Features of Note:
* Magnificent park-side setting with several outdoor spaces
* Unobstructed panoramic park views
* Architectural legacy coupled with modern renovations
* Comprehensive accommodation facilities
* Convenient, exclusive central city location

The "House of Benjamins," with its unrivalled position and comprehensive features, is unequivocally a trophy asset, promising a lucrative venture for those with an appreciation of heritage, luxury, and a strategic sense of real estate investment.
`;

const block0: Block = {
    body: body0,
    title: "Commercial Asset For Sale | Freehold",
    contentType: "opportunity",
    tag: "Opportunity #1",
    priceLabel: "Offers are invited in the region of",
    priceValue: "€ 7,000,000",
};

const body1 = `
An unparalleled opportunity beckons for investors pursuing a prestigious European residency through a Golden Visa program, augmented by the prospect of consistent financial returns. The esteemed "House of Benjamins" at 12 Kr. Barona street affords investors the chance to partake in an impeccable operational stake in company managing the hotel, situated within the heart of an exclusive central park area.

This remarkable asset not only delivers investors a substantial foothold in the European hotel market but also serves as a gateway to acquiring Latvian residency. Blessed with the principles of jus soli, Latvia offers residency without the prerequisite of habitual presence, representing a significant advantage for global investors.

Investment into the hotel's managing entity is an attractive proposition, promising an anticipated **annual return on investment within the 5% range**, complementing the intrinsic benefits of **EU residency for all investors**.

### Investment Highlights:
- Ownership stake in the "House of Benjamins," a historically significant building renovated to a luxurious "Europa Royale" hotel
- Guaranteed duty to manage the facility with 60 hotel rooms, restaurant, banquet halls, and inserted casino
- Prospective annual return on investment (ROI) intended to be approximately 5%
- Average Occupancy Rate High and Low: 32% to 68%
- Investor Equity Stake: Ranges from 0.5% to 100% based on investment size
- Entitlement to Latvian (EU) residency, with no mandatory habitation requisite
- Intrinsic value appreciation of a landmark property in a coveted location
- Total Asset Valuation: €7,000,000
- Current Market Capitalization: €10,000,000
- Projected Intrinsic Value Growth: 3% per annum
- Share Transfer Terms: Shares can be transferred or sold after a holding period of 5 years

### Exclusive Offer for Astute Investors:
- **Limited-time inducement**: Invest before the 30th of April 2024, and benefit from premium terms
- Early Bird Discount: A reduction of 2% on the initial investment stake
- Priority Booking Privileges: First choice of hotel room bookings for personal stays, subject to availability and advanced reservation requirements
- Exclusive Access: Private tours of the "House of Benjamins" and personal meetings with management to discuss strategic insights and future developments
- Investor Recognition: Your name or company acknowledged in a dedicated 'Investors' section on hotel's website, correspondence and literature
- Additional Bonus: After the first year of successful operation, an extra 0.5% of net profit will be distributed to early investors

#### Investment Structure:

- Investors will acquire equity shares in the property-holding entity, granting them an ownership stake proportional to their investment
- The investment will be locked for a minimum period of 5 years to allow for growth and stabilisation of the asset's revenue-generating capabilities
- Performance of the investment will be reviewed annually, with comprehensive financial reports provided to all stakeholders

### Exit Strategies:
- Share Resale: After the minimum holding period, investors may resell their shares either on the private market or back to the company, depending on the terms of shareholder agreement and available buy-back programme
- Buy-Back Option: The company may offer a buy-back option after 5 years at a pre-agreed formula or current market value, whichever is higher
- Asset Liquidation: In the event of a strategic liquidation, investors will receive priority compensation proportional to their shareholding before any other financial considerations
- Merger or Acquisition: Should an M&A opportunity arise that is beneficial to the shareholders and aligns with the long-term goals of the "House of Benjamins," investors will have the option to vote and potentially benefit from any premium valuation.
- Internal Portfolio Sale: **During the mandatory holding period**, should an investor wish to exit their position, they may sell their shares at face value plus a fixed premium fee to another pre-qualified investor within the QSI portfolio who is interested in participating in the Latvian residency by investment scheme. This internal marketplace facilitates a smoother and potentially quicker transfer of ownership while offering a predefined exit premium to the selling investor. 

#### The investment affords immediate qualification for the Latvian residency by investment programme.

This the golden ticket for discerning investors who place a premium on the fusion of luxury real estate with strategic immigration benefits. The House of Benjamins offers a rare admixture of historical significance, modern luxury accommodation, and a straightforward pathway to European residency, all underscored by a resilience of capital value and a promising income forecast.
`;

// TODO dynamic special temrs date
// TODO add to the block of opportunity 2:
// - {DETAILED_OPERATIONAL_STATISTICS}
// - {SHARES_PERCENTAGE_AND_VALUATION}
// {INCENTIVE_DETAILS} {COMPLETE_STRUCTURE_OF_INVESTMENT_AND_EXIT_STRATEGIES} into exclusive offer
// DETAILED_OPERATIONAL STATISTICS (Placeholder Information):

// Operational Capacity:

//     Total Number of Rooms: 60
//     Average Occupancy Rate: 72%
//     Average Daily Rate (ADR): €150
//     Revenue Per Available Room (RevPAR): €108

// Facilities Usage:

//     Restaurant Seat Turnover: 3 times per evening
//     Event Space Utilization: 40% of available days booked for events
//     Casino Foot Traffic: 350 visitors per day

// Operational Costs:

//     Average Monthly Operating Costs: €50,000
//     Staffing Costs: €20,000 per month
//     Utility Costs: €5,000 per month
//     Annual Maintenance and Upkeep: €100,000

// Revenue Streams:

//     Room Revenue: €1,944,000 per annum
//     F&B Revenue: €600,000 per annum
//     Event Space Revenue: €240,000 per annum
//     Casino Revenue: €1,200,000 per annum

// Net Operating Income (NOI):

//     Annual Gross Revenue: €3,984,000
//     Annual Operating Expenses: €900,000
//     Annual Net Operating Income: €3,084,000

// SHARES_PERCENTAGE_AND_VALUATION (Placeholder Information):

// Valuation:

//     Total Asset Valuation: €7,000,000
//     Current Market Capitalization: €10,000,000

// Share Offerings:

//     Minimum Investment Share: 0.5% per stake
//     Valuation per Share: €35,000
//     Number of Shares Available: 100 Shares (representing a 50% ownership stake in total)

// Investment Terms:

//     Projected Intrinsic Value Growth: 3% per annum
//     Share Transfer Terms: Shares can be transferred or sold after a minimum holding period of 5 years, subject to board approval
//     Dividend Distribution: Annual, based on net profits, expected to be about 5% ROI
//     Capital Gains Potential: Dependent on the market conditions and property appreciation

// Ownership Equity:

//     Investor Equity Stake: Ranges from 0.5% to 10% based on investment size
//     Residual Value Increment: Opportunity for increase in underlying equity value, reflecting property and business appreciation

const block1: Block = {
    body: body1,
    title: "Invest into the Managing Company",
    contentType: "opportunity",
    tag: "Opportunity #2",
    priceLabel: "No investment ceiling. We welcome all offers starting at",
    priceValue: "€ 200,000",
};

const asset: PropertyInfo = {
    slug: "house-of-benjamins",
    pageTitle: "House of Benjamins Hotel Opportunity | For Sale | Riga, Latvia",
    propTitle: "House of Benjamins",
    tagline: "Baptism Of Brilliance",
    addressShort: {
        flag: "🇱🇻",
        location: "12 Kr. Barona, Riga",
    },
    addressLong: "12 Krišjāņa Barona, Rīga",
    coords: { x: 24.120383, y: 56.950531 },
    // coords: [56.95043173818938, 24.120383710070335],
    heroHighlights: [
        "60 hotel rooms, restaurant, banquet halls, casino.",
        "Furniture & fixtures included.",
    ],
    descShort:
        "Buy Hotel Asset | Invest in Managing Company | Get EU Residency",
    descLong:
        "House of Benjamins Hotel, Riga: Ace Opportunity with Quorum Strategic Investments",
    featuredImage,
};

// TODO check for mobile layout
// TODO make tag into atom
// TODO unify buttonsconten
// TODO fetch from the bucket
// TODO generalise prop page
// TODO Content pages
// TODO improve print styles
// TODO Unify highligheted (bg-violet) texts
// TODO Share popover
// TODO other listing and contets
// TODO menu and footer with content and sections
// TODO coverage categroies to property list on 3D sphere
// TODO add secondary CTA in the end after map?

const PropertyPage: React.FC = () => {
    const albumStore = useAlbumStore();
    const shareStore = useShareStore();

    const router = useRouter();
    const slugToCheck = router.query.aboutSlug;
    const slug: string = typeof slugToCheck === "string" ? slugToCheck : "";
    const { data, isLoading, error } =
        trpc.properties.getPropertyBySlug.useQuery(slug);

    useEffect(() => {
        shareStore.setPropertyToShare({
            slug: asset.slug,
            title: asset.propTitle,
            address: asset.addressLong,
            imageSrc: img,
            descShort: asset.descShort,
            descLong: asset.descLong,
        });

        // We don't want the gallery state to persist (e.g. if I open gallery on prop A, close page,
        // then navigate to prop B, I wouldn't expect the gallery page to open there too);
        // To that end we need to make sure we enabled the scroll the on the :root back by calling reset()
        albumStore.reset();

        return () => {
            albumStore.reset();
            shareStore.reset();
        };
    }, []);

    if (!data) return <></>;
    return (
        <>
            <Head>
                <title>{data.pageTitle}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial Investment | EU Residency By Investment | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip bg-white text-black">
                {data.images.length && (
                    <Modal
                        open={albumStore.checkModalOpen()}
                        exiting={albumStore.exitingModal}
                        close={albumStore.closeModal}
                        toggleExitingAnimation={albumStore.toggleExiting}
                    >
                        <Gallery images={data.images} />
                    </Modal>
                )}
                <Modal
                    open={shareStore.dialogOpened}
                    exiting={shareStore.exitingModal}
                    close={shareStore.close}
                    toggleExitingAnimation={shareStore.toggleExiting}
                >
                    <ShareDialog />
                </Modal>
                <Header view="prop" />
                <AssetHero asset={data} />
                <div className="flex lg:hidden">
                    <CtaCard
                        view={ctaCardViewOptions.compact}
                        propTitle={data.propTitle}
                        contactName={data.contact}
                        opportunities={data.opportunities}
                        addressLong={data.addressLong}
                    />
                </div>

                <article
                    id="container"
                    className="mx-auto flex h-fit min-h-[35rem] w-full max-w-7xl flex-row"
                >
                    <section className="mb-[35rem] flex w-full flex-col gap-y-5 pt-10">
                        {data.content.map((block, index) => (
                            <TextBlock key={index} block={block} />
                        ))}
                        {data.images.length && <Album images={data.images} />}
                    </section>
                    <div className="sticky top-[41rem] z-[25] my-10 ml-auto hidden h-fit min-w-[21rem] translate-y-[-35rem] print:hidden lg:left-[50%] lg:flex ">
                        <CtaCard
                            propTitle={data.propTitle}
                            contactName={data.contact}
                            opportunities={data.opportunities}
                            addressLong={data.addressLong}
                        />
                    </div>
                </article>
                <div className="mx-auto mt-[-35rem] flex h-[35rem] w-full bg-blue-500">
                    <Map x={data.coords.x} y={data.coords.y} />
                </div>
                {/* <div className="bg-red mx-auto flex h-[20vh] w-full max-w-7xl flex-col">
                    You might be interested (content)
                </div>
                <div className="bg-red mx-auto flex h-[20vh] w-full max-w-7xl flex-col">
                    Similar opportinity
                </div> */}
                <div className="flex bg-black">
                    <Footer />
                </div>
            </main>
        </>
    );
};

export default PropertyPage;
