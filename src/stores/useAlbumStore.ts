import { create } from 'zustand';
import { devtools, persist } from 'zustand/middleware';
import { disableBodyScroll, enableBodyScroll } from '~/utils/common';

export const albumViewOptions = {
    none: "none",
    list: "list",
    closeup: "closeup",
} as const;
type AlbumView = typeof albumViewOptions[keyof typeof albumViewOptions];

interface AlbumProps {
    view: AlbumView;
    image: number;
    exitingModal: boolean;
};

interface AlbumReducers {
    setView: (view: AlbumView) => void;
    selectImage: (selectedImage: number) => void;
    showGalleryModal: () => void;
    closeModal: () => void;
    openCloseup: (selectedImage: number) => void;
    checkModalOpen: () => boolean;
    toggleExiting: () => void;
    reset: () => void;
};

type AlbumState = AlbumProps & AlbumReducers;

const defaults: AlbumProps = {
    view: albumViewOptions.none,
    image: 0,
    exitingModal: false,
};

export const useAlbumStore = create<AlbumState>()(
    devtools(
        (set, get) => ({
            ...defaults,
            reset: () => set((state) => {
                enableBodyScroll();
                return {
                    ...state,
                    ...defaults,
                };
            }),
            setView: (view) => set((state) => ({
                ...state,
                view,
            })),
            selectImage: (selectedImage) => set((state) => ({
                ...state,
                image: selectedImage,
            })),
            showGalleryModal: () => {
                disableBodyScroll();
                get().setView(albumViewOptions.list);
                return;
            },
            closeModal: () => {
                enableBodyScroll();
                get().setView(albumViewOptions.none);
                return;
            },
            checkModalOpen: () => (
                get().view !== albumViewOptions.none
            ),
            openCloseup: (selectedImage) => {
                disableBodyScroll();
                // TODO obvs change it to use image IDs rather than static image object when integrate the CMS
                get().selectImage(selectedImage);
                get().setView(albumViewOptions.closeup);
                return;
            },
            toggleExiting: () => set((state) => ({
                ...state,
                exitingModal: !state.exitingModal,
            })),
        }),
        {
            name: 'album-storage',
        }
    )
);
