import { type StaticImageData } from 'next/image';
import { create } from 'zustand';
import { devtools } from 'zustand/middleware';
import { ShareOption } from '~/components/common/ShareDialog';

type PropertyToShare = {
    imageSrc: string | StaticImageData | undefined;
    title: string | undefined;
    address: string | undefined;
    descShort: string | undefined;
    descLong: string;
    slug: string;
};

type ShareProps = {
    dialogOpened: boolean;
    exitingModal: boolean;
    shareOptions: ShareOption[];
} & PropertyToShare;

interface ShareReducers {
    reset: () => void;
    open: () => void;
    close: () => void;
    toggleExiting: () => void;
    setPropertyToShare: (data: PropertyToShare) => void;
    setShareOptions: (shareOptions: ShareOption[]) => void;
};

type ShareState = ShareProps & ShareReducers;

const defaults: ShareProps = {
    dialogOpened: false,
    exitingModal: false,
    imageSrc: undefined,
    title: undefined,
    address: undefined,
    descShort: undefined,
    descLong: "Ace Opportunity from Quorum Strategic Investments",
    slug: "",
    shareOptions: [],
};

export const useShareStore = create<ShareState>()(
    devtools(
        (set, get) => ({
            ...defaults,
            reset: () => set((state) => ({
                ...state,
                ...defaults,
            })),
            open: () => set((state) => ({
                ...state,
                dialogOpened: true,
            })),
            close: () => set((state) => ({
                ...state,
                dialogOpened: false,
            })),
            toggleExiting: () => set((state) => ({
                ...state,
                exitingModal: !state.exitingModal,
            })),
            setPropertyToShare: (data) => set((state) => ({
                ...state,
                ...data,
            })),
            setShareOptions: (shareOptions) => set((state) => ({
                ...state,
                shareOptions,
            })),
        }),
        {
            name: "share-storage",
        },
    ),
);
