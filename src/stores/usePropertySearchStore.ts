import { create } from 'zustand';
import { devtools, persist } from 'zustand/middleware';
import { type Filter, propertyFilters } from '~/types/properties';

type Props = {
    filters: Record<string, Filter>;
};

interface Reducers {
    reset: () => void;
    selectOption: (fieldId: string, optionValue: string) => void;
    deselectOption: (fieldId: string, optionValue: string) => void;
    setSelectedOptions: (fieldId: string, selectedOptions: string[]) => void;
};

type State = Props & Reducers;

const defaults: Props = {
    filters: propertyFilters,
};

export const usePropertySearchStore = create<State>()(
    devtools(
        (set, get) => ({
            ...defaults,
            reset: () => set((state) => ({
                ...state,
                ...defaults,
            })),
            selectOption: (fieldId, optionValue) =>
                set((state) => {
                    const fieldState = state.filters[fieldId];
                    if (!fieldState) return state;
                    if (fieldState.type !== "multi" && fieldState.type !== "select") return state;
                    return {
                        filters: {
                            ...state.filters,
                            [fieldId]: {
                                ...fieldState,
                                selectedOptions: Array.from(new Set([...fieldState.selectedOptions, optionValue])),
                            },
                        },
                    };
                }),
            deselectOption: (fieldId, optionValue) =>
                set((state) => {
                    const fieldState = state.filters[fieldId];
                    if (!fieldState) return state;
                    if (fieldState.type !== "multi" && fieldState.type !== "select") return state;
                    return {
                        filters: {
                            ...state.filters,
                            [fieldId]: {
                                ...fieldState,
                                selectedOptions: fieldState.selectedOptions.filter((option) => option !== optionValue),
                            },
                        },
                    };
                }),
            setSelectedOptions: (fieldId, selectedOptions) => set((state) => {
                const fieldState = state.filters[fieldId];
                if (!fieldState) return state;
                if (fieldState.type !== "multi" && fieldState.type !== "select") return state;
                return {
                    filters: {
                        ...state.filters,
                        [fieldId]: { 
                            ...fieldState,
                            selectedOptions,
                        },
                    },
                };
            }),
        }),
        {
            name: 'property-search-storage',
        }
    )
);
