import { create } from "zustand";
import { devtools } from "zustand/middleware";

import { NavGroup, navGroupLabels } from "~/types/nav";

type Props = {
    navMenuOpened: boolean;
    selectedNavGroup: NavGroup | undefined;
};

interface Reducers {
    reset: () => void;
    open: () => void;
    close: () => void;
    setSelectedNavGroup: (selectedNavGroup: NavGroup | undefined) => void;
}

type State = Props & Reducers;

const defaults: Props = {
    navMenuOpened: false,
    selectedNavGroup: undefined,
};

export const useNavMenuStore = create<State>()(
    devtools(
        (set, _get) => ({
            ...defaults,
            reset: () =>
                set((state) => ({
                    ...state,
                    ...defaults,
                })),
            open: () =>
                set((state) => ({
                    ...state,
                    navMenuOpened: true,
                })),
            close: () =>
                set((state) => ({
                    ...state,
                    navMenuOpened: false,
                })),
            setSelectedNavGroup: (selectedNavGroup) =>
                set((state) => ({
                    ...state,
                    selectedNavGroup,
                })),
        }),
        {
            name: "nav-menu-storage",
        },
    ),
);
