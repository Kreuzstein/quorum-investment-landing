const Echelons: React.FC = () => {
    return (
        <svg
            width="315"
            height="231"
            viewBox="0 0 315 231"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M34 7.82467L82.4746 83.5797V230L34 154.245V7.82467Z"
                fill="white"
            />
            <path
                d="M34 7.82467L82.4746 83.5797M34 7.82467V154.245L82.4746 230M34 7.82467L51.1525 1L100 76.1346M82.4746 83.5797V230M82.4746 83.5797L100 76.1346M82.4746 230L100 223.175V76.1346"
                stroke="url(#paint0_linear_2689_361)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M0 120H62"
                stroke="url(#paint1_linear_2689_361)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <path
                d="M256 120H280H311M311 120L275 92M311 120L275 149"
                stroke="url(#paint2_linear_2689_361)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <path
                d="M112 7.82467L160.475 83.5797V230L112 154.245V7.82467Z"
                fill="white"
            />
            <path
                d="M112 7.82467L160.475 83.5797M112 7.82467V154.245L160.475 230M112 7.82467L129.153 1L178 76.1346M160.475 83.5797V230M160.475 83.5797L178 76.1346M160.475 230L178 223.175V76.1346"
                stroke="url(#paint3_linear_2689_361)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M100 120H138"
                stroke="url(#paint4_linear_2689_361)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <path
                d="M190 7.82467L238.475 83.5797V230L190 154.245V7.82467Z"
                fill="white"
            />
            <path
                d="M190 7.82467L238.475 83.5797M190 7.82467V154.245L238.475 230M190 7.82467L207.153 1L256 76.1346M238.475 83.5797V230M238.475 83.5797L256 76.1346M238.475 230L256 223.175V76.1346"
                stroke="url(#paint5_linear_2689_361)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M178 120H216"
                stroke="url(#paint6_linear_2689_361)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_2689_361"
                    x1="97.3545"
                    y1="109.556"
                    x2="7.08675"
                    y2="118.881"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_2689_361"
                    x1="-2.45374e-08"
                    y1="60"
                    x2="83.1676"
                    y2="59.341"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" stopOpacity="0" />
                    <stop
                        offset="0.647616"
                        stopColor="#C362FF"
                        stopOpacity="0.42"
                    />
                    <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_2689_361"
                    x1="235"
                    y1="61"
                    x2="335.519"
                    y2="60.3294"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0" />
                    <stop offset="1" stopColor="#C362FF" stopOpacity="0.42" />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_2689_361"
                    x1="178"
                    y1="253.311"
                    x2="249.21"
                    y2="12.8341"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint4_linear_2689_361"
                    x1="154"
                    y1="90"
                    x2="90.0903"
                    y2="74.7155"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" stopOpacity="0" />
                    <stop
                        offset="0.275485"
                        stopColor="#00FFA3"
                        stopOpacity="0.45"
                    />
                    <stop
                        offset="0.698708"
                        stopColor="#C362FF"
                        stopOpacity="0.42"
                    />
                </linearGradient>
                <linearGradient
                    id="paint5_linear_2689_361"
                    x1="253.354"
                    y1="109.556"
                    x2="184.372"
                    y2="129.922"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#ABFFE1" />
                </linearGradient>
                <linearGradient
                    id="paint6_linear_2689_361"
                    x1="157.5"
                    y1="113.5"
                    x2="228.76"
                    y2="126.232"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" stopOpacity="0" />
                    <stop
                        offset="0.375799"
                        stopColor="#00FFA3"
                        stopOpacity="0.45"
                    />
                    <stop
                        offset="0.826149"
                        stopColor="#C362FF"
                        stopOpacity="0.42"
                    />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default Echelons;
