const Balance: React.FC = () => {
    return (
        <svg
            width="356"
            height="362"
            viewBox="0 0 356 362"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M36.7768 185.533L257.168 35.5282L295.594 261.664L36.7768 185.533Z"
                stroke="url(#paint0_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                opacity="0.8"
                d="M49.4406 207.097L263.659 41.8731L310.883 265.01L49.4406 207.097Z"
                stroke="url(#paint1_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                opacity="0.6"
                d="M62.4476 228.557L269.906 48.5684L325.798 268.094L62.4476 228.557Z"
                stroke="url(#paint2_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                opacity="0.4"
                d="M75.7964 249.887L275.926 55.6255L340.333 270.939L75.7964 249.887Z"
                stroke="url(#paint3_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                opacity="0.1"
                d="M89.4775 271.069L281.73 63.0678L354.476 273.579L89.4775 271.069Z"
                stroke="url(#paint4_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M24.3486 163.892L250.163 29.5137L279.666 258.032L24.3486 163.892Z"
                fill="white"
                stroke="url(#paint5_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M1 135.874L226.814 1.49609L250.363 28.5805L279.316 257.864L25.7496 164.47L1 135.874Z"
                fill="white"
            />
            <path
                d="M226.814 1.49609L1 135.874M226.814 1.49609L250.363 222.841M226.814 1.49609L250.363 28.5805L279.316 257.864M1 135.874L250.363 222.841M1 135.874L25.7496 164.47L279.316 257.864M250.363 222.841L279.316 257.864"
                stroke="url(#paint6_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M112.14 118.707L187.322 73.4102V94.4239V146.725L133.62 126.712L112.14 118.707Z"
                fill="white"
            />
            <path
                d="M112.14 118.707L187.322 73.4102V94.4239V146.725L133.62 126.712L112.14 118.707Z"
                fill="url(#paint7_linear_1537_2365)"
            />
            <path
                d="M187.322 94.4239V146.725L133.62 126.712M187.322 94.4239V73.4102L112.14 118.707L133.62 126.712M187.322 94.4239L133.62 126.712"
                stroke="url(#paint8_linear_1537_2365)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_1537_2365"
                    x1="339.832"
                    y1="116.569"
                    x2="4.60762"
                    y2="296.162"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_1537_2365"
                    x1="349.381"
                    y1="117.018"
                    x2="18.5498"
                    y2="314.643"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_1537_2365"
                    x1="358.451"
                    y1="117.611"
                    x2="32.9714"
                    y2="333.848"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_1537_2365"
                    x1="367.052"
                    y1="118.377"
                    x2="48.0363"
                    y2="353.782"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint4_linear_1537_2365"
                    x1="375.187"
                    y1="119.356"
                    x2="63.9148"
                    y2="374.437"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint5_linear_1537_2365"
                    x1="329.49"
                    y1="116.23"
                    x2="-9.17433"
                    y2="278.2"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint6_linear_1537_2365"
                    x1="306.141"
                    y1="88.2128"
                    x2="-32.523"
                    y2="250.183"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint7_linear_1537_2365"
                    x1="145.845"
                    y1="73.4102"
                    x2="145.845"
                    y2="132.143"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint8_linear_1537_2365"
                    x1="194.569"
                    y1="98.2089"
                    x2="101.212"
                    y2="140.385"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default Balance;
