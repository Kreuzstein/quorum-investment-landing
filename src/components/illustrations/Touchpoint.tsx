const Touchpoint: React.FC = () => {
    return (
        <svg
            width="336"
            height="337"
            viewBox="0 0 336 337"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M150.275 179.321L39.9551 165.386L32.714 133.547L53.2961 112.964L113.427 119.972L82.9527 43.7264L102.782 23.8973L140.154 14.0815L185.67 140.42L170.355 159.241L150.275 179.321Z"
                fill="white"
            />
            <path
                d="M150.275 179.321L39.9551 165.386L32.714 133.547L53.2961 112.964L113.427 119.972L82.9527 43.7264L102.782 23.8973L140.154 14.0815L185.67 140.42L170.355 159.241L150.275 179.321Z"
                fill="url(#paint0_linear_2689_362)"
            />
            <path
                d="M150.275 179.321L39.9551 165.386L32.714 133.547M150.275 179.321L170.355 159.241L185.67 140.42M150.275 179.321L166.343 159.747M185.67 140.42L140.154 14.0815M185.67 140.42L166.343 159.747M140.154 14.0815L102.782 23.8973L82.9527 43.7264M140.154 14.0815L120.325 33.9106M82.9527 43.7264L113.427 119.972M82.9527 43.7264L120.325 33.9106M32.714 133.547L122.727 143.239L113.427 119.972M32.714 133.547L53.2961 112.964L113.427 119.972M120.325 33.9106L166.343 159.747"
                stroke="url(#paint1_linear_2689_362)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                d="M194.27 161.538L297.189 175.194L304.285 206.394L284.116 226.563L225.191 219.697L255.054 294.412L235.623 313.843L199.001 323.462L160.8 208.715L173.338 180.72L194.27 161.538Z"
                fill="white"
            />
            <path
                d="M199.001 323.462L160.8 208.715L173.338 180.72M199.001 323.462L235.623 313.843L255.054 294.412M199.001 323.462L218.432 304.031M255.054 294.412L225.191 219.697M255.054 294.412L218.432 304.031M304.285 206.394L216.078 196.896L225.191 219.697M304.285 206.394L297.189 175.194L194.27 161.538L173.338 180.72M304.285 206.394L284.116 226.563L225.191 219.697M218.432 304.031L173.338 180.72"
                stroke="url(#paint2_linear_2689_362)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_2689_362"
                    x1="51.3536"
                    y1="176.784"
                    x2="177.105"
                    y2="51.0325"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_2689_362"
                    x1="23.1146"
                    y1="42.3534"
                    x2="248.853"
                    y2="150.491"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_2689_362"
                    x1="313.692"
                    y1="295.757"
                    x2="92.484"
                    y2="189.79"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default Touchpoint;
