const Orbit: React.FC = () => {
    return (
        <svg
            width="231"
            height="231"
            viewBox="0 0 231 231"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <circle
                cx="115.5"
                cy="115.5"
                r="20.5"
                fill="url(#paint0_radial_659_1385)"
            />
            <circle
                cx="115.501"
                cy="115.501"
                r="88.5142"
                stroke="#00FFA3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="121.995"
                cy="121.995"
                r="88.5142"
                stroke="#00FFA3"
                stroke-opacity="0.6"
                strokeLinejoin="bevel"
            />
            <circle
                cx="128.495"
                cy="128.495"
                r="88.5142"
                stroke="#00FFA3"
                stroke-opacity="0.5"
                strokeLinejoin="bevel"
            />
            <circle
                cx="134.991"
                cy="134.991"
                r="88.5142"
                stroke="#00FFA3"
                stroke-opacity="0.3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="141.487"
                cy="141.487"
                r="88.5142"
                stroke="#00FFA3"
                stroke-opacity="0.1"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(-1 0 0 1 204.014 26.9863)"
                stroke="#00FFA3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(-1 0 0 1 197.52 33.4805)"
                stroke="#00FFA3"
                stroke-opacity="0.6"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(-1 0 0 1 191.02 39.9805)"
                stroke="#00FFA3"
                stroke-opacity="0.5"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(-1 0 0 1 184.523 46.4766)"
                stroke="#00FFA3"
                stroke-opacity="0.3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(-1 0 0 1 178.027 52.9727)"
                stroke="#00FFA3"
                stroke-opacity="0.1"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(1 0 0 -1 26.9863 204.014)"
                stroke="#00FFA3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(1 0 0 -1 33.4805 197.52)"
                stroke="#00FFA3"
                stroke-opacity="0.6"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(1 0 0 -1 39.9805 191.02)"
                stroke="#00FFA3"
                stroke-opacity="0.5"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(1 0 0 -1 46.4766 184.523)"
                stroke="#00FFA3"
                stroke-opacity="0.3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="88.5142"
                cy="88.5142"
                r="88.5142"
                transform="matrix(1 0 0 -1 52.9727 178.027)"
                stroke="#00FFA3"
                stroke-opacity="0.1"
                strokeLinejoin="bevel"
            />
            <circle
                cx="115.499"
                cy="115.499"
                r="88.5142"
                transform="rotate(180 115.499 115.499)"
                stroke="#00FFA3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="109.005"
                cy="109.005"
                r="88.5142"
                transform="rotate(180 109.005 109.005)"
                stroke="#00FFA3"
                stroke-opacity="0.6"
                strokeLinejoin="bevel"
            />
            <circle
                cx="102.505"
                cy="102.505"
                r="88.5142"
                transform="rotate(180 102.505 102.505)"
                stroke="#00FFA3"
                stroke-opacity="0.5"
                strokeLinejoin="bevel"
            />
            <circle
                cx="96.0093"
                cy="96.0093"
                r="88.5142"
                transform="rotate(180 96.0093 96.0093)"
                stroke="#00FFA3"
                stroke-opacity="0.3"
                strokeLinejoin="bevel"
            />
            <circle
                cx="89.5132"
                cy="89.5132"
                r="88.5142"
                transform="rotate(180 89.5132 89.5132)"
                stroke="#00FFA3"
                stroke-opacity="0.1"
                strokeLinejoin="bevel"
            />
            <defs>
                <radialGradient
                    id="paint0_radial_659_1385"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(95 132) rotate(-42.0263) scale(54.5206 44.9523)"
                >
                    <stop offset="0.25304" stopColor="#00FFA3" />
                    <stop offset="0.757025" stopColor="#9E00FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </radialGradient>
            </defs>
        </svg>
    );
};

export default Orbit;
