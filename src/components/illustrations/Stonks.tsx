const Stonks: React.FC = () => {
  return (
    <svg
      viewBox="0 0 247 264"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.6"
        d="M208.524 12H149V197.65M208.524 12V197.65M208.524 12L225 29.4047V214M208.524 197.65H149M208.524 197.65L225 214M149 197.65L165.476 214H225"
        stroke="url(#paint0_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        opacity="0.4"
        d="M168 250V97.3684L151.524 80H92V233.684L108.476 250H168ZM168 250L134.517 217.368"
        stroke="url(#paint1_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        opacity="0.4"
        d="M235 224V39.1282L214.085 18L160.229 21.6974L155 202.872L176.438 224H235ZM235 224L214.085 202.872"
        stroke="url(#paint2_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        opacity="0.2"
        d="M246 235V50.1282L224.824 29L170.294 32.6974L165 213.872L186.706 235H246ZM246 235L210.529 200.138"
        stroke="url(#paint3_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <g opacity="0.6">
        <path d="M10 117H68.7413V239.616H10V117Z" fill="#00FFA3" opacity="0.25" />
        <path
          d="M68.7413 117H10V239.616M68.7413 117V239.616M68.7413 117L85 134.441V256M68.7413 239.616H10M68.7413 239.616L85 256M10 239.616L26.2587 256H85"
          stroke="url(#paint4_linear_1698_394)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <g opacity="0.4">
        <path d="M17 124H76.5245V246.616H17V124Z" fill="#00FFA3" opacity="0.25" />
        <path
          d="M76.5245 124H17V246.616M76.5245 124V246.616M76.5245 124L93 141.441V263M76.5245 246.616H17M76.5245 246.616L93 263M17 246.616L33.4755 263H93"
          stroke="url(#paint5_linear_1698_394)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <path
        d="M78 124.472L60.4615 106V230.639H2L20.0699 247H78V124.472Z"
        fill="#00FFA3" opacity="0.25"
      />
      <path
        d="M78 124.472L60.4615 106V230.639H2L20.0699 247H78V124.472Z"
        fill="url(#paint6_linear_1698_394)"
      />
      <g opacity="0.6">
        <path d="M82 69H140.741V223.588H82V69Z" fill="#00FFA3" opacity="0.25" />
        <path
          d="M140.741 69H82V223.588M140.741 69V223.588M140.741 69L157 86.4706V240M140.741 223.588H82M140.741 223.588L157 240M82 223.588L98.2587 240H157"
          stroke="url(#paint7_linear_1698_394)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <path d="M1 108H60.5245V230.616H1V108Z" fill="#00FFA3" opacity="0.25" />
      <path
        d="M60.5245 108H1V230.616M60.5245 108V230.616M60.5245 108L77 125.441V247M60.5245 230.616H1M60.5245 230.616L77 247M1 230.616L17.4755 247H77"
        stroke="url(#paint8_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M147 76.4722L129.692 58V212.639H72L89.8322 229H147V76.4722Z"
        fill="#00FFA3" opacity="0.25"
      />
      <path
        d="M147 76.4722L129.692 58V212.639H72L89.8322 229H147V76.4722Z"
        fill="url(#paint9_linear_1698_394)"
      />
      <path d="M71 59H130.524V212.684H71V59Z" fill="#00FFA3" opacity="0.25" />
      <path
        d="M130.524 59H71V212.684M130.524 59V212.684M130.524 59L147 76.3684V229M130.524 212.684H71M130.524 212.684L147 229M71 212.684L87.4755 229H147"
        stroke="url(#paint10_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M215 19.4115L197.462 1V186.693H139L157.07 203H215V19.4115Z"
        fill="#00FFA3" opacity="0.25"
      />
      <path
        d="M215 19.4115L197.462 1V186.693H139L157.07 203H215V19.4115Z"
        fill="url(#paint11_linear_1698_394)"
      />
      <path d="M139 1H197.741V186.65H139V1Z" fill="#00FFA3" opacity="0.25" />
      <path
        d="M197.741 1H139V186.65M197.741 1V186.65M197.741 1L214 18.4047V203M197.741 186.65H139M197.741 186.65L214 203M139 186.65L155.259 203H214"
        stroke="url(#paint12_linear_1698_394)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1698_394"
          x1="230.378"
          y1="82.4492"
          x2="122.071"
          y2="99.4535"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1698_394"
          x1="173.378"
          y1="118.723"
          x2="65.0815"
          y2="135.761"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1698_394"
          x1="240.291"
          y1="92.2521"
          x2="133.645"
          y2="108.7"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1698_394"
          x1="251.357"
          y1="103.252"
          x2="143.441"
          y2="120.104"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1698_394"
          x1="90.3074"
          y1="124.174"
          x2="-16.6528"
          y2="140.711"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1698_394"
          x1="98.3781"
          y1="131.174"
          x2="-9.9403"
          y2="148.145"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1698_394"
          x1="71.8398"
          y1="47.7395"
          x2="71.8398"
          y2="152.843"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1698_394"
          x1="162.307"
          y1="107.951"
          x2="55.3388"
          y2="124.462"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1698_394"
          x1="82.3781"
          y1="115.174"
          x2="-25.9403"
          y2="132.145"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint9_linear_1698_394"
          x1="140.921"
          y1="29.7395"
          x2="140.921"
          y2="134.843"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint10_linear_1698_394"
          x1="152.378"
          y1="97.7234"
          x2="44.0815"
          y2="114.761"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint11_linear_1698_394"
          x1="208.84"
          y1="4.39496"
          x2="208.84"
          y2="109.152"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint12_linear_1698_394"
          x1="219.307"
          y1="71.4492"
          x2="112.358"
          y2="88.0194"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Stonks;
