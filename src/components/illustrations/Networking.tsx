const Networking: React.FC = () => {
  return (
    <svg
      width="302"
      height="296"
      viewBox="0 0 302 296"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g opacity="0.6">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M97.4323 145.758L123.288 158.162L172.49 181.765L177.53 132.509L180.32 107.178L97.4323 145.758ZM30.2001 127.047L188.029 64.7075L224.46 82.1843L211.845 211.154L177.111 236.849L31.8969 170.219L30.2001 127.047Z"
          fill="white"
        />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M97.4323 145.758L123.288 158.162L172.49 181.765L177.53 132.509L180.32 107.178L97.4323 145.758ZM30.2001 127.047L188.029 64.7075L224.46 82.1843L211.845 211.154L177.111 236.849L31.8969 170.219L30.2001 127.047Z"
          fill="url(#paint0_linear_1698_396)"
        />
        <path
          d="M224.46 82.1843L188.029 64.7075L30.2001 127.047L31.8969 170.219M224.46 82.1843L211.845 211.154M224.46 82.1843L242.361 109.415L229.43 239.926M211.845 211.154L177.111 236.849M211.845 211.154L229.43 239.926M177.111 236.849L31.8969 170.219M177.111 236.849L194.321 265.432M31.8969 170.219L50.4839 199.462L194.321 265.432M229.43 239.926L194.321 265.432M177.53 132.509L180.32 107.178L97.4323 145.758L123.288 158.162M177.53 132.509L172.49 181.765L123.288 158.162M177.53 132.509L123.288 158.162"
          stroke="url(#paint1_linear_1698_396)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <g opacity="0.4">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M103.187 155.348L129.043 167.752L178.245 191.355L183.285 142.099L186.074 116.768L103.187 155.348ZM35.955 136.637L193.784 74.2974L230.215 91.7741L217.6 220.744L182.866 246.439L37.6518 179.808L35.955 136.637Z"
          fill="white"
        />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M103.187 155.348L129.043 167.752L178.245 191.355L183.285 142.099L186.074 116.768L103.187 155.348ZM35.955 136.637L193.784 74.2974L230.215 91.7741L217.6 220.744L182.866 246.439L37.6518 179.808L35.955 136.637Z"
          fill="url(#paint2_linear_1698_396)"
        />
        <path
          d="M230.215 91.7741L193.784 74.2974L35.955 136.637L37.6518 179.808M230.215 91.7741L217.6 220.744M230.215 91.7741L248.116 119.005L235.185 249.516M217.6 220.744L182.866 246.439M217.6 220.744L235.185 249.516M182.866 246.439L37.6518 179.808M182.866 246.439L200.076 275.021M37.6518 179.808L56.2388 209.052L200.076 275.021M235.185 249.516L200.076 275.021M183.285 142.099L186.074 116.768L103.187 155.348L129.043 167.752M183.285 142.099L178.245 191.355L129.043 167.752M183.285 142.099L129.043 167.752"
          stroke="url(#paint3_linear_1698_396)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <g opacity="0.2">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M108.942 164.94L134.798 177.344L183.999 200.947L189.039 151.691L191.829 126.36L108.942 164.94ZM41.7099 146.229L199.539 83.8892L235.97 101.366L223.355 230.335L188.621 256.03L43.4067 189.4L41.7099 146.229Z"
          fill="white"
        />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M108.942 164.94L134.798 177.344L183.999 200.947L189.039 151.691L191.829 126.36L108.942 164.94ZM41.7099 146.229L199.539 83.8892L235.97 101.366L223.355 230.335L188.621 256.03L43.4067 189.4L41.7099 146.229Z"
          fill="url(#paint4_linear_1698_396)"
        />
        <path
          d="M235.97 101.366L199.539 83.8892L41.7099 146.229L43.4067 189.4M235.97 101.366L223.355 230.335M235.97 101.366L253.871 128.596L240.94 259.108M223.355 230.335L188.621 256.03M223.355 230.335L240.94 259.108M188.621 256.03L43.4067 189.4M188.621 256.03L205.831 284.613M43.4067 189.4L61.9937 218.644L205.831 284.613M240.94 259.108L205.831 284.613M189.039 151.691L191.829 126.36L108.942 164.94L134.798 177.344M189.039 151.691L183.999 200.947L134.798 177.344M189.039 151.691L134.798 177.344"
          stroke="url(#paint5_linear_1698_396)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <path
        d="M174.6 93.6913L25 158.742L43.8 188.456L188.367 257L223.634 230.401L237 99.7145L218.2 70L206.367 201.586L171.167 225.679L117 146.696L170.2 120.193L174.6 93.6913Z"
        fill="white"
      />
      <path
        d="M174.6 93.6913L25 158.742L43.8 188.456L188.367 257L223.634 230.401L237 99.7145L218.2 70L206.367 201.586L171.167 225.679L117 146.696L170.2 120.193L174.6 93.6913Z"
        fill="url(#paint6_linear_1698_396)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M91.6774 136.165L117.533 148.568L166.735 172.172L171.775 122.915L174.565 97.5843L91.6774 136.165ZM24.4452 117.453L182.274 55.1138L218.705 72.5905L206.09 201.56L171.356 227.255L26.1421 160.625L24.4452 117.453Z"
        fill="white"
      />
      <path
        d="M218.705 72.5905L182.274 55.1138L24.4452 117.453L26.1421 160.625M218.705 72.5905L206.09 201.56M218.705 72.5905L236.606 99.8211L223.675 230.333M206.09 201.56L171.356 227.255M206.09 201.56L223.675 230.333M171.356 227.255L26.1421 160.625M171.356 227.255L188.566 255.838M26.1421 160.625L44.729 189.869L188.566 255.838M223.675 230.333L188.566 255.838M171.775 122.915L174.565 97.5843L91.6774 136.165L117.533 148.568M171.775 122.915L166.735 172.172L117.533 148.568M171.775 122.915L117.533 148.568"
        stroke="url(#paint7_linear_1698_396)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M162 79.1739L151.149 146L77.1178 117.693L40 41.0171L122.5 1.5L162 79.1739Z"
        fill="white"
      />
      <path
        d="M151.149 146L162 79.1739L122.5 1.5M151.149 146L77.1178 117.693L40 41.0171M151.149 146L111.254 67.0854M111.254 67.0854L122.5 1.5M111.254 67.0854L40 41.0171M122.5 1.5L40 41.0171"
        stroke="url(#paint8_linear_1698_396)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1698_396"
          x1="189.027"
          y1="65.1864"
          x2="118.671"
          y2="211.846"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1698_396"
          x1="311.089"
          y1="199.917"
          x2="-26.5035"
          y2="222.193"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1698_396"
          x1="194.782"
          y1="74.7763"
          x2="124.426"
          y2="221.436"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1698_396"
          x1="316.844"
          y1="209.507"
          x2="-20.7487"
          y2="231.783"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1698_396"
          x1="200.537"
          y1="84.3681"
          x2="130.181"
          y2="231.028"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1698_396"
          x1="322.599"
          y1="219.099"
          x2="-14.9938"
          y2="241.375"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1698_396"
          x1="80.4418"
          y1="70"
          x2="80.4418"
          y2="163.068"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1698_396"
          x1="305.334"
          y1="190.324"
          x2="-32.2584"
          y2="212.599"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1698_396"
          x1="187.682"
          y1="21.8524"
          x2="-12.5276"
          y2="75.3372"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Networking;
