const Reinforce: React.FC = () => {
    return (
        <svg
            width="233"
            height="235"
            viewBox="0 0 233 235"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M1 23.4L23.4 1H118.6V45.8H45.8V118.6H1V23.4Z"
                fill="white"
            />
            <path
                d="M118.6 1H23.4L1 23.4V118.6M118.6 1V45.8M118.6 1L141 23.4V69.32M118.6 45.8H45.8M118.6 45.8L141 69.32M45.8 45.8V118.6M45.8 45.8L69.32 69.32M45.8 118.6H1M45.8 118.6L69.32 141M1 118.6L23.4 141H69.32M141 69.32H69.32M69.32 69.32V141"
                stroke="url(#paint0_linear_2689_360)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M232 211.6L209.6 234H114.4V189.2H187.2V116.4H232V211.6Z"
                fill="white"
            />
            <path
                d="M114.4 234H209.6L232 211.6V116.4M114.4 234V189.2M114.4 234L92 211.6V165.68M114.4 189.2H187.2M114.4 189.2L92 165.68M187.2 189.2V116.4M187.2 189.2L163.68 165.68M187.2 116.4H232M187.2 116.4L163.68 94M232 116.4L209.6 94H163.68M92 165.68H163.68M163.68 165.68V94"
                stroke="url(#paint1_linear_2689_360)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <circle
                cx="116.5"
                cy="118.5"
                r="20.5"
                fill="url(#paint2_radial_2689_360)"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_2689_360"
                    x1="183.232"
                    y1="60.1119"
                    x2="-34.2498"
                    y2="155.057"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_2689_360"
                    x1="49.7683"
                    y1="174.888"
                    x2="267.25"
                    y2="79.9431"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <radialGradient
                    id="paint2_radial_2689_360"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(96 135) rotate(-42.0263) scale(54.5206 44.9523)"
                >
                    <stop offset="0.161458" stopColor="#AAFFE0" />
                    <stop offset="0.65625" stopColor="#E6BCFF" />
                    <stop offset="1" stopColor="white" />
                </radialGradient>
            </defs>
        </svg>
    );
};

export default Reinforce;
