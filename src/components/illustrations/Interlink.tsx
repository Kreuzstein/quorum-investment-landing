const Interlink: React.FC = () => {
    return (
        <svg
            width="603"
            height="586"
            viewBox="0 0 603 586"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M262.407 491.929L219.908 447.85L361.004 281.22M262.407 491.929L427.469 297.772L361.004 281.22M262.407 491.929L327.323 519.319L520.326 296.125L506.771 244.2M506.771 244.2L462.323 199.053L193.379 120.217M506.771 244.2L239.055 162.619M193.379 120.217L164.309 197.981L209.106 243.866M193.379 120.217L239.055 162.619M209.106 243.866L361.004 281.22M209.106 243.866L239.055 162.619"
                stroke="url(#paint0_linear_2006_419)"
                strokeLinejoin="bevel"
            />
            <path
                d="M355.29 167.039L290.912 148.877L339.591 91.6433L381.657 135.844L355.29 167.039Z"
                fill="white"
            />
            <path
                d="M355.29 167.039L290.912 148.877L339.591 91.6433L381.657 135.844L355.29 167.039Z"
                fill="url(#paint1_linear_2006_419)"
            />
            <path
                d="M262.209 492.474L219.893 448.03L297.561 355.662L347.402 391.124L262.209 492.474Z"
                fill="white"
            />
            <path
                d="M262.209 492.474L219.893 448.03L297.561 355.662L347.402 391.124L262.209 492.474Z"
                fill="url(#paint2_linear_2006_419)"
            />
            <path
                d="M274.177 65.2252L338.936 92.4922L381.295 136.461L276.981 260.56L428.087 297.981L472.737 343.751L443.609 421.47L175.264 343.105L130.962 298.072L117.505 246.224L274.177 65.2252Z"
                fill="white"
            />
            <path
                d="M338.936 92.4922L274.177 65.2252L117.505 246.224L130.962 298.072M338.936 92.4922L381.295 136.461L276.981 260.56M338.936 92.4922L209.436 243.674L276.981 260.56M130.962 298.072L175.264 343.105L443.609 421.47M130.962 298.072L398.076 379.179M443.609 421.47L472.737 343.751L428.087 297.981M443.609 421.47L398.076 379.179M428.087 297.981L276.981 260.56M428.087 297.981L398.076 379.179"
                stroke="url(#paint3_linear_2006_419)"
                strokeLinejoin="bevel"
            />
            <path
                d="M164.08 198.153L170.25 182.605L215.428 126.328L356.567 167.752L281.78 262.875L209.014 243.674L164.08 198.153Z"
                fill="white"
            />
            <path
                d="M506.653 244.101L238.51 163.038L208.472 243.691L163.87 198.428L193.191 120.456L290.799 149.001L375.852 173.47L460.906 197.938L506.653 244.101Z"
                fill="white"
            />
            <path
                d="M506.653 244.101L238.51 163.038L208.472 243.691L163.87 198.428L193.191 120.456L290.799 149.001L375.852 173.47L460.906 197.938L506.653 244.101Z"
                fill="url(#paint4_linear_2006_419)"
            />
            <path
                d="M327.067 519.141L262.151 491.751L219.653 447.672L360.748 281.041L208.85 243.688L164.053 197.803L193.124 120.038L462.068 198.875L506.515 244.022L520.07 295.947L327.067 519.141Z"
                fill="white"
            />
            <path
                d="M262.151 491.751L327.067 519.141L520.07 295.947L506.515 244.022M262.151 491.751L219.653 447.672L360.748 281.041M262.151 491.751L427.213 297.594L360.748 281.041M506.515 244.022L462.068 198.875L193.124 120.038M506.515 244.022L238.799 162.441M193.124 120.038L164.053 197.803L208.85 243.688M193.124 120.038L238.799 162.441M208.85 243.688L360.748 281.041M208.85 243.688L238.799 162.441"
                stroke="url(#paint5_linear_2006_419)"
                strokeLinejoin="bevel"
            />
            <path
                d="M472.908 343.534L443.915 421.08L221.19 356.02L276.788 260.549L428.245 297.783L472.908 343.534Z"
                fill="white"
            />
            <path
                d="M399.213 379.953L131.617 297.687L174.978 342.879L443.674 421.385L472.879 343.512L428.146 297.704L399.213 379.953Z"
                fill="white"
            />
            <path
                d="M399.213 379.953L131.617 297.687L174.978 342.879L443.674 421.385L472.879 343.512L428.146 297.704L399.213 379.953Z"
                fill="url(#paint6_linear_2006_419)"
            />
            <path
                d="M443.693 421.362L174.761 342.71L130.343 297.573L398.045 378.972M443.693 421.362L472.838 343.522L428.072 297.647M443.693 421.362L398.045 378.972M428.072 297.647L276.174 260.402L209.709 243.898M428.072 297.647L398.045 378.972"
                stroke="url(#paint7_linear_2006_419)"
                strokeLinejoin="bevel"
            />
            <path
                d="M191.683 316.029L358.477 279.994L209.841 243.534L192.95 227L119.477 253.285L131.3 297.799L174.793 343.162L226.309 358L191.683 316.029Z"
                fill="url(#paint8_linear_2006_419)"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_2006_419"
                    x1="251.653"
                    y1="411.612"
                    x2="394.46"
                    y2="206.32"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="0.553581" stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_2006_419"
                    x1="322.767"
                    y1="108.361"
                    x2="390.343"
                    y2="125.071"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_2006_419"
                    x1="283.689"
                    y1="373.895"
                    x2="351.645"
                    y2="390.657"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_2006_419"
                    x1="385.854"
                    y1="130.484"
                    x2="243.085"
                    y2="335.798"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="0.553581" stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint4_linear_2006_419"
                    x1="236.493"
                    y1="65.3797"
                    x2="314.523"
                    y2="126.728"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint5_linear_2006_419"
                    x1="251.403"
                    y1="411.423"
                    x2="394.21"
                    y2="206.131"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="0.553581" stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint6_linear_2006_419"
                    x1="152.518"
                    y1="271.103"
                    x2="419.888"
                    y2="379.4"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint7_linear_2006_419"
                    x1="385.661"
                    y1="129.831"
                    x2="242.687"
                    y2="335.409"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="0.553581" stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint8_linear_2006_419"
                    x1="271.913"
                    y1="228.696"
                    x2="136.387"
                    y2="332.888"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.08" />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default Interlink;
