const Revelation: React.FC = () => {
    return (
        <svg
            width="318"
            height="400"
            viewBox="0 0 418 500"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M276.733 357.71L226.916 324.913L213.329 281.3L295.893 206.984L144.177 117.49L132.507 64.4562L189.292 2.00226L404.76 146.449L416.256 192.679L409.637 247.108L276.733 357.71Z"
                fill="white"
            />
            <path
                d="M181.627 360.052L231.445 392.872L409.637 247.278L416.256 192.811M181.627 360.052L168.041 316.409L295.893 207.126M181.627 360.052L335.434 230.693L295.893 207.126M295.893 207.126L144.178 117.57M144.178 117.57L132.507 64.4993L189.292 2.00195M144.178 117.57L204.794 54.8977M189.292 2.00195L404.76 146.549L416.256 192.811M189.292 2.00195L204.794 54.8977M416.256 192.811L204.794 54.8977"
                stroke="url(#paint0_linear_2223_1954)"
                strokeWidth="2"
            />
            <path
                d="M143.211 143.138L187.506 107.13L237.533 140.181L250.387 184.016L227.285 202.803H138.694L130.183 153.749L143.211 143.138Z"
                fill="white"
            />
            <path
                d="M131.244 152.104L181.205 184.964L194.831 228.659L122.51 292.63L274.663 382.294L286.367 435.428L229.419 498L13.3302 353.28L1.80078 306.962L8.43892 252.43L131.244 152.104Z"
                fill="white"
            />
            <path
                d="M237.105 139.95L187.145 107.13L8.43892 252.724L1.80078 307.191M237.105 139.95L250.731 183.593L122.51 292.876M237.105 139.95L82.8559 269.309L122.51 292.876M122.51 292.876L274.663 382.432M274.663 382.432L286.367 435.503L229.419 498M274.663 382.432L213.872 445.104M229.419 498L13.3302 353.453L1.80078 307.191M229.419 498L213.872 445.104M1.80078 307.191L213.872 445.104"
                stroke="url(#paint1_linear_2223_1954)"
                strokeWidth="2"
            />
            <g filter="url(#filter0_f_2223_1954)">
                <ellipse
                    cx="222.196"
                    cy="246.188"
                    rx="70.6654"
                    ry="70.2036"
                    fill="url(#paint2_radial_2223_1954)"
                />
            </g>
            <path
                d="M265.357 251.963C265.357 251.963 243.47 285.5 216.471 285.5C189.473 285.5 167.586 251.963 167.586 251.963C167.586 251.963 170.722 246.623 176.086 239.986C180.526 234.491 186.492 228.107 193.468 223.129C200.324 218.236 208.155 214.7 216.471 214.7C224.788 214.7 232.619 218.236 239.475 223.129C246.451 228.107 252.417 234.491 256.857 239.986C262.221 246.623 265.357 251.963 265.357 251.963Z"
                fill="white"
            />
            <path
                d="M265.357 251.963C265.357 251.963 243.47 285.5 216.471 285.5C189.473 285.5 167.586 251.963 167.586 251.963C167.586 251.963 170.722 246.623 176.086 239.986C180.526 234.491 186.492 228.107 193.468 223.129C200.324 218.236 208.155 214.7 216.471 214.7C224.788 214.7 232.619 218.236 239.475 223.129C246.451 228.107 252.417 234.491 256.857 239.986C262.221 246.623 265.357 251.963 265.357 251.963Z"
                fill="url(#paint3_linear_2223_1954)"
            />
            <path
                d="M216.471 214.7C208.155 214.7 200.324 218.236 193.468 223.129M216.471 214.7V197M216.471 214.7C224.788 214.7 232.619 218.236 239.475 223.129M176.086 239.986C170.722 246.623 167.586 251.963 167.586 251.963C167.586 251.963 189.473 285.5 216.471 285.5C243.47 285.5 265.357 251.963 265.357 251.963C265.357 251.963 262.221 246.623 256.857 239.986M176.086 239.986L160 228.186M176.086 239.986C180.526 234.491 186.492 228.107 193.468 223.129M193.468 223.129L181.914 207.114M239.475 223.129L248.5 207.114M239.475 223.129C246.451 228.107 252.417 234.491 256.857 239.986M256.857 239.986L272.943 228.186"
                stroke="url(#paint4_linear_2223_1954)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <circle cx="216.472" cy="250.1" r="20.2286" fill="white" />
            <circle
                cx="216.472"
                cy="250.1"
                r="20.2286"
                fill="url(#paint5_linear_2223_1954)"
            />
            <circle
                cx="216.472"
                cy="250.1"
                r="20.2286"
                stroke="url(#paint6_linear_2223_1954)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <defs>
                <filter
                    id="filter0_f_2223_1954"
                    x="107.53"
                    y="131.984"
                    width="229.331"
                    height="228.407"
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity="0" result="BackgroundImageFix" />
                    <feBlend
                        mode="normal"
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feGaussianBlur
                        stdDeviation="22"
                        result="effect1_foregroundBlur_2223_1954"
                    />
                </filter>
                <linearGradient
                    id="paint0_linear_2223_1954"
                    x1="390.476"
                    y1="271.02"
                    x2="125.889"
                    y2="35.0571"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_2223_1954"
                    x1="27.6546"
                    y1="228.982"
                    x2="292.327"
                    y2="465.702"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <radialGradient
                    id="paint2_radial_2223_1954"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(151.53 304.306) rotate(-42.2047) scale(188.468 127.378)"
                >
                    <stop offset="0.25304" stopColor="#00FFA3" />
                    <stop offset="0.669295" stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </radialGradient>
                <linearGradient
                    id="paint3_linear_2223_1954"
                    x1="250"
                    y1="184.5"
                    x2="199.292"
                    y2="277.235"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint4_linear_2223_1954"
                    x1="294.014"
                    y1="202.9"
                    x2="152.493"
                    y2="267.639"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="white" />
                </linearGradient>
                <linearGradient
                    id="paint5_linear_2223_1954"
                    x1="209.363"
                    y1="234.988"
                    x2="209.363"
                    y2="260.653"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint6_linear_2223_1954"
                    x1="230.398"
                    y1="245.824"
                    x2="189.929"
                    y2="263.886"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default Revelation;
