const Feasibility: React.FC = () => {
  return (
    <svg
      width="288"
      height="286"
      viewBox="0 0 288 286"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g opacity="0.6">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M11 39.8L39.8 11H162.2V68.6H68.6V162.2H11V39.8Z"
          fill="white"
        />
        <path
          d="M162.2 11H39.8L11 39.8V162.2M162.2 11V68.6M162.2 11L191 39.8V98.84M162.2 68.6H68.6M162.2 68.6L191 98.84M68.6 68.6V162.2M68.6 68.6L98.84 98.84M68.6 162.2H11M68.6 162.2L98.84 191M11 162.2L39.8 191H98.84M191 98.84H98.84M98.84 98.84V191"
          stroke="url(#paint0_linear_1698_393)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <g opacity="0.4">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M21 49.8L49.8 21H172.2V78.6H78.6V172.2H21V49.8Z"
          fill="white"
        />
        <path
          d="M172.2 21H49.8L21 49.8V172.2M172.2 21V78.6M172.2 21L201 49.8V108.84M172.2 78.6H78.6M172.2 78.6L201 108.84M78.6 78.6V172.2M78.6 78.6L108.84 108.84M78.6 172.2H21M78.6 172.2L108.84 201M21 172.2L49.8 201H108.84M201 108.84H108.84M108.84 108.84V201"
          stroke="url(#paint1_linear_1698_393)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <g opacity="0.2">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M30 58.8L58.8 30H181.2V87.6H87.6V181.2H30V58.8Z"
          fill="white"
        />
        <path
          d="M181.2 30H58.8L30 58.8V181.2M181.2 30V87.6M181.2 30L210 58.8V117.84M181.2 87.6H87.6M181.2 87.6L210 117.84M87.6 87.6V181.2M87.6 87.6L117.84 117.84M87.6 181.2H30M87.6 181.2L117.84 210M30 181.2L58.8 210H117.84M210 117.84H117.84M117.84 117.84V210"
          stroke="url(#paint2_linear_1698_393)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <path
        d="M153 58.5V2L180.5 27V87.5L88 91.5V180.5H30L1 151.5H58.5V58.5H153Z"
        fill="white"
      />
      <path
        d="M153 58.5V2L180.5 27V87.5L88 91.5V180.5H30L1 151.5H58.5V58.5H153Z"
        fill="url(#paint3_linear_1698_393)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1 29.8L29.8 1H152.2V58.6H58.6V152.2H1V29.8Z"
        fill="white"
      />
      <path
        d="M152.2 1H29.8L1 29.8V152.2M152.2 1V58.6M152.2 1L181 29.8V88.84M152.2 58.6H58.6M152.2 58.6L181 88.84M58.6 58.6V152.2M58.6 58.6L88.84 88.84M58.6 152.2H1M58.6 152.2L88.84 181M1 152.2L29.8 181H88.84M181 88.84H88.84M88.84 88.84V181"
        stroke="url(#paint4_linear_1698_393)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <g opacity="0.6">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M87 221V87H147V191H172H251V251H117L87 221Z"
          fill="white"
        />
        <path
          d="M147 87H87V221L117 251M147 87V191H172M147 87L172 113V191M251 191V251M251 191H172M251 191L276.5 218V275M251 251H117M251 251L276.5 275M117 251L142 273L276.5 275"
          stroke="url(#paint5_linear_1698_393)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <g opacity="0.4">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M97 231V97H157V201H182H261V261H127L97 231Z"
          fill="white"
        />
        <path
          d="M157 97H97V231L127 261M157 97V201H182M157 97L182 123V201M261 201V261M261 201H182M261 201L286.5 228V285M261 261H127M261 261L286.5 285M127 261L152 283L286.5 285"
          stroke="url(#paint6_linear_1698_393)"
          strokeWidth="2"
          strokeLinejoin="bevel"
        />
      </g>
      <path
        d="M136 181V78L162.5 100V181H242.5L265.5 208L266 264L131 262.5L107.5 240.5L136 181Z"
        fill="white"
      />
      <path
        d="M136 181V78L162.5 100V181H242.5L265.5 208L266 264L131 262.5L107.5 240.5L136 181Z"
        fill="url(#paint7_linear_1698_393)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M77 211V77H137V181H162H241V241H107L77 211Z"
        fill="white"
      />
      <path
        d="M137 77H77V211L107 241M137 77V181H162M137 77L162 103V181M241 181V241M241 181H162M241 181L266.5 208V265M241 241H107M241 241L266.5 265M107 241L132 263L266.5 265"
        stroke="url(#paint8_linear_1698_393)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1698_393"
          x1="245.298"
          y1="87.0011"
          x2="-34.3212"
          y2="209.073"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1698_393"
          x1="255.298"
          y1="97.0011"
          x2="-24.3212"
          y2="219.073"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1698_393"
          x1="264.298"
          y1="106.001"
          x2="-15.3212"
          y2="228.073"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1698_393"
          x1="165.951"
          y1="5"
          x2="165.951"
          y2="97.5702"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1698_393"
          x1="235.298"
          y1="77.0011"
          x2="-44.3212"
          y2="199.073"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1698_393"
          x1="300.471"
          y1="156.245"
          x2="45.7074"
          y2="267.467"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1698_393"
          x1="310.471"
          y1="166.245"
          x2="55.7074"
          y2="277.467"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1698_393"
          x1="148.951"
          y1="78"
          x2="148.951"
          y2="170.57"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1698_393"
          x1="290.471"
          y1="146.245"
          x2="35.7074"
          y2="257.467"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Feasibility;
