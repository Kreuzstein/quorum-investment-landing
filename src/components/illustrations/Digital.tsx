const Digital: React.FC = () => {
  return (
    <svg
      width="214"
      height="278"
      viewBox="0 0 214 278"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M97.3797 12.061L213 127.05L207.342 178.875L69.9 267.141L21.4062 257.424L31.105 198.31L130.561 138.386L35.1461 59.0282L49.6943 6.39258L97.3797 12.061Z"
        fill="url(#paint0_linear_1560_3279)"
      />
      <path
        d="M97.3797 12.061L213 127.05L207.342 178.875L69.9 267.141M97.3797 12.061L82.8316 67.126M97.3797 12.061L49.6943 6.39258L35.1461 59.0282M69.9 267.141L78.7905 208.837M69.9 267.141L21.4062 257.424L31.105 198.31M78.7905 208.837L176.63 148.104M78.7905 208.837L31.105 198.31M176.63 148.104L82.8316 67.126M176.63 148.104L130.561 138.386M82.8316 67.126L35.1461 59.0282M35.1461 59.0282L130.561 138.386M130.561 138.386L31.105 198.31"
        stroke="url(#paint1_linear_1560_3279)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M69.0215 250.479V12.0615L186.925 28.9452V269.409L69.0215 250.479Z"
        fill="white"
        stroke="url(#paint2_linear_1560_3279)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M61.0854 257.281V18.8633L178.989 35.7469L186.925 28.4996V269.409L178.989 276.211L61.0854 257.281Z"
        fill="white"
      />
      <path
        d="M178.989 276.211L61.0854 257.281V18.8633L178.989 35.7469M178.989 276.211V35.7469M178.989 276.211L186.925 269.409V28.4996L178.989 35.7469"
        stroke="url(#paint3_linear_1560_3279)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M143.845 58.5428V119.195L94.5294 108.425L35.0107 58.5428L50.8824 5.82617L97.3637 10.9278L143.845 58.5428Z"
        fill="white"
      />
      <path
        d="M143.845 58.5428V119.195L94.5294 108.425L35.0107 58.5428L50.8824 5.82617L97.3637 10.9278L143.845 58.5428Z"
        fill="url(#paint4_linear_1560_3279)"
      />
      <path
        d="M35.0107 196.853L94.5294 158.874L143.845 168.541V219.527L70.1551 269.409L23.6738 257.505L35.0107 196.853Z"
        fill="white"
      />
      <path
        d="M35.0107 196.853L94.5294 158.874L143.845 168.541V219.527L70.1551 269.409L23.6738 257.505L35.0107 196.853Z"
        fill="url(#paint5_linear_1560_3279)"
      />
      <path
        d="M35.0107 58.5428L82.6257 65.3449M35.0107 58.5428L50.8824 5.82617L97.3637 10.9278M35.0107 58.5428L94.5294 108.425L143.845 119.195M82.6257 65.3449L143.845 119.195M82.6257 65.3449L97.3637 10.9278M143.845 119.195V58.5428L97.3637 10.9278M35.0107 196.853L94.5294 158.874L143.845 168.541M35.0107 196.853L23.6738 257.505L70.1551 269.409M35.0107 196.853L82.6257 209.89M70.1551 269.409L82.6257 209.89M70.1551 269.409L143.845 219.527V168.541M82.6257 209.89L143.845 168.541"
        stroke="url(#paint6_linear_1560_3279)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        opacity="0.6"
        d="M23.6738 56.2752L71.2888 63.0773M23.6738 56.2752L39.5455 3.55859L86.0267 8.6602M23.6738 56.2752L83.1925 106.158L132.508 116.928M71.2888 63.0773L132.508 116.928M71.2888 63.0773L86.0267 8.6602M132.508 116.928V56.2752L86.0267 8.6602M23.6738 194.585L83.1925 156.607L132.508 166.273M23.6738 194.585L12.3369 255.238L58.8182 267.141M23.6738 194.585L71.2888 207.623M58.8182 267.141L71.2888 207.623M58.8182 267.141L132.508 217.259V166.273M71.2888 207.623L132.508 166.273"
        stroke="url(#paint7_linear_1560_3279)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        opacity="0.4"
        d="M12.3369 54.0076L59.9519 60.8097M12.3369 54.0076L28.2086 1.29102L74.6898 6.39262M12.3369 54.0076L71.8556 103.89L121.171 114.66M59.9519 60.8097L121.171 114.66M59.9519 60.8097L74.6898 6.39262M121.171 114.66V54.0076L74.6898 6.39262M12.3369 192.318L71.8556 154.339L121.171 164.005M12.3369 192.318L1 252.97L47.4813 264.874M12.3369 192.318L59.9519 205.355M47.4813 264.874L59.9519 205.355M47.4813 264.874L121.171 214.992V164.005M59.9519 205.355L121.171 164.005"
        stroke="url(#paint8_linear_1560_3279)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1560_3279"
          x1="213"
          y1="107.291"
          x2="32.7432"
          y2="205.922"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="0.572917" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1560_3279"
          x1="263.955"
          y1="118.251"
          x2="122.871"
          y2="169.077"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1560_3279"
          x1="222.491"
          y1="120.721"
          x2="12.8156"
          y2="162.659"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1560_3279"
          x1="214.555"
          y1="127.523"
          x2="4.87953"
          y2="169.46"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1560_3279"
          x1="90.8701"
          y1="17.2863"
          x2="90.8701"
          y2="257.949"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1560_3279"
          x1="90.8701"
          y1="17.2863"
          x2="90.8701"
          y2="257.949"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1560_3279"
          x1="175.805"
          y1="118.901"
          x2="-13.1692"
          y2="155.221"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1560_3279"
          x1="164.468"
          y1="116.633"
          x2="-24.5061"
          y2="152.953"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1560_3279"
          x1="153.131"
          y1="114.366"
          x2="-35.843"
          y2="150.685"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Digital;
