const Wave1: React.FC = () => {
  return (
    <svg
      viewBox="0 0 1920 1075"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2167.5 357.376C2217 -47.6212 1886 -80.5259 1582 123.874C1202 379.374 1158.5 568.873 850 727.873C554.245 880.304 776.265 333.291 679 276.373C476.5 157.873 -140 1010.88 -663 961.379"
        stroke="url(#paint0_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2177.5 367.376C2227 -37.6212 1896 -70.5259 1592 133.874C1212 389.374 1168.5 578.873 860 737.873C564.245 890.304 786.265 343.291 689 286.373C486.5 167.873 -130 1020.88 -653 971.379"
        stroke="url(#paint1_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2187.5 377.376C2237 -27.6212 1906 -60.5259 1602 143.874C1222 399.374 1178.5 588.873 870 747.873C574.245 900.304 796.265 353.291 699 296.373C496.5 177.873 -120 1030.88 -643 981.379"
        stroke="url(#paint2_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2197.5 387.376C2247 -17.6212 1916 -50.5259 1612 153.874C1232 409.374 1188.5 598.873 880 757.873C584.245 910.304 806.265 363.291 709 306.373C506.5 187.873 -110 1040.88 -633 991.379"
        stroke="url(#paint3_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2207.5 397.376C2257 -7.62119 1926 -40.5259 1622 163.874C1242 419.374 1198.5 608.873 890 767.873C594.245 920.304 816.265 373.291 719 316.373C516.5 197.873 -100 1050.88 -623 1001.38"
        stroke="url(#paint4_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2217.5 407.376C2267 2.37881 1936 -30.5259 1632 173.874C1252 429.374 1208.5 618.873 900 777.873C604.245 930.304 826.265 383.291 729 326.373C526.5 207.873 -90 1060.88 -613 1011.38"
        stroke="url(#paint5_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2227.5 417.376C2277 12.3788 1946 -20.5259 1642 183.874C1262 439.374 1218.5 628.873 910 787.873C614.245 940.304 836.265 393.291 739 336.373C536.5 217.873 -80 1070.88 -603 1021.38"
        stroke="url(#paint6_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2237.5 427.376C2287 22.3788 1956 -10.5259 1652 193.874C1272 449.374 1228.5 638.873 920 797.873C624.245 950.304 846.265 403.291 749 346.373C546.5 227.873 -70 1080.88 -593 1031.38"
        stroke="url(#paint7_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2247.5 437.376C2297 32.3788 1966 -0.525902 1662 203.874C1282 459.374 1238.5 648.873 930 807.873C634.245 960.304 856.265 413.291 759 356.373C556.5 237.873 -60 1090.88 -583 1041.38"
        stroke="url(#paint8_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2257.5 447.376C2307 42.3788 1976 9.4741 1672 213.874C1292 469.374 1248.5 658.873 940 817.873C644.245 970.304 866.265 423.291 769 366.373C566.5 247.873 -50 1100.88 -573 1051.38"
        stroke="url(#paint9_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2267.5 457.376C2317 52.3788 1986 19.4741 1682 223.874C1302 479.374 1258.5 668.873 950 827.873C654.245 980.304 876.265 433.291 779 376.373C576.5 257.873 -40 1110.88 -563 1061.38"
        stroke="url(#paint10_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M2277.5 467.376C2327 62.3788 1996 29.4741 1692 233.874C1312 489.374 1268.5 678.873 960 837.873C664.245 990.304 886.265 443.291 789 386.373C586.5 267.873 -30 1120.88 -553 1071.38"
        stroke="url(#paint11_linear_1560_3224)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1560_3224"
          x1="-760.987"
          y1="668.444"
          x2="1865.61"
          y2="-292.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1560_3224"
          x1="-750.987"
          y1="678.444"
          x2="1875.61"
          y2="-282.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1560_3224"
          x1="-740.987"
          y1="688.444"
          x2="1885.61"
          y2="-272.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1560_3224"
          x1="-730.987"
          y1="698.444"
          x2="1895.61"
          y2="-262.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1560_3224"
          x1="-720.987"
          y1="708.444"
          x2="1905.61"
          y2="-252.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1560_3224"
          x1="-710.987"
          y1="718.444"
          x2="1915.61"
          y2="-242.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1560_3224"
          x1="-700.987"
          y1="728.444"
          x2="1925.61"
          y2="-232.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1560_3224"
          x1="-690.987"
          y1="738.444"
          x2="1935.61"
          y2="-222.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1560_3224"
          x1="-680.987"
          y1="748.444"
          x2="1945.61"
          y2="-212.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint9_linear_1560_3224"
          x1="-670.987"
          y1="758.444"
          x2="1955.61"
          y2="-202.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint10_linear_1560_3224"
          x1="-660.987"
          y1="768.444"
          x2="1965.61"
          y2="-192.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint11_linear_1560_3224"
          x1="-650.987"
          y1="778.444"
          x2="1975.61"
          y2="-182.053"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0" />
          <stop offset="0.3125" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="0.463542" stopColor="#00FFA3" stopOpacity="0.2" />
          <stop offset="0.5625" stopColor="#00FFA3" stopOpacity="0.5" />
          <stop offset="0.697917" stopColor="#C362FF" stopOpacity="0.7" />
          <stop offset="1" stopColor="#C362FF" stopOpacity="0" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Wave1;
