const Wave2: React.FC = () => {
  return (
    <svg
      viewBox="0 0 1920 1459"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M-620 376.232C-217.5 413.234 106.5 -55.7505 313.5 6.74219C549.485 77.9853 168 454.726 234.5 666.726C318.008 932.948 1267.5 205.727 1421 454.727C1574.5 703.727 902.5 895.226 1031 1114.73C1159.5 1334.23 1596 770.726 2039.5 820.226C2483 869.726 2458.5 1189.23 2761 1305.23C3024.55 1406.29 3282.5 1244.56 3466.5 1164.23"
        stroke="url(#paint0_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-610 386.232C-207.5 423.234 116.5 -45.7505 323.5 16.7422C559.485 87.9853 178 464.726 244.5 676.726C328.008 942.948 1277.5 215.727 1431 464.727C1584.5 713.727 912.5 905.226 1041 1124.73C1169.5 1344.23 1606 780.726 2049.5 830.226C2493 879.726 2468.5 1199.23 2771 1315.23C3034.55 1416.29 3292.5 1254.56 3476.5 1174.23"
        stroke="url(#paint1_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-600 396.232C-197.5 433.234 126.5 -35.7505 333.5 26.7422C569.485 97.9853 188 474.726 254.5 686.726C338.008 952.948 1287.5 225.727 1441 474.727C1594.5 723.727 922.5 915.226 1051 1134.73C1179.5 1354.23 1616 790.726 2059.5 840.226C2503 889.726 2478.5 1209.23 2781 1325.23C3044.55 1426.29 3302.5 1264.56 3486.5 1184.23"
        stroke="url(#paint2_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-590 406.232C-187.5 443.234 136.5 -25.7505 343.5 36.7422C579.485 107.985 198 484.726 264.5 696.726C348.008 962.948 1297.5 235.727 1451 484.727C1604.5 733.727 932.5 925.226 1061 1144.73C1189.5 1364.23 1626 800.726 2069.5 850.226C2513 899.726 2488.5 1219.23 2791 1335.23C3054.55 1436.29 3312.5 1274.56 3496.5 1194.23"
        stroke="url(#paint3_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-580 416.232C-177.5 453.234 146.5 -15.7505 353.5 46.7422C589.485 117.985 208 494.726 274.5 706.726C358.008 972.948 1307.5 245.727 1461 494.727C1614.5 743.727 942.5 935.226 1071 1154.73C1199.5 1374.23 1636 810.726 2079.5 860.226C2523 909.726 2498.5 1229.23 2801 1345.23C3064.55 1446.29 3322.5 1284.56 3506.5 1204.23"
        stroke="url(#paint4_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-570 426.232C-167.5 463.234 156.5 -5.75045 363.5 56.7422C599.485 127.985 218 504.726 284.5 716.726C368.008 982.948 1317.5 255.727 1471 504.727C1624.5 753.727 952.5 945.226 1081 1164.73C1209.5 1384.23 1646 820.726 2089.5 870.226C2533 919.726 2508.5 1239.23 2811 1355.23C3074.55 1456.29 3332.5 1294.56 3516.5 1214.23"
        stroke="url(#paint5_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-560 436.232C-157.5 473.234 166.5 4.24955 373.5 66.7422C609.485 137.985 228 514.726 294.5 726.726C378.008 992.948 1327.5 265.727 1481 514.727C1634.5 763.727 962.5 955.226 1091 1174.73C1219.5 1394.23 1656 830.726 2099.5 880.226C2543 929.726 2518.5 1249.23 2821 1365.23C3084.55 1466.29 3342.5 1304.56 3526.5 1224.23"
        stroke="url(#paint6_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-550 446.232C-147.5 483.234 176.5 14.2495 383.5 76.7422C619.485 147.985 238 524.726 304.5 736.726C388.008 1002.95 1337.5 275.727 1491 524.727C1644.5 773.727 972.5 965.226 1101 1184.73C1229.5 1404.23 1666 840.726 2109.5 890.226C2553 939.726 2528.5 1259.23 2831 1375.23C3094.55 1476.29 3352.5 1314.56 3536.5 1234.23"
        stroke="url(#paint7_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-540 456.232C-137.5 493.234 186.5 24.2495 393.5 86.7422C629.485 157.985 248 534.726 314.5 746.726C398.008 1012.95 1347.5 285.727 1501 534.727C1654.5 783.727 982.5 975.226 1111 1194.73C1239.5 1414.23 1676 850.726 2119.5 900.226C2563 949.726 2538.5 1269.23 2841 1385.23C3104.55 1486.29 3362.5 1324.56 3546.5 1244.23"
        stroke="url(#paint8_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-530 466.232C-127.5 503.234 196.5 34.2495 403.5 96.7422C639.485 167.985 258 544.726 324.5 756.726C408.008 1022.95 1357.5 295.727 1511 544.727C1664.5 793.727 992.5 985.226 1121 1204.73C1249.5 1424.23 1686 860.726 2129.5 910.226C2573 959.726 2548.5 1279.23 2851 1395.23C3114.55 1496.29 3372.5 1334.56 3556.5 1254.23"
        stroke="url(#paint9_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-520 476.232C-117.5 513.234 206.5 44.2495 413.5 106.742C649.485 177.985 268 554.726 334.5 766.726C418.008 1032.95 1367.5 305.727 1521 554.727C1674.5 803.727 1002.5 995.226 1131 1214.73C1259.5 1434.23 1696 870.726 2139.5 920.226C2583 969.726 2558.5 1289.23 2861 1405.23C3124.55 1506.29 3382.5 1344.56 3566.5 1264.23"
        stroke="url(#paint10_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-510 486.232C-107.5 523.234 216.5 54.2495 423.5 116.742C659.485 187.985 278 564.726 344.5 776.726C428.008 1042.95 1377.5 315.727 1531 564.727C1684.5 813.727 1012.5 1005.23 1141 1224.73C1269.5 1444.23 1706 880.726 2149.5 930.226C2593 979.726 2568.5 1299.23 2871 1415.23C3134.55 1516.29 3392.5 1354.56 3576.5 1274.23"
        stroke="url(#paint11_linear_1560_3335)"
        strokeWidth="2"
      />
      <path
        d="M-500 496.232C-97.5 533.234 226.5 64.2495 433.5 126.742C669.485 197.985 288 574.726 354.5 786.726C438.008 1052.95 1387.5 325.727 1541 574.727C1694.5 823.727 1022.5 1015.23 1151 1234.73C1279.5 1454.23 1716 890.726 2159.5 940.226C2603 989.726 2578.5 1309.23 2881 1425.23C3144.55 1526.29 3402.5 1364.56 3586.5 1284.23"
        stroke="url(#paint12_linear_1560_3335)"
        strokeWidth="2"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1560_3335"
          x1="-499"
          y1="272.242"
          x2="2563"
          y2="1280.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1560_3335"
          x1="-489"
          y1="282.242"
          x2="2573"
          y2="1290.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1560_3335"
          x1="-479"
          y1="292.242"
          x2="2583"
          y2="1300.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1560_3335"
          x1="-469"
          y1="302.242"
          x2="2593"
          y2="1310.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1560_3335"
          x1="-459"
          y1="312.242"
          x2="2603"
          y2="1320.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1560_3335"
          x1="-449"
          y1="322.242"
          x2="2613"
          y2="1330.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1560_3335"
          x1="-439"
          y1="332.242"
          x2="2623"
          y2="1340.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1560_3335"
          x1="-429"
          y1="342.242"
          x2="2633"
          y2="1350.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1560_3335"
          x1="-419"
          y1="352.242"
          x2="2643"
          y2="1360.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint9_linear_1560_3335"
          x1="-409"
          y1="362.242"
          x2="2653"
          y2="1370.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint10_linear_1560_3335"
          x1="-399"
          y1="372.242"
          x2="2663"
          y2="1380.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint11_linear_1560_3335"
          x1="-389"
          y1="382.242"
          x2="2673"
          y2="1390.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint12_linear_1560_3335"
          x1="-379"
          y1="392.242"
          x2="2683"
          y2="1400.74"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00FFA3" stopOpacity="0" />
          <stop offset="0.223958" stopColor="#C362FF" stopOpacity="0.3" />
          <stop offset="0.473958" stopColor="#00FFA3" stopOpacity="0.8" />
          <stop offset="0.645833" stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Wave2;
