const TopDown: React.FC = () => {
    return (
        <svg
            width="304"
            height="255"
            viewBox="0 0 304 255"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M146.5 44.5V125.5V185"
                stroke="url(#paint0_linear_1537_2510)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <path
                d="M169 77.5V124H252V154"
                stroke="url(#paint1_linear_1537_2510)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <path
                d="M125.5 77.5V135H50.5V159.5"
                stroke="url(#paint2_linear_1537_2510)"
                strokeWidth="11"
                strokeLinejoin="bevel"
            />
            <path d="M97 1H183V87H97V1Z" fill="white" />
            <path d="M97 1H183V87H97V1Z" fill="url(#paint3_linear_1537_2510)" />
            <path
                d="M183 1H97V87M183 1V87M183 1L206.5 18.5V105M183 87H97M183 87L206.5 105M97 87L119.5 105H206.5"
                stroke="url(#paint4_linear_1537_2510)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path d="M1 147H66.9726V213.154H1V147Z" fill="white" />
            <path
                d="M66.9726 147H1V213.154M66.9726 147V213.154M66.9726 147L85 160.462V227M66.9726 213.154H1M66.9726 213.154L85 227M1 213.154L18.2603 227H85"
                stroke="url(#paint5_linear_1537_2510)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path d="M110 174H175.973V240.154H110V174Z" fill="white" />
            <path
                d="M175.973 174H110V240.154M175.973 174V240.154M175.973 174L194 187.462V254M175.973 240.154H110M175.973 240.154L194 254M110 240.154L127.26 254H194"
                stroke="url(#paint6_linear_1537_2510)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <path d="M219 144H284.973V210.154H219V144Z" fill="white" />
            <path
                d="M284.973 144H219V210.154M284.973 144V210.154M284.973 144L303 157.462V224M284.973 210.154H219M284.973 210.154L303 224M219 210.154L236.26 224H303"
                stroke="url(#paint7_linear_1537_2510)"
                strokeWidth="2"
                strokeLinejoin="bevel"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_1537_2510"
                    x1="199.5"
                    y1="26"
                    x2="91.0402"
                    y2="172.843"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" stopOpacity="0" />
                    <stop
                        offset="0.375799"
                        stopColor="#00FFA3"
                        stopOpacity="0.45"
                    />
                    <stop
                        offset="0.649321"
                        stopColor="#C362FF"
                        stopOpacity="0.42"
                    />
                    <stop
                        offset="0.909188"
                        stopColor="#C362FF"
                        stopOpacity="0"
                    />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_1537_2510"
                    x1="7971"
                    y1="61.9822"
                    x2="7966.13"
                    y2="401.295"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" stopOpacity="0" />
                    <stop
                        offset="0.411458"
                        stopColor="#00FFA3"
                        stopOpacity="0.56"
                    />
                    <stop
                        offset="0.588542"
                        stopColor="#C362FF"
                        stopOpacity="0.42"
                    />
                    <stop
                        offset="0.909188"
                        stopColor="#C362FF"
                        stopOpacity="0"
                    />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_1537_2510"
                    x1="6818.5"
                    y1="60.8665"
                    x2="6812.05"
                    y2="424.535"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" stopOpacity="0" />
                    <stop
                        offset="0.375799"
                        stopColor="#00FFA3"
                        stopOpacity="0.56"
                    />
                    <stop
                        offset="0.649321"
                        stopColor="#C362FF"
                        stopOpacity="0.42"
                    />
                    <stop
                        offset="0.909188"
                        stopColor="#C362FF"
                        stopOpacity="0"
                    />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_1537_2510"
                    x1="151.75"
                    y1="1"
                    x2="151.75"
                    y2="105"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" stopOpacity="0.17" />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
                </linearGradient>
                <linearGradient
                    id="paint4_linear_1537_2510"
                    x1="239.531"
                    y1="44.9117"
                    x2="72.3354"
                    y2="121.764"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint5_linear_1537_2510"
                    x1="86.8736"
                    y1="174.932"
                    x2="-15.7007"
                    y2="219.59"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint6_linear_1537_2510"
                    x1="195.874"
                    y1="201.932"
                    x2="93.2993"
                    y2="246.59"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
                <linearGradient
                    id="paint7_linear_1537_2510"
                    x1="304.874"
                    y1="171.932"
                    x2="202.299"
                    y2="216.59"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#C362FF" />
                    <stop offset="1" stopColor="#00FFA3" />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default TopDown;
