const Pitch: React.FC = () => {
  return (
    <svg
      width="298"
      height="321"
      viewBox="0 0 298 321"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="0.698242"
        y="187.139"
        width="187.926"
        height="187.926"
        transform="rotate(-45 0.698242 187.139)"
        stroke="url(#paint0_linear_1698_395)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <rect
        opacity="0.6"
        x="10.6982"
        y="187.139"
        width="187.926"
        height="187.926"
        transform="rotate(-45 10.6982 187.139)"
        stroke="url(#paint1_linear_1698_395)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <rect
        opacity="0.4"
        x="20.6982"
        y="187.139"
        width="187.926"
        height="187.926"
        transform="rotate(-45 20.6982 187.139)"
        stroke="url(#paint2_linear_1698_395)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <rect
        opacity="0.2"
        x="30.6982"
        y="187.139"
        width="187.926"
        height="187.926"
        transform="rotate(-45 30.6982 187.139)"
        stroke="url(#paint3_linear_1698_395)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <path
        d="M238.5 0.5L194.5 33L141 141L79.5 92L45 112L6.5 156L122.5 259.5L167 244.5L194.5 222L163.5 160.5L238.5 0.5Z"
        fill="white"
      />
      <path
        d="M238.5 0.5L194.5 33L141 141L79.5 92L45 112L6.5 156L122.5 259.5L167 244.5L194.5 222L163.5 160.5L238.5 0.5Z"
        fill="url(#paint4_linear_1698_395)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M289.789 32.9444L195.094 221.398L154.288 235.3L41.2884 135.8L80.1107 90.0524L141 142.5L162.728 160.163L239 1L289.789 32.9444Z"
        fill="white"
      />
      <path
        d="M195.094 221.398L289.789 32.9444L239 1M195.094 221.398L154.288 235.3M195.094 221.398L166.5 244L122.5 260.004M154.288 235.3L41.2884 135.8M154.288 235.3L122.5 260.004M41.2884 135.8L80.1107 90.0524M41.2884 135.8L5 155.5M80.1107 90.0524L141 142.5M80.1107 90.0524L45 111.5L5 155.5M239 1L162.728 160.163L141 142.5M239 1L195.195 32.9444L141 142.5M5 155.5L122.5 260.004"
        stroke="url(#paint5_linear_1698_395)"
        strokeWidth="2"
        strokeLinejoin="bevel"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1698_395"
          x1="225.297"
          y1="258.452"
          x2="-33.5612"
          y2="369.259"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1698_395"
          x1="235.297"
          y1="258.452"
          x2="-23.5612"
          y2="369.259"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1698_395"
          x1="245.297"
          y1="258.452"
          x2="-13.5612"
          y2="369.259"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1698_395"
          x1="255.297"
          y1="258.452"
          x2="-3.56121"
          y2="369.259"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1698_395"
          x1="165.76"
          y1="0.5"
          x2="165.76"
          y2="80.1302"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" stopOpacity="0.17" />
          <stop offset="1" stopColor="#00FFA3" stopOpacity="0.17" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1698_395"
          x1="400.533"
          y1="9.12605"
          x2="-56.2084"
          y2="171.976"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C362FF" />
          <stop offset="1" stopColor="#00FFA3" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Pitch;
