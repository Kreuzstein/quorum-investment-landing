const Support: React.FC = () => {
    return (
        <svg
            width="386"
            height="377"
            viewBox="0 0 386 377"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <g opacity="0.2">
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M228.735 78.8325L252.254 147.961L278.633 138.986L249.131 52.2718L227.941 45.3536L152.955 97.9554L168.957 120.767L228.735 78.8325Z"
                    fill="url(#paint0_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M106.161 118.584L179.173 117.578L178.79 89.7168L87.2026 90.9785L74.0748 108.994L100.93 196.564L127.57 188.395L106.161 118.584Z"
                    fill="url(#paint1_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M304.228 183.252L245.874 139.358L262.624 117.09L335.823 172.151L335.792 194.442L262.438 249.296L245.751 226.981L304.228 183.252Z"
                    fill="url(#paint2_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M106.002 247.529L179.012 248.74L178.55 276.601L86.9665 275.081L73.8894 257.029L100.991 169.534L127.608 177.779L106.002 247.529Z"
                    fill="url(#paint3_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M228.002 287.354L168.343 245.252L152.277 268.018L227.114 320.831L248.324 313.973L278.07 227.341L251.716 218.292L228.002 287.354Z"
                    fill="url(#paint4_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
            </g>
            <g opacity="0.4">
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M225.216 88.4441L246.584 151.253L270.552 143.099L243.747 64.3115L224.494 58.0257L156.363 105.819L170.902 126.545L225.216 88.4441Z"
                    fill="url(#paint5_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M113.845 124.56L180.183 123.647L179.835 98.3323L96.6204 99.4786L84.6927 115.847L109.093 195.412L133.297 187.989L113.845 124.56Z"
                    fill="url(#paint6_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M293.805 183.318L240.786 143.437L256.004 123.205L322.512 173.232L322.483 193.485L255.835 243.325L240.674 223.05L293.805 183.318Z"
                    fill="url(#paint7_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M113.702 241.716L180.037 242.817L179.617 268.13L96.4061 266.749L84.5245 250.347L109.149 170.851L133.332 178.342L113.702 241.716Z"
                    fill="url(#paint8_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M224.549 277.906L170.343 239.653L155.746 260.337L223.742 308.322L243.012 302.091L270.039 223.379L246.095 215.157L224.549 277.906Z"
                    fill="url(#paint9_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
            </g>
            <g opacity="0.7">
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M222.125 97.9145L241.468 154.769L263.163 147.388L238.9 76.0697L221.471 70.3798L159.8 113.642L172.961 132.403L222.125 97.9145Z"
                    fill="url(#paint10_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M121.311 130.607L181.36 129.78L181.044 106.865L105.719 107.903L94.9218 122.72L117.009 194.742L138.919 188.022L121.311 130.607Z"
                    fill="url(#paint11_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M284.211 183.794L236.218 147.694L249.994 129.38L310.196 174.664L310.17 192.998L249.841 238.112L236.116 219.76L284.211 183.794Z"
                    fill="url(#paint12_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M121.183 236.657L181.229 237.653L180.849 260.567L105.527 259.317L94.7716 244.47L117.062 172.511L138.952 179.291L121.183 236.657Z"
                    fill="url(#paint13_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M221.523 269.414L172.456 234.787L159.243 253.511L220.792 296.947L238.236 291.306L262.701 220.056L241.026 212.614L221.523 269.414Z"
                    fill="url(#paint14_linear_659_1386)"
                    stroke="#00FFA3"
                    stroke-linecap="square"
                    strokeLinejoin="bevel"
                />
            </g>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M218.764 107.301L236.125 158.327L255.596 151.703L233.82 87.6952L218.178 82.5886L162.828 121.416L174.64 138.254L218.764 107.301Z"
                fill="url(#paint15_linear_659_1386)"
                stroke="#00FFA3"
                stroke-linecap="square"
                strokeLinejoin="bevel"
            />
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M128.617 136.521L182.511 135.779L182.227 115.213L114.623 116.145L104.933 129.443L124.756 194.082L144.42 188.051L128.617 136.521Z"
                fill="url(#paint16_linear_659_1386)"
                stroke="#00FFA3"
                stroke-linecap="square"
                strokeLinejoin="bevel"
            />
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M274.819 184.259L231.745 151.859L244.109 135.422L298.14 176.064L298.117 192.518L243.972 233.008L231.654 216.537L274.819 184.259Z"
                fill="url(#paint17_linear_659_1386)"
                stroke="#00FFA3"
                stroke-linecap="square"
                strokeLinejoin="bevel"
            />
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M128.503 231.701L182.394 232.595L182.053 253.16L114.452 252.038L104.799 238.713L124.804 174.13L144.451 180.216L128.503 231.701Z"
                fill="url(#paint18_linear_659_1386)"
                stroke="#00FFA3"
                stroke-linecap="square"
                strokeLinejoin="bevel"
            />
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M218.559 261.101L174.522 230.024L162.663 246.828L217.903 285.812L233.559 280.749L255.516 216.803L236.063 210.124L218.559 261.101Z"
                fill="url(#paint19_linear_659_1386)"
                stroke="#00FFA3"
                stroke-linecap="square"
                strokeLinejoin="bevel"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_659_1386"
                    x1="203.69"
                    y1="143.66"
                    x2="232.549"
                    y2="40.1652"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_659_1386"
                    x1="160.076"
                    y1="162.437"
                    x2="70.5643"
                    y2="103.008"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_659_1386"
                    x1="234.823"
                    y1="186.844"
                    x2="342.158"
                    y2="191.681"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_659_1386"
                    x1="160.041"
                    y1="203.828"
                    x2="70.3621"
                    y2="263.004"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint4_linear_659_1386"
                    x1="210.16"
                    y1="220.186"
                    x2="247.645"
                    y2="320.878"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint5_linear_659_1386"
                    x1="202.46"
                    y1="147.345"
                    x2="228.681"
                    y2="53.3117"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint6_linear_659_1386"
                    x1="162.832"
                    y1="164.404"
                    x2="81.5031"
                    y2="110.409"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint7_linear_659_1386"
                    x1="230.745"
                    y1="186.582"
                    x2="328.267"
                    y2="190.977"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint8_linear_659_1386"
                    x1="162.8"
                    y1="202.01"
                    x2="81.3196"
                    y2="255.777"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint9_linear_659_1386"
                    x1="208.338"
                    y1="216.878"
                    x2="242.396"
                    y2="308.365"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint10_linear_659_1386"
                    x1="201.526"
                    y1="151.232"
                    x2="225.261"
                    y2="66.1127"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint11_linear_659_1386"
                    x1="165.653"
                    y1="166.673"
                    x2="92.0346"
                    y2="117.797"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint12_linear_659_1386"
                    x1="227.129"
                    y1="186.748"
                    x2="315.406"
                    y2="190.727"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint13_linear_659_1386"
                    x1="165.626"
                    y1="200.716"
                    x2="91.8706"
                    y2="249.385"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint14_linear_659_1386"
                    x1="206.849"
                    y1="214.172"
                    x2="237.678"
                    y2="296.986"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint15_linear_659_1386"
                    x1="200.277"
                    y1="155.153"
                    x2="221.579"
                    y2="78.7588"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint16_linear_659_1386"
                    x1="168.414"
                    y1="168.891"
                    x2="102.342"
                    y2="125.024"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint17_linear_659_1386"
                    x1="223.588"
                    y1="186.91"
                    x2="302.816"
                    y2="190.48"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint18_linear_659_1386"
                    x1="168.391"
                    y1="199.444"
                    x2="102.196"
                    y2="243.124"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
                <linearGradient
                    id="paint19_linear_659_1386"
                    x1="205.389"
                    y1="211.522"
                    x2="233.058"
                    y2="285.847"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop
                        offset="0.678001"
                        stopColor="#00FFA3"
                        stopOpacity="0.2"
                    />
                    <stop offset="1" stopColor="#00FFA3" stopOpacity="0" />
                </linearGradient>
            </defs>
        </svg>
    );
};

export default Support;
