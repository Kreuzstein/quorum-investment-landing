const Icon: React.FC = () => {
    return (
        <svg
            width="64"
            height="64"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <g clipPath="url(#clip0_3307_41401)">
                <path
                    d="M11 20C17.0751 20 22 15.0751 22 9.00001C22 5.96244 20.7688 3.21244 18.7782 1.22183M11 20C7.96244 20 5.21244 18.7688 3.22183 16.7782M11 20V23M11 23H8M11 23H14"
                    stroke="currentColor"
                    strokeWidth="2"
                />
                <circle
                    cx="11"
                    cy="8.99998"
                    r="8"
                    transform="rotate(15 11 8.99998)"
                    stroke="currentColor"
                    strokeWidth="2"
                />
                <ellipse
                    cx="11"
                    cy="8.99999"
                    rx="4"
                    ry="8"
                    transform="rotate(15 11 8.99999)"
                    stroke="currentColor"
                    strokeWidth="2"
                />
                <ellipse
                    cx="11"
                    cy="8.99999"
                    rx="8"
                    ry="4"
                    transform="rotate(15 11 8.99999)"
                    stroke="currentColor"
                    strokeWidth="2"
                />
                <rect
                    x="12.1046"
                    y="1.01376"
                    width="2"
                    height="16"
                    transform="rotate(15 12.1046 1.01376)"
                    fill="currentColor"
                />
                <rect
                    x="3.5314"
                    y="5.9635"
                    width="16"
                    height="2"
                    transform="rotate(15 3.5314 5.9635)"
                    fill="currentColor"
                />
            </g>
            <defs>
                <clipPath id="clip0_3307_41401">
                    <rect width="24" height="24" fill="white" />
                </clipPath>
            </defs>
        </svg>
    );
};

export default Icon;
