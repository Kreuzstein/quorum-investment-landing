// const Icon: React.FC = () => {
//   return (
//     <svg
//       viewBox="0 0 327 318"
//       fill="none"
//       xmlns="http://www.w3.org/2000/svg"
//     >
//       <path
//         d="M249.382 56.1777L180.903 104.215L153.961 25.0252"
//         stroke="currentColor"
//         strokeWidth="14.159"
//         strokeLinejoin="bevel"
//       />
//       <path
//         d="M95.5678 44.0171L120.093 123.988L36.4536 125.141"
//         stroke="currentColor"
//         strokeWidth="14.159"
//         strokeLinejoin="bevel"
//       />
//       <path
//         d="M285.317 105.967L218.329 156.061L285.176 206.343"
//         stroke="url(#paint0_linear_14_52)"
//         strokeWidth="14.159"
//         strokeLinejoin="bevel"
//       />
//       <path
//         d="M95.2838 267.906L120.034 188.004L36.3982 186.616"
//         stroke="currentColor"
//         strokeWidth="14.159"
//         strokeLinejoin="bevel"
//       />
//       <path
//         d="M153.191 286.811L180.356 207.697L248.698 255.927"
//         stroke="currentColor"
//         strokeWidth="14.159"
//         strokeLinejoin="bevel"
//       />
//       <defs>
//         <linearGradient
//           id="paint0_linear_14_52"
//           x1="218.329"
//           y1="156.061"
//           x2="305.68"
//           y2="236.346"
//           gradientUnits="userSpaceOnUse"
//         >
//           {/* <stop stopColor="#00FFA3" /> */}
//           <stop stopColor="#CBFF67" />
//           <stop offset="1" stopColor="#C362FF" />
//         </linearGradient>
//       </defs>
//     </svg>
//   );
// };

// export default Icon;

const Icon: React.FC = () => {
    return (
        <svg
            width="56"
            height="56"
            viewBox="0 0 310 252"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M154.977 246L94.3488 89.8372L72.7616 89.8372L8 89.8372L154.977 246Z"
                fill="currentColor"
            />
            <path
                d="M215.605 89.8372L154.977 246L301.953 89.8372L237.192 89.8372L215.605 89.8372Z"
                fill="currentColor"
            />
            <path
                d="M94.3488 89.8372H215.605L237.192 89.8372L194.017 9H154.977L115.936 9.00002L72.7616 89.8372L94.3488 89.8372Z"
                fill="currentColor"
            />
            <path
                d="M154.977 246L215.605 89.8372M154.977 246L94.3488 89.8372M154.977 246L301.953 89.8372M154.977 246L8 89.8372M215.605 89.8372H94.3488M215.605 89.8372L237.192 89.8372M94.3488 89.8372L72.7616 89.8372M301.953 89.8372L246.837 9H194.017M301.953 89.8372L237.192 89.8372M8 89.8372L63.1163 9L115.936 9.00002M8 89.8372L72.7616 89.8372M72.7616 89.8372L115.936 9.00002M115.936 9.00002L154.977 9H194.017M237.192 89.8372L194.017 9"
                stroke="currentColor"
                strokeWidth="17.1669"
                strokeLinejoin="bevel"
            />
            {/* <defs>
                <linearGradient
                    id="paint0_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#CBFF67" />
                    <stop offset="0.0001" stopColor="#CBFF67" />
                    <stop offset="1" stopColor="#B062FF" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#CBFF67" />
                    <stop offset="0.0001" stopColor="#CBFF67" />
                    <stop offset="1" stopColor="#B062FF" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#CBFF67" />
                    <stop offset="0.0001" stopColor="#CBFF67" />
                    <stop offset="1" stopColor="#B062FF" />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#CBFF67" />
                    <stop offset="0.0001" stopColor="#CBFF67" />
                    <stop offset="1" stopColor="#B062FF" />
                </linearGradient>
            </defs> */}

            {/* <defs>
                <linearGradient
                    id="paint0_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint1_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint2_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
                <linearGradient
                    id="paint3_linear_33_7"
                    x1="154.977"
                    y1="9.00001"
                    x2="-128.507"
                    y2="264.758"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#00FFA3" />
                    <stop offset="1" stopColor="#C362FF" />
                </linearGradient>
            </defs> */}
        </svg>
    );
};

export default Icon;
