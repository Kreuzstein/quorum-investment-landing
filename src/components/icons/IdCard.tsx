const Icon: React.FC = () => {
    return (
        <svg
            width="64"
            height="64"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <rect
                x="1"
                y="3"
                width="22"
                height="18"
                stroke="currentColor"
                strokeWidth="2"
            />
            <circle cx="8" cy="10" r="3" stroke="currentColor" strokeWidth="2" />
            <path
                d="M4 17C4 14.7909 5.79086 13 8 13V13C10.2091 13 12 14.7909 12 17V17H4V17Z"
                stroke="currentColor"
                strokeWidth="2"
            />
            <rect x="14" y="8" width="7" height="2" fill="currentColor" />
            <rect x="14" y="11" width="7" height="2" fill="currentColor" />
            <rect x="14" y="14" width="4" height="2" fill="currentColor" />
        </svg>
    );
};

export default Icon;
