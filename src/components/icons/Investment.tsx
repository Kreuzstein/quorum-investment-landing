const Icon: React.FC = () => {
    return (
        <svg
            width="64"
            height="64"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <rect
                x="2"
                y="17"
                width="4"
                height="6"
                stroke="currentColor"
                strokeWidth="2"
            />
            <rect
                x="10"
                y="13"
                width="4"
                height="10"
                stroke="currentColor"
                strokeWidth="2"
            />
            <rect
                x="18"
                y="9"
                width="4"
                height="14"
                stroke="currentColor"
                strokeWidth="2"
            />
            <path
                d="M3 13L18 1M18 1H13M18 1L17 6"
                stroke="currentColor"
                strokeWidth="2"
            />
        </svg>
    );
};

export default Icon;
