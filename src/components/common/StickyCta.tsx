/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import { type ReactNode } from "react";
import Image from "next/image";
import Link from "next/link";

import { userPlaceholder } from "~/utils/imageData";
import Button from "../atoms/Button";
import { type ContactName, contacts } from "../home/Contact";

type Props = {
    children?: ReactNode;
    title: string;
    contact: ContactName;
    ctaLabel?: string;
};

const StickyCta: React.FC<Props> = ({
    children,
    title,
    contact: contactName,
    ctaLabel,
}) => {
    const contactObj =
        contacts.filter(
            (c) => c.name.split(" ")[0]?.toLowerCase() === contactName,
        )[0] ?? contacts[0];

    if (!contactObj) return <></>;

    return (
        <div className="sticky top-[6rem] h-fit w-full space-y-2 border-4 border-double border-violet-1000 px-5 py-3 shadow-2xl">
            <div className="w-fit bg-violet-500 px-1 font-header text-xl font-[600] text-white">
                {title}
            </div>
            {children}
            <div className="flex w-full flex-row gap-3 border-t-2 pt-2 font-body">
                <div className="relative my-auto flex h-16 w-16 rounded-full border-4 border-violet-500 min-w-1/4">
                    <Image
                        src={contactObj.image}
                        alt={contactObj.name}
                        fill
                        style={{
                            objectFit: "cover",
                            objectPosition: "center",
                            borderRadius: "9999px",
                        }}
                        placeholder={userPlaceholder}
                    />
                </div>
                <div className="flex flex-col text-sm w-3/4">
                    <div className="font-lg w-fit bg-violet-500 px-1 font-semibold text-white">
                        {contactObj.name}
                    </div>
                    <div className="font-semibold">{contactObj.role}</div>
                    <div className="text-xs">{contactObj.locale}</div>
                    <a
                        href={`mailto:${contactObj.email}`}
                        className="font-semibold text-violet-500 underline"
                    >
                        {contactObj.email}
                    </a>
                </div>
            </div>
            <a
                href={contactObj.meeting}
                target="_blank"
                className="flex w-full border-b-2 pb-2 font-body text-white children:mx-auto children:w-full children:bg-violet-500 children:py-2 children:hover:scale-105"
            >
                <Button
                    label={ctaLabel ?? "BOOK A CALL"}
                    clickHandler={() => {}}
                />
            </a>
            <div className="mx-auto w-fit font-body text-xs font-thin children:mx-1 children:underline">
                <Link href="/about/terms-and-conditions" target="_blank">
                    Terms & Conditions
                </Link>
                and
                <Link href="/about/privacy-policy" target="_blank">
                    Privacy Policy
                </Link>
                apply.
            </div>
        </div>
    );
};

export default StickyCta;
