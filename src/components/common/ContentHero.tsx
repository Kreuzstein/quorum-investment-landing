/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";

import Wave from "../illustrations/Wave0";
import {
    type LandingPageHero,
} from "~/types/content";
import { type PropertyInfo as Meta } from "~/types/properties";
import Pitch from "../illustrations/Pitch";
import Button from "../atoms/Button";
import Link from "next/link";

// TODO mebe move the big static images from public to another server folder -- since they are served by vercel anyway to avoid DDOS

// TODO add image position prop
// TODO add flex for segemented titel

const ContentHero: React.FC<{
    hero: LandingPageHero;
}> = ({ hero }) => {
    const {
        segmentedTitle,
        title,
        heroHighlights,
        featuredImage,
        cta,
        hideWave,
    } = hero;
    return (
        <div className="relative h-fit min-h-[75vh] bg-no-repeat lg:h-[48rem]">
            <Image
                src={featuredImage}
                placeholder="blur"
                alt="Hero Image"
                sizes="100vw"
                fill
                priority
                style={{
                    objectFit: "cover",
                    objectPosition: "top",
                }}
            />
            <div className="relative h-full min-h-[75vh] w-full bg-black bg-opacity-50">
                {!hideWave && (
                    <div className="absolute left-0 top-0 z-10 hidden h-full w-[300%] opacity-80 lg:block lg:w-full">
                        {!!hero.waveSub ? <hero.waveSub /> : <Wave />}
                    </div>
                )}
                <div className="static z-30 h-[20%] space-y-3 px-2 pt-[10rem] font-body text-white lg:absolute lg:left-[10%] lg:top-[20rem] lg:p-0">
                    {!segmentedTitle ? (
                        <h1 className="h-fit w-fit text-6xl font-[700] text-white lg:h-[4.5rem]">
                            {title}
                        </h1>
                    ) : (
                        <h1 className="flex h-fit w-fit flex-col text-6xl font-[700]">
                            {title.map(({ line, highlighted }, index) => (
                                <span
                                    className={
                                        highlighted
                                            ? "w-fit bg-white px-1 pb-2 text-violet-500 mb-3"
                                            : "text-white mb-5"
                                    }
                                    key={index}
                                >
                                    {line}
                                </span>
                            ))}
                        </h1>
                    )}
                    <span className="max-w-2xl font-body text-2xl font-[500] children:mb-3 lg:text-3xl">
                        {heroHighlights.map(({ line, highlighted }, index) => {
                            return (
                                <p
                                    key={index}
                                    className={
                                        highlighted
                                            ? "w-fit bg-white px-1 text-violet-500"
                                            : "text-white"
                                    }
                                >
                                    {line}
                                    <br />
                                </p>
                            );
                        })}
                    </span>

                    {!!cta && (
                        <a
                            href={hero.ctaHref}
                            target="_blank"
                            className="mx-auto flex w-fit font-body text-gold-1000 children:mx-auto children:my-6 children:w-fit children:py-3 children:text-2xl children:hover:scale-105"
                        >
                            <Button
                                label={hero.ctaText}
                                clickHandler={() => {}}
                                border
                            />
                        </a>
                    )}
                    {/* <div
                        className="mx-auto max-w-2xl rounded-md bg-white p-3 font-body text-xl font-[500] text-black"
                        style={{ marginTop: "2rem" }}
                    >
                        <p className="flex font-header text-xl font-bold">
                            {ctaHeader}
                        </p>
                        {ctaTexts?.map((item, index) => {
                            return (
                                <p key={index}>
                                    {item}
                                    <br />
                                </p>
                            );
                        })}
                        <a
                            className="mx-6 flex font-body text-green-1000 children:mb-1 children:mt-3 children:w-full children:text-base"
                            href="https://calendar.app.google/v5v1ovLGY685HFjp7"
                            target="_blank"
                        >
                            <Button
                                label="Get Started"
                                clickHandler={() => {}}
                                border
                            />
                        </a>
                         <div className="mx-auto w-fit font-body text-xs font-thin children:mx-1 children:underline">
                            <Link
                                href="/about/terms-and-conditions"
                                target="_blank"
                            >
                                Terms & Conditions
                            </Link>
                            and
                            <Link href="/about/privacy-policy" target="_blank">
                                Privacy Policy
                            </Link>
                            apply.
                        </div> 
                    </div>*/}
                </div>
            </div>
            {!!hero.illustration && (
                <div className="absolute left-[65%] top-[15rem] z-20 hidden h-[20rem] w-[20rem] lg:flex">
                    <hero.illustration />
                </div>
            )}
        </div>
    );
};

export default ContentHero;
