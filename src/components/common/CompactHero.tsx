/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";

import Wave1 from "../illustrations/Wave1";
import { type PropertyInfo as Meta } from "~/types/properties";

const CompactHero: React.FC<{
    asset: Pick<Meta, "propTitle" | "heroHighlights" | "featuredImage">;
    imagePosition?: string;
    hideWave?: boolean;
}> = ({ asset, imagePosition, hideWave }) => {
    const { propTitle, heroHighlights, featuredImage } = asset;
    return (
        <div className="relative h-fit w-full bg-no-repeat lg:h-[24rem]">
            <Image
                src={featuredImage}
                placeholder="blur"
                alt={propTitle}
                sizes="100vw"
                fill
                priority
                style={{
                    objectFit: "cover", // This will have the same effect as bg-cover
                    objectPosition: imagePosition ?? "50% 20%", // This will have the same effect as bg-top
                }}
            />
            <div className="relative h-full w-full bg-black bg-opacity-50">
                <div className="absolute left-0 top-0 opacity-50 z-10 hidden h-full w-[300%] scale-x-[-1] lg:block lg:w-full">
                    {!hideWave ? <Wave1 /> : <></>}
                </div>
                <div className="static z-30 h-[20%] space-y-3 px-2 pt-[10rem] font-body text-white lg:absolute lg:left-[10%] lg:top-[10rem] lg:p-0">
                    <h1 className="h-fit w-fit bg-white py-2 text-6xl font-[700] text-violet-500 lg:h-[4.5rem] lg:pt-1">
                        {propTitle}
                    </h1>
                    <p className="block h-8 w-1"> </p>
                    <span className="max-w-2xl font-body text-2xl font-[500]">
                        {heroHighlights.map((highlightText, index) => {
                            return (
                                <p key={index}>
                                    {highlightText}
                                    <br />
                                </p>
                            );
                        })}
                    </span>
                    <div className="h-[3rem] font-header text-black"></div>
                </div>
            </div>
        </div>
    );
};

export default CompactHero;
