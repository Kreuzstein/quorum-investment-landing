/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import { type ReactNode } from "react";
import Image from "next/image";
import Link from "next/link";

import { userPlaceholder } from "~/utils/imageData";
import Button from "../atoms/Button";
import { type ContactName, contacts } from "../home/Contact";

type Props = {
    contact: ContactName;
    ctaLabel?: string;
};

const FixedCta: React.FC<Props> = ({
    contact: contactName,
    ctaLabel,
}) => {
    const contactObj =
        contacts.filter(
            (c) => c.name.split(" ")[0]?.toLowerCase() === contactName,
        )[0] ?? contacts[0];

    if (!contactObj) return <></>;

    return (
        <div className="fixed bottom-0 h-20 w-full px-5 py-3 bg-black bg-opacity-60 border-t-2 border-violet-500">
            <a
                href={contactObj.meeting}
                target="_blank"
                className="flex w-full pb-2 font-body text-white children:mx-auto children:w-full children:bg-violet-500 children:py-2 children:hover:scale-105"
            >
                <Button
                    label={ctaLabel ?? "BOOK A CALL"}
                    clickHandler={() => {}}
                />
            </a>
        </div>
    );
};

export default FixedCta;
