/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { useEffect, useState } from "react";
import Link from "next/link";

import { homeBlocks, type HomeBlock } from "~/pages";
import Button from "../atoms/Button";
import Logo from "../atoms/Logo";

import { scroller } from "react-scroll";
import NavMenu from "./NavMenu";

type Props = {
    view: "home" | "prop" | "content";
};

// TODO Generic landing with blocks
// // TODO menu:
// Anton Kreuzstein, [03/01/2024 18:49]
// List with us
// Looking to invest
// Residency consultation

// Anton Kreuzstein, [03/01/2024 18:54]
// Invest
// Real estate
// Hospitality
// Stakes in companies
// Residency
// Venture capital

// Anton Kreuzstein, [03/01/2024 19:03]
// Asset management
// Current services
// Managing companies for stakes investment
// Residency deals

// Anton Kreuzstein, [03/01/2024 21:22]
// Biz
// Franchise
// Sale
// Valuationfeatureds
// Resources

// SELL YOUR BUSIESS CTA

// Anton Kreuzstein, [03/01/2024 21:23]
// Services

//     Asset Classes
//     Alternative Markets
//     Commercial Property Services
//     Specialised Services
//     View all Services

// Properties

//     For Sale
//     For Lease
//     Residential
//     View all Listings

// Insights & Research

//     Research
//     Blogs
//     News
//     Podcast
//     Our Awards & Achievements

// Exports finder

// About Us

//     Investor Relations
//     Careers
//     ESG
//     Subscribe

// Anton Kreuzstein, [03/01/2024 21:29]
// Sell Assets — Invest — About Us

// Anton Kreuzstein, [03/01/2024 21:29]
// Aout one page for eachclients/inestors/partners

// 1. **Acquire Real Estate** (formerly "List with us")
//    - List a Property
//    - Data-Driven Valuation Services
//    - Seller Advisory
//    - Transaction Management
//
// 2. **Investment Platform** (refined version of "Invest")
//    - Residency by Investment
//    - Asset Types
//      - Office Spaces
//      - Retail Venues
//      - Industrial Units
//      - Multi-family Complexes
//    - Syndicated Investments
//    - Due Diligence Process
//
// 3. **QSI Insights** (slight modification of "Insight")
//    - Market Analysis
//    - Investment Trends
//    - Regulatory Updates
//    - Webinars & Events
//    - Whitepapers & Reports
//
// 4. **Corporate Overview** (synonymously reflecting "About QSI")
//    - Company Ethos
//    - Leadership Team
//    - Corporate Social Responsibility
//    - Careers
//    - Contact Information
//

const Header: React.FC<Props> = ({ view }) => {
    const [show, setShow] = useState(true);
    const [lastScrollY, setLastScrollY] = useState(0);

    useEffect(() => {
        const controlNavbar = () => {
            if (typeof window !== "undefined") {
                if (window.scrollY >= lastScrollY) {
                    // if scroll down hide the navbar
                    setShow(false);
                } else {
                    // if scroll up show the navbar
                    setShow(true);
                }
                // remember current page location to use in the next move
                setLastScrollY(window.scrollY);
            } else {
                setShow(true);
            }
        };

        if (typeof window !== "undefined") {
            window.addEventListener("scroll", controlNavbar);

            // cleanup function
            return () => {
                window.removeEventListener("scroll", controlNavbar);
            };
        }
    }, [lastScrollY]);

    const scrollToElement = (elementName: HomeBlock) => {
        scroller.scrollTo(elementName, {
            duration: 500,
            delay: 100,
            smooth: true,
            offset: -50,
        });
    };
    return (
        <div className={`${!show && "hidden"} lg:block`}>
            <div className="fixed z-50 w-full bg-black bg-opacity-60">
                <div className="mx-auto flex h-20 w-full max-w-7xl flex-row border-b border-green-1000 font-header">
                    <Link href="/">
                        <div className="">
                            <Logo />
                        </div>
                    </Link>
                    <NavMenu />
                    {/* {view === "home" && (
                        <div className="ml-auto flex flex-row space-x-4 p-3 text-green-1000">
                            <div className="hidden lg:flex">
                                <Button
                                    label="Services"
                                    clickHandler={() =>
                                        scrollToElement(homeBlocks.services)
                                    }
                                />
                            </div>
                            <div className="hidden lg:flex">
                                <Button
                                    label="Investments"
                                    clickHandler={() =>
                                        scrollToElement(homeBlocks.coverage)
                                    }
                                />
                            </div>
                            <div className="hidden lg:flex">
                                <Button
                                    label="About Us"
                                    clickHandler={() =>
                                        scrollToElement(homeBlocks.about)
                                    }
                                />
                            </div>
                            <div className="hidden lg:flex">
                                <Button
                                    label="Let's Talk"
                                    border
                                    clickHandler={() =>
                                        scrollToElement(homeBlocks.contact)
                                    }
                                />
                            </div>
                            <div className="flex lg:hidden">
                                <Button
                                    label="Contact"
                                    border
                                    clickHandler={() =>
                                        scrollToElement(homeBlocks.contact)
                                    }
                                />
                            </div>
                        </div>
                    )} */}
                    {view === "prop" && (
                        <div className="ml-3 flex flex-row space-x-4 p-3 font-body text-green-1000 ">
                            <a
                                className="hidden children:uppercase lg:flex"
                                href="https://calendar.app.google/v5v1ovLGY685HFjp7"
                                target="_blank"
                            >
                                <Button
                                    label="Enquire Now"
                                    border
                                    clickHandler={() => {}}
                                />
                            </a>
                            <a
                                className="flex children:uppercase lg:hidden "
                                href="https://calendar.app.google/v5v1ovLGY685HFjp7"
                                target="_blank"
                            >
                                <Button
                                    label="Contact"
                                    border
                                    clickHandler={() => {}}
                                />
                            </a>
                        </div>
                    )}
                    {view !== "prop" && (
                        <div className="ml-3 flex flex-row space-x-4 p-3 font-body text-green-1000">
                            <a
                                className="hidden children:uppercase lg:flex"
                                href="https://calendar.app.google/v5v1ovLGY685HFjp7"
                                target="_blank"
                            >
                                <Button
                                    label="Let's Talk"
                                    border
                                    clickHandler={() => {}}
                                />
                            </a>
                            <a
                                className="flex children:uppercase lg:hidden "
                                href="https://calendar.app.google/v5v1ovLGY685HFjp7"
                                target="_blank"
                            >
                                <Button
                                    label="Contact"
                                    border
                                    clickHandler={() => {}}
                                />
                            </a>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Header;
