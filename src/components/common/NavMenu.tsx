/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { createElement, useEffect } from "react";
import Link from "next/link";

import { NavGroup, NavItem, navGroupLabels } from "~/types/nav";
import Chevron from "../icons/Chevron";
import Investment from "../icons/Investment";
import GlobeIcon from "../icons/GlobeIcon";
import IdCard from "../icons/IdCard";
import Cross from "../icons/Cross";
import { useNavMenuStore } from "~/stores/useNavMenuStore";
import { disableBodyScroll, enableBodyScroll } from "~/utils/common";

// TODO add animations when swapping active tabs
// TODO order of items on mobile view

const navItemList: NavItem[] = [
    {
        group: navGroupLabels.props,
        title: "Sell Your Property",
        desc: "We'll compose a premium-feel listing for your asset and prepare a thorough breakdown of investment terms that best suits you.",
        type: "featured",
        href: "/list-with-us",
        icon: Investment,
    },
    {
        group: navGroupLabels.props,
        title: "Sell Stakes in Your Assets",
        desc: "Facilitate partial sales and stakeholder investments in your assets",
        type: "regular",
        href: "/services/sell-stakes-in-commercial-assets",
    },
    {
        group: navGroupLabels.props,
        title: "Sell Digital Business",
        desc: "Sell or attract investments into SaaS, startups, and other non-traditional assets",
        type: "regular",
        href: "/services/alternative-markets",
    },
    // {
    //     group: navGroupLabels.props,
    //     title: "Commercial Property Services",
    //     desc: "Comprehensive services for managing commercial properties",
    //     type: "regular",
    //     href: "/services/commercial-property-services",
    // },
    {
        group: navGroupLabels.props,
        title: "Transaction Management",
        desc: "Support your asset sale with our legal and compliance network",
        type: "regular",
        href: "/services/transaction-management",
    },
    {
        group: navGroupLabels.props,
        title: "Valuation Services",
        desc: "Accurate valuation for commercial properties and investments",
        type: "regular",
        href: "/services/valuation-services",
    },
    {
        group: navGroupLabels.props,
        title: "Commercial Property Management",
        desc: "Optimise your commercial asset's performance and strategy",
        type: "regular",
        href: "/services/commercial-property-management",
    },
    {
        group: navGroupLabels.props,
        title: "View All Services",
        desc: "Explore our full range of commercial asset management offerings",
        type: "regular",
        href: "/services",
    },
    {
        group: navGroupLabels.invest,
        title: "Real Estate",
        desc: "Invest in residential, commercial, and industrial real estate opportunities",
        type: "regular",
        href: {
            pathname: "/properties",
            query: {
                assetClass: ["hospitality,commercial,residential"],
                deal: null,
                price: null,
                residency: null,
            },
        },
    },
    {
        group: navGroupLabels.invest,
        title: "Commercial Property",
        desc: "Explore lucrative commercial real estate investments and projects",
        type: "regular",
        href: {
            pathname: "/properties",
            query: {
                assetClass: ["hospitality,commercial"],
                deal: null,
                price: null,
                residency: null,
            },
        },
    },
    {
        group: navGroupLabels.invest,
        title: "Stakes & Venture Capital",
        desc: "Invest and acquire equity stakes in high-potential startups and established innovators",
        type: "regular",
        href: {
            pathname: "/properties",
            query: {
                assetClass: null,
                deal: ["stake,vc"],
                price: null,
                residency: null,
            },
        },
    },
    // {
    //     group: navGroupLabels.invest,
    //     title: "Hospitality",
    //     desc: "Diverse investment opportunities in the hospitality and tourism sector",
    //     type: "regular",
    //     href: "/investment-opportunities/commercial-assets",
    // },
    {
        group: navGroupLabels.invest,
        title: "Residency by Investment",
        desc: "Learn how QSI partner network enables you to acquire a residency in EU and other beneficial regions by investing into commercial assets or real estate.",
        type: "featured",
        href: "/residency-by-investment",
        icon: IdCard,
        gradientClasses: "from-violet-500 to-yellow-500",
    },
    {
        group: navGroupLabels.invest,
        title: "Digital Businesses",
        desc: "Invest in the digital economy with tech startups and e-commerce ventures",
        type: "regular",
        href: {
            pathname: "/properties",
            query: {
                assetClass: ["digital"],
                deal: null,
                price: null,
                residency: null,
            },
        },
    },
    {
        group: navGroupLabels.invest,
        title: "Syndicated Investments",
        desc: "Pool resources with other investors for large-scale investment ventures",
        type: "regular",
        href: {
            pathname: "/properties",
            query: {
                assetClass: null,
                deal: ["syndicate"],
                price: null,
                residency: null,
            },
        },
    },
    {
        group: navGroupLabels.invest,
        title: "View All Listings",
        desc: "Discover our comprehensive suite of investment opportunities",
        type: "regular",
        // href: "/investment-opportunities",
        href: {
            pathname: "/properties",
            query: {
                assetClass: null,
                deal: null,
                price: null,
                residency: null,
            },
        },
    },
    // {
    //     group: navGroupLabels.content,
    //     title: "Investment Trends & News",
    //     desc: "Stay updated with the latest trends and news in the investment world",
    //     type: "regular",
    //     href: "/publications/news",
    // },
    // {
    //     group: navGroupLabels.content,
    //     title: "Knowledge Base",
    //     desc: "Access a repository of articles enriching your investment knowledge",
    //     type: "regular",
    //     href: "/publications/articles",
    // },
    // {
    //     group: navGroupLabels.content,
    //     title: "Market Analysis",
    //     desc: "In-depth analysis of current market conditions and future trends",
    //     type: "regular",
    //     href: "/publications/market-analysis",
    // },
    // {
    //     group: navGroupLabels.content,
    //     title: "Whitepapers & Reports",
    //     desc: "Explore detailed research, whitepapers, and industry reports",
    //     type: "regular",
    //     href: "/publications/research",
    // },
    // {
    //     group: navGroupLabels.content,
    //     title: "Regulatory Updates",
    //     desc: "Read about the latest changes in investment laws and regulations",
    //     type: "regular",
    //     href: "/publications/regulatory-updates",
    // },
    // {
    //     group: navGroupLabels.content,
    //     title: "Events & Webinars",
    //     desc: "Join industry-leading events and webinars for insights and networking",
    //     type: "regular",
    //     href: "/publications/events",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Our Partner Network",
    //     desc: "Discover QSI Partner Network: delivering unparalleled investment solutions. Connect with experts in compliance, governance, advisory, and more!",
    //     type: "featured",
    //     href: "/qsi-partner-network",
    //     icon: GlobeIcon,
    //     gradientClasses: "from-violet-500 to-green-500",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Our History",
    //     desc: "Learn about QSI's journey and milestones in transforming the investment landscape",
    //     type: "regular",
    //     href: "/about/history",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Investor Relations",
    //     desc: "Key investor information including financials, governance, and stock information",
    //     type: "regular",
    //     href: "/about/investor-relations",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Leadership Team",
    //     desc: "Meet the hard workers driving QSI's strategic direction and success",
    //     type: "regular",
    //     href: "/about/leadership-team",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Careers",
    //     desc: "Join our team and shape the future of investment services with QSI",
    //     type: "regular",
    //     href: "/about/careers",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Corporate Social Responsibility",
    //     desc: "Our commitment to ethical practiceLinks, sustainability, and community engagement",
    //     type: "regular",
    //     href: "/about/csr-and-esg",
    // },
    // {
    //     group: navGroupLabels.about,
    //     title: "Contact Us",
    //     desc: "Reach out to QSI for investment inquiries, support, and collaboration opportunities",
    //     type: "regular",
    //     href: "/about/contact",
    // },
];

const NavMenu: React.FC = () => {
    const store = useNavMenuStore();

    useEffect(() => {
        return () => {
            enableBodyScroll();
            store.reset();
            return;
        };
    }, []);

    return (
        <>
            {/* Wide screen nav menu */}
            <nav
                className="ml-auto hidden h-full font-semibold text-green-1000 lg:flex"
                onMouseLeave={store.close}
            >
                <div
                    className="flex h-full flex-row space-x-6"
                    onMouseEnter={store.open}
                >
                    {Object.values(navGroupLabels).map((item, index) => (
                        <NavGroupButtonWideScreen label={item} key={index} />
                    ))}
                </div>
                <div
                    className={
                        "absolute right-0 top-20 w-full max-w-[48rem] rounded-b-md bg-black bg-opacity-80 p-5 transition-all wider-than-header:right-[calc(50%-40rem)] wider-than-header:w-3/5" +
                        " " +
                        (store.navMenuOpened && store.selectedNavGroup
                            ? "block"
                            : "hidden")
                    }
                >
                    <ul className="grid grid-flow-col grid-rows-3 gap-2 font-body">
                        {navItemList
                            .filter((i) => i.group === store.selectedNavGroup)
                            .map((item, index) => (
                                <NavItemButtonWideScreen
                                    item={item}
                                    key={index}
                                />
                            ))}
                    </ul>
                </div>
            </nav>

            {/* Tablet and  mobile nav menu */}
            <nav className="mx-auto flex h-full text-xl font-semibold text-green-1000 lg:hidden">
                <button
                    className="my-auto flex flex-row space-x-1"
                    onClick={
                        store.navMenuOpened
                            ? () => {
                                  store.close();
                                  enableBodyScroll();
                                  return;
                              }
                            : () => {
                                  disableBodyScroll();
                                  store.setSelectedNavGroup(undefined);
                                  store.open();
                                  return;
                              }
                    }
                >
                    <span>Menu</span>
                    <span className="my-auto rotate-[-90deg] scale-125">
                        {store.navMenuOpened ? <Cross /> : <Chevron />}
                    </span>
                </button>
                <div
                    className={
                        "absolute right-0 top-20 min-h-[calc(100vh-5rem)] w-full max-w-[48rem] rounded-b-md bg-black p-5 transition-all md:w-3/5" +
                        " " +
                        (store.navMenuOpened ? "block" : "hidden")
                    }
                >
                    {store.selectedNavGroup == undefined ? (
                        // Menu groups list
                        <div className="my-10 flex h-full w-full flex-col gap-y-10">
                            {Object.values(navGroupLabels).map(
                                (item, index) => (
                                    <NavGroupButtonMobile
                                        label={item}
                                        key={index}
                                    />
                                ),
                            )}
                        </div>
                    ) : (
                        // Items under the selected menu group
                        <div className="my-6 flex h-[calc(100vh-9rem)] w-full flex-col gap-y-6 overflow-y-auto overflow-x-hidden">
                            <button
                                className="my-auto flex w-full flex-row font-body font-thin"
                                onClick={() => {
                                    store.setSelectedNavGroup(undefined);
                                    return;
                                }}
                            >
                                <span className="my-auto h-fit">
                                    <Chevron />
                                </span>
                                <span className="ml-0 h-fit">Main Menu</span>
                            </button>
                            <div className="text-3xl">
                                {store.selectedNavGroup}
                            </div>
                            <ul className="grid grid-flow-row grid-cols-2 gap-2 font-body">
                                {navItemList
                                    .toSorted((a, _b) =>
                                        a.type === "featured" ? 0 : 1,
                                    )
                                    .filter(
                                        (i) =>
                                            i.group === store.selectedNavGroup,
                                    )
                                    .map((item, index) => (
                                        <NavItemButtonMobile
                                            item={item}
                                            key={index}
                                        />
                                    ))}
                            </ul>
                        </div>
                    )}
                </div>
            </nav>
        </>
    );
};

export default NavMenu;

const NavGroupButtonWideScreen: React.FC<{ label: NavGroup }> = ({ label }) => {
    const store = useNavMenuStore();
    return (
        <button
            className="group flex h-full"
            onMouseEnter={() => store.setSelectedNavGroup(label)}
        >
            <div className="my-auto flex flex-row">
                <span>{label}</span>
                <span className="rotate-[-90deg] transition-transform group-hover:rotate-90">
                    <Chevron />
                </span>
            </div>
        </button>
    );
};

const NavGroupButtonMobile: React.FC<{ label: NavGroup }> = ({ label }) => {
    const store = useNavMenuStore();
    return (
        <button
            className="flex w-full text-3xl"
            onClick={() => store.setSelectedNavGroup(label)}
        >
            <div className="my-auto flex w-full flex-row">
                <span className="ml-0 h-fit">{label}</span>
                <span className="my-auto ml-auto h-fit rotate-180 scale-125">
                    <Chevron />
                </span>
            </div>
        </button>
    );
};

const NavItemButtonWideScreen: React.FC<{ item: NavItem }> = ({ item }) => {
    const { title, desc, type, href } = item;
    if (type === "featured") {
        const gradientClasses =
            item.gradientClasses ?? "from-violet-500 to-violet-1000";
        return (
            <li
                className={
                    "group row-span-3 rounded-md bg-gradient-to-tr text-white" +
                    " " +
                    gradientClasses
                }
            >
                <Link href={href}>
                    <div className="h-full w-full bg-black bg-opacity-0 transition-colors group-hover:bg-opacity-30">
                        <div className="flex h-full flex-col p-3">
                            <span className="mb-4 mt-auto flex font-thin">
                                {createElement(item.icon)}
                            </span>
                            <p className="text-lg">{title}</p>
                            <p className="mb-auto font-body text-sm font-[400]">
                                {desc}
                            </p>
                        </div>
                    </div>
                </Link>
            </li>
        );
    }
    return (
        <li className="rounded-md p-2 transition-colors hover:bg-violet-500 hover:bg-opacity-50">
            <Link href={href}>
                <p>{title}</p>
                <p className="font-body text-sm font-[400] text-white">
                    {desc}
                </p>
            </Link>
        </li>
    );
};

const NavItemButtonMobile: React.FC<{ item: NavItem }> = ({ item }) => {
    const { title, desc, type, href } = item;
    if (type === "featured") {
        const gradientClasses =
            item.gradientClasses ?? "from-violet-500 to-violet-1000";
        return (
            <li
                className={
                    "group col-span-2 rounded-md bg-gradient-to-tr text-white" +
                    " " +
                    gradientClasses
                }
            >
                <Link href={href}>
                    <div className="h-full w-full bg-black bg-opacity-0 transition-colors group-hover:bg-opacity-30">
                        <div className="flex h-full flex-col p-3">
                            <span className="mb-4 mt-auto flex font-thin">
                                {createElement(item.icon)}
                            </span>
                            <p className="text-lg">{title}</p>
                            <p className="mb-auto font-body text-sm font-[400]">
                                {desc}
                            </p>
                        </div>
                    </div>
                </Link>
            </li>
        );
    }
    return (
        <li className="rounded-md p-2 transition-colors hover:bg-violet-500 hover:bg-opacity-50">
            <Link href={href}>
                <p>{title}</p>
                <p className="font-body text-sm font-[400] text-white">
                    {desc}
                </p>
            </Link>
        </li>
    );
};
