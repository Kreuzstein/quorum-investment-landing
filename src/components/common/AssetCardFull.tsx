/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";
import Link from "next/link";

import { type PropertyListingFull } from "~/types/properties";
import Button from "../atoms/Button";
import Tag from "../atoms/Tag";
import Location from "../icons/Location";
import Passport from "../icons/Passport";
import { contacts } from "../home/Contact";

const AssetCardFull: React.FC<{ property: PropertyListingFull }> = ({
    property,
}) => {
    return (
        <div className="group h-full w-full cursor-pointer rounded-md bg-white bg-opacity-10 shadow-2xl transition-colors hover:bg-opacity-5">
            <Link href={"/properties/" + property.slug}>
                <div className="relative flex h-full w-full flex-col p-3">
                    <div className="z-20 flex h-[20rem] w-full flex-col">
                        <div className="mt-auto w-fit bg-white px-1 font-body text-2xl font-bold text-violet-500">
                            {property.propTitle +
                                " " +
                                property.assetClassLabel}
                        </div>
                        <div className="mb-3 mt-0 w-fit px-1 font-header text-xl font-[400] tracking-widest text-white">
                            {property.tagline}
                        </div>
                    </div>
                    <div className=" flex h-fit w-full flex-col space-y-5 font-body text-white">
                        <div className="flex w-full flex-col text-base children:flex children:w-fit children:gap-1 children:p-1 md:flex-row md:text-sm">
                            <div className="mx-auto md:mr-auto">
                                <Location />
                                <span>{property.addressLong}</span>
                            </div>
                            <div className="mx-auto bg-blue-700 font-semibold md:ml-auto">
                                <Passport />
                                <span>EU Residence Eligible</span>
                            </div>
                        </div>
                        {property.opportunities.map((o, oIndex) => (
                            <div key={oIndex}>
                                <div className="w-full text-base">
                                    <Tag label={o.title} />
                                    {o?.blurb}
                                </div>
                                <div className="mt-3 flex w-full text-base">
                                    <div className="flex w-1/2">
                                        {o.bullets?.map(
                                            ({ label, value }, bIndex) => (
                                                <div
                                                    className="flex flex-col gap-1 md:flex-row"
                                                    key={bIndex}
                                                >
                                                    <div className="mt-auto flex font-body font-light">
                                                        {label}
                                                    </div>
                                                    <div className="flex font-header text-base font-semibold md:ml-auto">
                                                        {value}
                                                    </div>
                                                </div>
                                            ),
                                        )}
                                    </div>
                                    <div className="flex w-1/2">
                                        <div className="ml-auto mt-auto w-fit text-base">
                                            {o.priceLabel}{" "}
                                            <span className="font-header font-semibold text-violet-1000">
                                                {o.priceAmount}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                        {
                            (() => {
                                const contactName = property.contact;
                                const contactObj =
                                    contacts.filter(
                                        (c) => c.name.split(" ")[0]?.toLowerCase() === contactName,
                                    )[0] ?? contacts[0];
                                if (!contactObj) return <></>;
                                return (
                                    <a
                                    href={contactObj.meeting}
                                    target="_blank"
                                    className="flex w-full font-body text-white children:mx-auto children:w-full children:bg-violet-500 children:py-2 children:hover:scale-105"
                                    onClick={(event) => event.stopPropagation()}
                                >
                                    <Button
                                        label="ENQUIRE NOW"
                                        clickHandler={() => {}}
                                    />
                                </a>
                                )
                            })()
                        }
                    </div>

                    <div className="absolute left-0 top-0 z-10 h-[20rem] w-full overflow-hidden rounded-t-md">
                        <Image
                            className="transition-transform group-hover:scale-105"
                            src={property.featuredImage}
                            alt={property.propTitle}
                            fill
                            sizes="20vw"
                            style={{
                                objectFit: "cover",
                                objectPosition: "center",
                            }}
                        />
                        <div className="relative z-20 flex h-full w-full bg-black bg-opacity-10 transition-colors group-hover:bg-opacity-30">
                            <div className="m-auto flex h-fit w-fit shrink origin-right bg-white p-1 font-body text-violet-500 opacity-0 transition-opacity group-hover:opacity-100">
                                <Button
                                    label="More Details"
                                    border
                                    clickHandler={() => {
                                        return;
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    );
};

export default AssetCardFull;
