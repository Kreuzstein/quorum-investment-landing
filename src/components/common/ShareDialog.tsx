/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import {
    MutableRefObject,
    createElement,
    useEffect,
    useRef,
    useState,
} from "react";
import Image from "next/image";

import KeyCap from "../atoms/KeyCap";
import Cross from "../icons/Cross";
import Copy from "../icons/Copy";
import Mail from "../icons/Mail";
import Smartphone from "../icons/Smartphone";

import { useShareStore } from "~/stores/useShareStore";
import {
    checkForDesktopUseragent,
    encodeRFC3986URIComponent,
    writeToClipboard,
} from "~/utils/common";

import LineIcon from "../icons/LineIcon";
import TelegramIcon from "../icons/TelegramIcon";
import WhatsAppIcon from "../icons/WhatsAppIcon";
import Share from "../icons/Share";

const shareOptionTypes = {
    externalLink: "externalLink",
    action: "action",
} as const;

export type ShareOption = {
    icon: React.FC;
    label: string;
    hotkey: string | undefined;
    hideOnDesktop?: boolean;
} & (
    | {
          type: typeof shareOptionTypes.action;
          action: () => void | Promise<void>;
      }
    | {
          type: typeof shareOptionTypes.externalLink;
          externalLink: string;
          ref: MutableRefObject<null | HTMLAnchorElement>;
      }
);

const URL_BASE = "https://investments.quorum.ltd/";

type Props = {};
const ShareDialog: React.FC<Props> = ({}) => {
    const store = useShareStore();

    const copyLink = async () => {
        await writeToClipboard(URL_BASE + "properties/" + store.slug);
        return;
    };
    const mailButtonRef: MutableRefObject<null | HTMLAnchorElement> =
        useRef(null);

    const allShareOptions: ShareOption[] = [
        {
            type: shareOptionTypes.action,
            icon: Copy,
            label: "Copy Link",
            hotkey: "C",
            action: copyLink,
        },
        {
            type: shareOptionTypes.externalLink,
            icon: Mail,
            label: "Email",
            // hotkey: "M",
            hotkey: undefined,
            externalLink: `mailto:?subject=${encodeRFC3986URIComponent(
                store.descLong,
            )}&body=${encodeRFC3986URIComponent(
                store.descShort +
                    ". " +
                    "More info: " +
                    URL_BASE +
                    "properties/" +
                    store.slug +
                    ".",
            )}`,
            ref: mailButtonRef,
        },
        {
            type: shareOptionTypes.externalLink,
            icon: Smartphone,
            label: "SMS | iMessage",
            hotkey: undefined,
            externalLink: `sms:?body=${encodeRFC3986URIComponent(
                "Investment opportunity with QSI: " +
                    URL_BASE +
                    "properties/" +
                    store.slug,
            )}`,
            ref: useRef(null),
        },
        {
            type: shareOptionTypes.externalLink,
            icon: LineIcon,
            label: "LINE",
            hotkey: undefined,
            externalLink: `line://msg/text/${encodeRFC3986URIComponent(
                store.descLong +
                    ". " +
                    store.descShort +
                    ". " +
                    "More info: " +
                    URL_BASE +
                    "properties/" +
                    store.slug +
                    ".",
            )}`,
            ref: useRef(null),
        },
        {
            type: shareOptionTypes.externalLink,
            icon: WhatsAppIcon,
            label: "WhatsApp",
            hotkey: undefined,
            externalLink: `https://wa.me/?text=${encodeRFC3986URIComponent(
                store.descLong +
                    ". " +
                    store.descShort +
                    ". " +
                    "More info: " +
                    URL_BASE +
                    "properties/" +
                    store.slug +
                    ".",
            )}`,
            ref: useRef(null),
        },
        {
            type: shareOptionTypes.externalLink,
            icon: TelegramIcon,
            label: "Telegram",
            hotkey: undefined,
            externalLink: `tg://msg?text=${encodeRFC3986URIComponent(
                store.descLong +
                    ". " +
                    store.descShort +
                    ". " +
                    "More info: " +
                    URL_BASE +
                    "properties/" +
                    store.slug +
                    ".",
            )}`,
            ref: useRef(null),
        },
        {
            type: shareOptionTypes.action,
            icon: Share,
            label: "Other",
            hotkey: undefined,
            action: async () => {
                try {
                    await navigator.share({
                        url: URL_BASE + "properties/" + store.slug,
                        text: store.descShort,
                        title: store.descLong,
                    });
                } catch (err) {}
                return;
            },
            hideOnDesktop: true,
        },
        // WECHAT  -- https://github.com/JasonBoy/wechat-jssdk/blob/master/lib/client.js https://www.npmjs.com/package/wechat-jssdk/v/5.0.8#oauth
        // SIGNAL -- probalby not viable
        // FB MESSENGER
        // TODO FB
        // TWITTER
        // TODO hide other on desktop
    ];

    // Keyboard prompts
    useEffect(() => {
        function handleKeyPress(event: any) {
            switch (event.keyCode) {
                case 67: // C code
                    copyLink();
                    break;
                // case 77: // M code
                //     (mailButtonRef.current as HTMLAnchorElement).click();
                //     break;
                case 27: // Esc key code
                    store.close();
                    break;
                default:
                    break;
            }
        }

        // Attach event listener to window for key down events
        window.addEventListener("keydown", handleKeyPress);

        // Cleanup: remove event listener when the component unmounts
        return () => {
            window.removeEventListener("keydown", handleKeyPress);
        };
    }, [store.dialogOpened]);

    // Remove some share options on desktop
    const assignNeededShareOptions = (removeNonDesktopItems: boolean) => {
        store.setShareOptions(
            allShareOptions.filter(
                (o) => !(removeNonDesktopItems && o.hideOnDesktop),
            ),
        );
        return;
    };
    useEffect(() => {
        checkForDesktopUseragent(assignNeededShareOptions);
        return () => {};
    }, [store.dialogOpened]);

    return (
        <div
            className="relative z-[1001] m-auto flex h-full w-full flex-col gap-4 bg-white p-5 font-body shadow-2xl transition-colors lg:h-fit lg:max-w-2xl lg:border-4 lg:border-double lg:border-violet-500"
            onClick={(event) => {
                event.stopPropagation();
                return;
            }}
        >
            <div
                className="flex cursor-pointer flex-row gap-2 text-2xl transition-colors hover:text-violet-1000 lg:text-base"
                onClick={store.close}
            >
                <div className="my-auto flex">
                    <Cross />
                </div>
                Close
                <KeyCap palette="light">Esc</KeyCap>
            </div>
            <div className="flex font-header text-2xl font-semibold">
                Share This Opportunity
            </div>
            <div className="flex flex-row  gap-2">
                <div className="relative my-auto flex h-20 min-h-[72px] w-20 min-w-[72px] overflow-clip rounded-xl border-4 border-violet-500">
                    {store.imageSrc ? (
                        <Image
                            src={store.imageSrc}
                            alt={store.title || store.slug}
                            fill
                            style={{
                                objectFit: "cover",
                                objectPosition: "center",
                            }}
                            // TODO change placeholder
                            placeholder="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAUfSURBVHgB7ZxNSFRhFIaP1aJxVTmbylxUmGRRmC2McZM1KysJMqiFLQyqRZs2LYMgImoTES1aZAshW2TZavqDHPohs4ICKzQa/4LUdioY1H3P9MkECqLz3XmvnAeGmbLuOPf5fs853xTVN576IwYNS8SgwoSQYULIMCFkmBAyTAgZJoQME0KGCSHDhJBhQsgwIWSYEDJMCBkmhAwTQoYJIcOEkLFMIkhx8XKpKN8QPNYHr2MSL1mlfz8+PiGZ/iHJDAxJz5fe4M+TEjUiJQQCEjXVUrV9i0oBI6O/9AHiJSuDn1VO//v0yy5p73g0/fMoEAkhuPkN9UlJ1iW01adfvpHu95+0J8zUC5y4ik0b5PKFakk9SUv7w1QkekwRe9UJWv3ZMyf0OfWkM7ixj+Z8Y/F/knW1KhK95OKVG/S9hVqIkwFu3roTzAt9Mh/KStfI6VNN+ppdCu0qK1cGbuJ8ZQAMbbgGwDXd/MMIrZCG+r0qJV8tGte4er1FimMxOX3ymLBCKQQTcmJXtbS2Pcjr8IKegskdkz6WzYxQCmnYt1dFYHWUb3BNXBvvwQidELReDFXtHSnxRepxJ20voROCTR/o/vBJfJF+9VaXzlXbNwsbdELK1q2Rns99XjdxGmIJ5pOK8o3CBuWQlRkYFN8g5hUvWSFsUAlBoBCEsXEbGRnT92Pbk3AJiWVvThgxp/GJyX/vGRMmqITkRm19496DLYxCN4egd8Tj/oVguMLkzgadEKx+ykrXim8Qms/0DwsbdEK6330Mlr6rvU62GK7KSlcHe52PwgadEGzaQHJ3rfgCgUuAJBcbhHPIhIbakVTy0UvQOzBcIb3LmBehDC4iGRXkzqS56bDkGxfWR66dEUohaLkIk6NgIZ9RWVwLYX3mwgfaIgeEyZF6deP9Qls0hkBcK/2iS/PyrFBXndxsadP9wkKk5FasQAauyczS8sqd54SY110f9NkNN9g4Yq8yF5B5PHn8qGyt3KQVKy2t94Qd+jIgB6LAzccO64SM8R+ViVi2joyO/Rf7QtIJ+5hEzU7tHT2fe3WIWkiRRJhERogDE70WwQU3frZlsfai/sFIiXBETkgumPQR98qN2Lra3qgSyWJrB258lG/+TNhxBDJMCBkmhAwTQoYJIcOEkGFCyDAhZJgQMkwIGQUPnSBAWLVti0Zocd7clZOGCQKRKAlCBLnQmcSCCUE4vWFfUp8BIrQaSp8I/+iyhurrspFjRIeRyEIRRCEIPdqLfAbyGhCB1ug+fKFbJiLH+J2Se2qncy6FOLEbqpDErh1y5NABfd3adj8Q8VYYSdTs0N4LMTjn6ONo3WyElsJFCvZI437p7fsu5y9dk95v/cJKZmBYs5Go30IyTIoktERXKEKQ5Ws6elCHp6s3bsvU1G9hB3PZs+evpr8NAnmX4R8/xTfel734QOgZGIvZKz5movVuh8pobmoM5ZiEdyG5XwAQRVDaii8cwLgVxlFqr0Iggr1ScC5kz8x36nyC/ZJPvApxBW6FWtPnk9TTtO6VdJL3iFchWKVka6ei2zsc7ii1O0fvC29CMFzhgXDEYgGHidzn8oVXIYDx2Nh86fmabVzY1fvCo5DsF1MiPrVYcCWrPgOgFn4nw4SQYULIMCFkmBAyTAgZJoQMb0JwcGaxhE0c2aN0fV6jD5E+QbUYsSGLDBNChgkhw4SQYULIMCFkmBAyTAgZJoQME0KGCSHDhJBhQsgwIWSYEDJMCBkmhAwTQsZfTFwV+YvpYyIAAAAASUVORK5CYII="
                        />
                    ) : (
                        <></>
                    )}
                </div>
                <div className="flex flex-col">
                    <div className="w-fit bg-violet-500 px-1 font-body text-2xl font-[600] text-white">
                        {store.title}
                    </div>
                    <div className="font-body text-sm font-light">
                        {store.address}
                    </div>
                    <div className="font-body">{store.descShort}</div>
                </div>
            </div>
            <div className="grid grid-cols-1 gap-2 text-xl lg:grid-cols-2 overflow-y-scroll">
                {store.shareOptions.map((item, index) => {
                    return <ShareOptionButton key={index} shareOption={item} />;
                })}
            </div>
        </div>
    );
};

export default ShareDialog;

const ShareOptionButton: React.FC<{
    shareOption: ShareOption;
}> = ({ shareOption }) => {
    const { icon, label, type, hotkey } = shareOption;

    if (type === shareOptionTypes.action) {
        return (
            <div
                className="group col-span-1 w-full cursor-pointer rounded-md border-2 border-gray-400 border-opacity-40 hover:bg-gray-100"
                onClick={shareOption.action}
            >
                <div className="flex w-full  flex-row  gap-2  p-5 align-middle font-semibold transition-colors">
                    <div className="my-auto flex scale-[1.2]">
                        {createElement(icon)}
                    </div>
                    <div className="mr-auto flex">{label}</div>
                    {hotkey ? <KeyCap palette="light">{hotkey}</KeyCap> : <></>}
                </div>
            </div>
        );
    }
    if (type === shareOptionTypes.externalLink) {
        return (
            <a
                className="group col-span-1 w-full cursor-pointer rounded-md border-2 border-gray-400 border-opacity-40 hover:bg-gray-100"
                target="_blanc"
                href={shareOption.externalLink}
                ref={shareOption.ref}
            >
                <div className="flex w-full  flex-row  gap-2  p-5 align-middle font-semibold transition-colors">
                    <div className="my-auto flex scale-[1.2]">
                        {createElement(icon)}
                    </div>
                    <div className="mr-auto flex">{label}</div>
                    {hotkey ? <KeyCap palette="light">{hotkey}</KeyCap> : <></>}
                </div>
            </a>
        );
    }
    return <></>;
};
