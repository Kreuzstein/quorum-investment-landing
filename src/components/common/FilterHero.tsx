/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { useRouter } from "next/router";
import Image from "next/image";

import { type FilterPageHero } from "~/types/content";
import MultiSelect from "../atoms/Multiselect";
import Wave from "../illustrations/Wave1";
import { usePropertySearchStore } from "~/stores/usePropertySearchStore";
import { useEffect } from "react";

const FilterHero: React.FC<{
    hero: FilterPageHero;
}> = ({ hero }) => {
    const { featuredImage, title, hideWave } = hero;

    const router = useRouter();
    const store = usePropertySearchStore();

    useEffect(() => {
        const parseQuery = () => {
            // don't use router query as it won't parse manually entered qs params
            const path = router.asPath;
            const matches = /\?(.*)/.exec(path);
            if (!matches || !matches[1]) return;
            const qs = matches[1];
            
            for (const field of qs.split("&")) {
                const [fieldId, selectedOptionsString] = field.split("=");
                if (!fieldId) continue;
                if (!selectedOptionsString) {
                    store.setSelectedOptions(
                        decodeURIComponent(fieldId),
                        [],
                    );
                    continue;                    
                }
                const selectedOptions = decodeURIComponent(decodeURIComponent(selectedOptionsString)).split(",");
                store.setSelectedOptions(
                    decodeURIComponent(fieldId),
                    selectedOptions,
                );
            }

            return;
        };
        parseQuery();
        return () => {};
    }, [router.asPath]);

    useEffect(() => {
        const handleFilterChange = () => {
            const newQuery = new Map<string, string>();
            for (const [fieldId, filter] of Object.entries(store.filters)) {
                if (filter.type !== "multi") continue;
                const selectedOptions = Array.from(new Set(filter.selectedOptions));
                newQuery.set(fieldId, selectedOptions.join(","));
            }

            // Remove any filters that have null or undefined values to clean up the URL
            for (const [fieldId, selectedOptionsString] of newQuery) {
                if (!selectedOptionsString) {
                    newQuery.delete(fieldId);
                }
            }

            // Use 'replace' to avoid adding a new entry in the history stack
            router.replace(
                {
                    pathname: router.pathname,
                    query: Object.fromEntries(newQuery),
                },
                undefined,
                { shallow: true }, // NB! Shallow routing can be used to change the URL without fetching new data
            );
            return;
        };
        handleFilterChange();
        return () => {};
    }, [store.filters]);

    return (
        <div className="relative h-fit w-full bg-no-repeat lg:h-[36rem]">
            <Image
                src={featuredImage}
                placeholder="blur"
                alt={title}
                sizes="100vw"
                fill
                priority
                style={{
                    objectFit: "cover",
                    objectPosition: "50% 20%",
                }}
            />
            <div className="relative h-full w-full bg-black bg-opacity-50">
                {!hideWave && (
                    <div className="absolute left-0 top-80 z-10 hidden h-full w-[300%] scale-y-[-1] opacity-50 lg:block lg:w-full">
                        {!!hero.waveSub ? <hero.waveSub /> : <Wave />}
                    </div>
                )}
                <div className="z-30 flex h-[20%] w-full flex-col space-y-10 px-2 pb-[5rem] pt-[10rem] font-body text-white lg:absolute lg:mt-[20rem] lg:p-0">
                    <h1 className="mx-auto h-fit w-fit text-6xl font-[700] text-white bg-violet-500 p-1 pb-3 lg:h-[4.5rem]">
                        {title}
                    </h1>
                    <div className="mx-auto flex w-full max-w-7xl flex-col gap-5 rounded-md bg-black bg-opacity-60 p-3 md:flex-row">
                        {Object.entries(store.filters).map(
                            ([fieldId, filter], index) => {
                                if (filter.type === "multi") {
                                    return (
                                        <div className="flex-1" key={index}>
                                            <MultiSelect
                                                filter={filter}
                                                selectOption={(value) =>
                                                    store.selectOption(
                                                        fieldId,
                                                        value,
                                                    )
                                                }
                                                deselectOption={(value) =>
                                                    store.deselectOption(
                                                        fieldId,
                                                        value,
                                                    )
                                                }
                                            />
                                        </div>
                                    );
                                }
                                return <></>;
                            },
                        )}
                        {/* add filter reset? */}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FilterHero;
