/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Markdown from "react-markdown";
import gfm from "remark-gfm";

import { Block } from "~/types/content";
import Tag from "../atoms/Tag";
import Header2 from "../atoms/Header2";

const TextBlock: React.FC<{ block: Block; paddedLists?: boolean }> = ({
    block,
    paddedLists,
}) => {
    const { body, tag, title, contentType } = block;
    return (
        <div className="flex flex-col px-2 font-body">
            <div className="flex flex-col">
                <Tag label={tag} />
                <Header2 title={title} />
            </div>
            <div>
                <Markdown
                    remarkPlugins={[gfm]}
                    components={{
                        h3(props) {
                            const { node, ...rest } = props;
                            return (
                                <h3
                                    className="font-body text-2xl font-light"
                                    // className="mt-2 text-lg font-semibold"
                                    {...rest}
                                />
                            );
                        },
                        h4(props) {
                            const { node, ...rest } = props;
                            return (
                                <h4
                                    // className="mt-2 font-semibold"
                                    className="mt-2 text-lg font-semibold"
                                    {...rest}
                                />
                            );
                        },
                        p(props) {
                            const { node, ...rest } = props;
                            return <p className="my-5" {...rest} />;
                        },
                        ul(props) {
                            const { node, ...rest } = props;
                            return (
                                <ul
                                    className={
                                        "mb-5 list-inside list-disc" +
                                        " " +
                                        (!!paddedLists ? "children:py-1" : "")
                                    }
                                    {...rest}
                                />
                            );
                        },
                        ol(props) {
                            const { node, ...rest } = props;
                            return (
                                <ol
                                    className={
                                        "mb-5 list-decimal" +
                                        " " +
                                        (!!paddedLists ? "children:py-1" : "")
                                    }
                                    {...rest}
                                />
                            );
                        },
                        a(props) {
                            const { node, ...rest } = props;
                            return (
                                <a
                                    target="_blank"
                                    href={rest.href}
                                    className="my-5 font-semibold text-violet-500 underline"
                                    {...rest}
                                />
                            );
                        },
                        blockquote(props) {
                            const { node, ...rest } = props;
                            return <blockquote className="ml-5 my-2 bg-black bg-opacity-20 w-fit text-black rounded-md text-sm px-1 h-fit " {...rest} />;
                        }
                    }}
                >
                    {body}
                </Markdown>
            </div>
            {contentType === "opportunity" ? (
                <div className="text-xl">
                    {block.priceLabel}{" "}
                    <span className="font-header font-semibold text-violet-1000">
                        {block.priceValue}
                    </span>
                    .
                </div>
            ) : (
                <></>
            )}
        </div>
    );
};

export default TextBlock;
