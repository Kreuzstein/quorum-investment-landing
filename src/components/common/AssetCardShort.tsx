/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Link from "next/link";
import Image from "next/image";

import { Opportunity, PropertyInfo } from "~/types/properties";
import featuredImage from "~/../public/Rīga,_Benjamiņu_nams_2002-10-02.jpeg";
import Button from "../atoms/Button";

const meta: PropertyInfo = {
    slug: "house-of-benjamins",
    pageTitle: "House of Benjamins Hotel Opportunity | For Sale | Riga, Latvia",
    propTitle: "House of Benjamins",
    tagline: "Baptism Of Brilliance",
    addressShort: {
        flag: "🇱🇻",
        location: "12 Kr. Barona, Riga",
    },
    addressLong: "12 Krišjāņa Barona, Rīga",
    coords: { x: 24.120383, y: 56.950531 },
    // coords: [56.95043173818938, 24.120383710070335],
    heroHighlights: [
        "60 hotel rooms, restaurant, banquet halls, casino.",
        "Furniture & fixtures included.",
    ],
    descShort:
        "Buy Hotel Asset | Invest in Managing Company | Get EU Residency",
    descLong:
        "House of Benjamins Hotel, Riga: Ace Opportunity with Quorum Strategic Investments",
    featuredImage,
};

const asset = {
    assetClassLabel: "Hotel",
    tags: ["HOTEL", "CASINO", "EU RESIDENCY"],
    listingBadge: "For Sale",
    highlightsShort: "60 beds | furniture included",
};

const oppData: Opportunity[] = [
    {
        title: "Sale opportunity",
        bullets: [
            {
                label: "Total area",
                value: "4,695 m²",
            },
        ],
        priceLabel: "Asking price",
        priceAmount: "€7,000,000",
        dealType: "sale",
        residenceOption: "eu-pr",
        priceBracket: "5M-to-10M",
        blurb: "",
    },
    {
        title: "Investment opportunity",
        details: ["Stake in the managing company"],
        bullets: [
            {
                label: "Returns range",
                value: "5% ROI",
            },
        ],
        priceLabel: "Invest from",
        priceAmount: "€80,000",
        tags: ["EU Residency"],
        dealType: "stake",
        residenceOption: "eu-pr",
        priceBracket: "under-200K",
        blurb: "",
    },
];

const AssetCardShort: React.FC = () => {
    return (
        <div className="group h-full w-full cursor-pointer rounded-md bg-black bg-opacity-60 shadow-2xl transition-colors">
            <Link href="/properties/house-of-benjamins">
                <div className="relative flex h-full w-full flex-col p-3">
                    <div className="z-20 flex h-3/4 w-full flex-col">
                        <div className="flex w-full flex-row">
                            <div className="h-fit w-fit bg-black px-1">
                                <span className="my-auto align-middle text-2xl">
                                    {meta.addressShort.flag}
                                </span>
                                <span className="my-auto py-1 align-middle  font-body text-lg uppercase text-white">
                                    {" " + meta.addressShort.location}
                                </span>
                            </div>
                            <div className="ml-auto w-fit bg-green-500 p-1 font-body text-lg font-semibold uppercase text-white">
                                {asset.listingBadge}
                            </div>
                        </div>
                        <div className="mt-auto w-fit bg-white px-1 font-header text-2xl font-bold text-violet-500">
                            {meta.propTitle + " " + asset.assetClassLabel}
                        </div>
                        <div className="mt-2 flex w-fit flex-row gap-2 font-body">
                            {asset.tags.map((item, index) => (
                                <div
                                    key={index}
                                    className="bg-black px-1 text-white"
                                >
                                    {item}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="mt-auto flex w-full flex-col font-body text-white">
                        <div className="mb-1 flex w-full text-sm font-light">
                            {asset.highlightsShort}
                        </div>
                        <div className="flex flex-row">
                            <div className="flex w-fit grow flex-col">
                                {oppData.map(
                                    ({ priceLabel, priceLabelLong, priceAmount }, index) => (
                                        <div
                                            className="flex flex-row"
                                            key={index}
                                        >
                                            <div className="flex w-fit">
                                                {priceLabelLong ?? priceLabel}
                                            </div>
                                            <div className="ml-auto flex w-fit font-semibold">
                                                {priceAmount}
                                            </div>
                                        </div>
                                    ),
                                )}
                            </div>
                        </div>
                    </div>

                    <div className="absolute left-0 top-0 z-10 h-3/4 w-full rounded-t-md overflow-hidden">
                        <Image
                            className="transition-transform group-hover:scale-105"
                            src={meta.featuredImage}
                            alt={meta.propTitle}
                            fill
                            sizes="20vw"
                            style={{
                                objectFit: "cover",
                                objectPosition: "center",
                            }}
                        />
                        <div className="relative z-20 flex h-full w-full bg-black bg-opacity-10 transition-colors group-hover:bg-opacity-30">
                            <div className="m-auto flex h-fit w-fit shrink origin-right bg-white p-1 font-body text-violet-500 opacity-0 transition-opacity group-hover:opacity-100">
                                <Button
                                    label="More Details"
                                    border
                                    clickHandler={() => {
                                        return;
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    );
};

export default AssetCardShort;
