/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Wave0 from "../illustrations/Wave0";

type Props = {
    hideWave?: boolean;
};

const Motto: React.FC<Props> = ({ hideWave }) => {
    return (
        <div className="relative h-fit w-full bg-black py-10 text-white">
            {!hideWave && (
                <div className="absolute left-0 top-[25rem] z-[2] h-full w-[300%] scale-y-[-1] opacity-50 lg:w-full">
                    <Wave0 />
                </div>
            )}
            <div className="mx-auto flex h-full w-full max-w-7xl flex-col space-y-5">
                <div className="z-10  mx-auto flex w-fit text-center font-header text-3xl font-[600]">
                    Quorum. Together as one.
                </div>
            </div>
        </div>
    );
};

export default Motto;
