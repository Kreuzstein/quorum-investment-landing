/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";

const GlobeFallback: React.FC = () => {
  return (
    <div>
      <Image
        src="/globe-placeholder.png"
        height={550}
        width={550}
        alt="Our partner network around the globe"
        style={{
            height: "auto",
            minWidth: "500px",
        }}
      />
    </div>
  );
};

export default GlobeFallback;
