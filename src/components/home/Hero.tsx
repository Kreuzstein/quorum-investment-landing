/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";

import Button from "../atoms/Button";

import Stonks from "../illustrations/Stonks";
import Wave0 from "../illustrations/Wave0";
import AssetCardShort from "../common/AssetCardShort";

const Hero: React.FC = () => {
  // Photo by <a href="https://unsplash.com/@macmuse?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Todd Carter</a> on <a href="https://unsplash.com/photos/brown-concrete-building-near-green-grass-field-during-daytime--J8_vXF8bpc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  return (
    <div className="relative h-[48rem] w-full bg-no-repeat">
      <Image
        src="/hero.jpg"
        placeholder="blur"
        blurDataURL="/hero-placeholder.jpg"
        alt="Hero Image"
        sizes="100vw"
        fill
        priority
        style={{
          objectFit: "cover", // This will have the same effect as bg-cover
          objectPosition: "top", // This will have the same effect as bg-top
        }}
      />
      <div className="relative h-full w-full bg-black bg-opacity-50">
        <div className="absolute left-0 top-0 z-10 h-full w-[300%] lg:w-full">
          <Wave0 />
        </div>
        <div className="absolute left-[10%] top-[10rem] z-30 h-[20%] space-y-5 font-body text-6xl font-[700] text-white lg:top-[20rem]">
          <p>Esteemed estates.</p>
          <p>Discerning investors.</p>
          <p className="h-[4.5rem] w-fit bg-white pt-1 text-violet-500">
            Together.
          </p>
          {/* <p className="h-[5rem] bg-gradient-to-br from-gold-1000 from-15% via-green-1000 to-violet-1000 to-70% bg-clip-text text-transparent">
            Together.
          </p> */}
          <p className="max-w-2xl font-body text-2xl font-[500]">
            We facilitate forging connections between investors and upscale,
            high potential properties.
          </p>
          <div className="h-[3rem] font-header text-black">
            {/* <Button label="Explore QSI" fill /> */}
          </div>
        </div>
        <div className="absolute left-[55%] top-[15rem] z-20 hidden h-[20rem] w-[20rem] lg:flex">
          <Stonks />
        </div>
        <div className="absolute left-[58%] top-[18rem] z-[25] hidden h-[23rem] w-[30rem] lg:flex">
          <AssetCardShort />
        </div>
      </div>
    </div>
  );
};

export default Hero;
