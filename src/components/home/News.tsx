/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
const News: React.FC = () => {
    return (
      <div className="h-fit w-full bg-black text-white py-10">
        <div className="mx-auto flex h-full w-full max-w-7xl flex-col space-y-5">
          <div className="mx-auto flex w-fit text-center font-header text-3xl font-[600]">
            News & Insight
          </div>
          <div className="mx-auto w-fit font-body text-xl font-[400]">
            Established as an innovative technology firm, Quorum Group started its
            journey serving the finance and hospitality sectors. Our venture in
            real estate investment advisory took flight in 2023 when we were
            called upon by a client to assist with financial forecasting and sales
            pitching for a hotel property. The successful execution and
            deliverance transformed into a continuous pursuit, leading us to
            create a dedicated division for this service: Quorum Strategic
            Investments. We are more than just a technology company - we are a
            partner in navigating the complex world of real estate investment.
          </div>
        </div>
      </div>
    );
  };
  
  export default News;
  