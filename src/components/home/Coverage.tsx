/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { Suspense, lazy, useState } from "react";
import dynamic from "next/dynamic";
import { ErrorBoundary } from "react-error-boundary";

import GlobeFallback from "./GlobeFallback";
const Globe = dynamic(() => import("./Globe.jsx"), {
    loading: () => <GlobeFallback />,
    ssr: false,
});

import Wave1 from "../illustrations/Wave1";

import { capitalise, rgbToHex } from "~/utils/common";

const markerLocations = {
    clients: "clients",
    investors: "investors",
    partners: "partners",
    all: "all",
} as const;
type Marker = (typeof markerLocations)[keyof typeof markerLocations];

const Coverage: React.FC = () => {
    type Location = { location: [number, number]; size: number };
    const clients: Location[] = [
        { location: [56, -2], size: 0.05 }, // Glasgow
        { location: [57, 24], size: 0.05 }, // Riga
        { location: [53, 0], size: 0.05 }, // London
        { location: [45, 9], size: 0.05 }, // Milan
        { location: [1, 104], size: 0.05 }, // Singapore
        { location: [-32, 116], size: 0.05 }, // Perth
        { location: [60, 23], size: 0.05 }, // Helsinki
        { location: [-34, 151], size: 0.05 }, // Sydney
        { location: [42, 45], size: 0.05 }, // Tbilisi
    ];
    const investors: Location[] = [
        { location: [53, 0], size: 0.05 }, // London
        { location: [60, 23], size: 0.05 }, // Helsinki
        { location: [33, 35], size: 0.05 }, // Tel Aviv
        { location: [25, 55], size: 0.05 }, // Dubai
        { location: [22, 115], size: 0.05 }, // Hong Kong
        { location: [1, 104], size: 0.05 }, // Singapore
        { location: [14, 100], size: 0.05 }, // KT
        { location: [-34, 151], size: 0.05 }, // Sydney
        { location: [44, -80], size: 0.05 }, // Toronto
    ];
    const partners: Location[] = [
        { location: [53, 0], size: 0.05 }, // London
        { location: [60, 23], size: 0.05 }, // Helsinki
        { location: [45, 9], size: 0.05 }, // Milan
        { location: [48, 12], size: 0.05 }, // Munich
        { location: [41, -4], size: 0.05 }, // Madrid
        { location: [14, 100], size: 0.05 }, // KT
        { location: [-34, 151], size: 0.05 }, // Sydney
        { location: [44, -80], size: 0.05 }, // Toronto
        { location: [41, -74], size: 0.05 }, // NYC
        { location: [42, 45], size: 0.05 }, // Tbilisi
    ];
    const all: Location[] = Array.from(
        new Set([...investors, ...partners, ...clients]),
    );

    const markerColours: { [key in Marker]: [number, number, number] } = {
        clients: [1, 0.25, 0.25],
        investors: [1, 1, 0],
        partners: [195 / 255, 98 / 255, 1],
        all: [0, 1, 163 / 255],
    };
    const locations: { [key in Marker]: Location[] } = {
        clients,
        investors,
        partners,
        all,
    };
    const markerBodies: { [key in Marker]: string } = {
        clients: `Properties in Scotland, on the Baltic coast, and consulting clients in ${
            clients.length - 2
        } other locations.`,
        investors: `Investors from across ${investors.length} financial capitals, like London and Dubai.`,
        partners: `Professional network spanning Asia, Europe, Oceania, and North America.`,
        all: "",
    };
    const [selectedMarkers, setSelectedMarkers] = useState(
        markerLocations.investors as Marker,
    );

    return (
        <div className="relative flex h-fit w-full flex-col bg-black py-10">
            <div className="absolute left-[-5rem] top-[5rem] z-0 h-full w-[500%] scale-y-[-1] opacity-60 lg:w-full">
                <Wave1 />
            </div>
            <div className="z-10 mx-auto flex h-full w-full max-w-7xl flex-col space-y-5">
                <div className="mx-auto flex w-fit text-center font-header text-3xl font-[600] text-white">
                    Our Network
                </div>
                <div className="mx-auto flex w-fit px-5 font-body text-xl font-[400] text-white lg:px-0">
                    Join our network of investors, property managers, and realty
                    experts!
                </div>
                <div className="flex h-full w-full flex-col lg:flex-row">
                    <div className="flex w-full lg:w-[60%]">
                        <div className="mx-auto ">
                            <ErrorBoundary fallback={<GlobeFallback />}>
                                <Suspense fallback={<GlobeFallback />}>
                                    <Globe
                                        markers={locations[selectedMarkers]}
                                        markerColor={
                                            markerColours[selectedMarkers]
                                        }
                                    />
                                </Suspense>
                            </ErrorBoundary>
                        </div>
                    </div>
                    <div className="my-auto w-full text-white lg:flex lg:w-[40%] lg:flex-col">
                        {Object.values(markerLocations).map((item, index) => {
                            if (item === markerLocations.all) return;
                            return (
                                <MarkerGroupSelector
                                    label={item}
                                    selectedLabel={selectedMarkers}
                                    setSelectedLabel={setSelectedMarkers}
                                    colour={markerColours[item]}
                                    body={markerBodies[item]}
                                    key={index}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Coverage;

const MarkerGroupSelector: React.FC<{
    label: Marker;
    body: string;
    colour: [number, number, number];
    selectedLabel: string;
    setSelectedLabel: Function;
}> = (props) => {
    return (
        <div
            onMouseEnter={() => props.setSelectedLabel(props.label)}
            onClick={() => props.setSelectedLabel(props.label)}
            onMouseLeave={() => props.setSelectedLabel(markerLocations.all)}
            className="flex cursor-pointer flex-row px-6 py-3 transition-transform hover:scale-105"
        >
            <div className="flex-0 flex h-20 w-1/5">
                <div
                    className="m-auto h-5 w-5 rounded-full opacity-75"
                    style={{
                        background: rgbToHex(props.colour),
                    }}
                ></div>
            </div>
            <div className="mx-auto mb-auto flex flex-1 flex-col font-body lg:m-auto">
                <div className=" text-2xl ">{capitalise(props.label)}</div>
                <div className=" text-lg opacity-75">{props.body}</div>
            </div>
        </div>
    );
};
