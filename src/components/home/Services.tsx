/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { useState } from "react";

import Image from "next/image";
import Networking from "../illustrations/Networking";
import Pitch from "../illustrations/Pitch";
import Finance from "../illustrations/Finance";
import Feasibility from "../illustrations/Feasibility";
import Digital from "../illustrations/Digital";
import Plus from "../icons/Plus";
import Minus from "../icons/Minus";

import { find } from "~/utils/common";

type Service = {
  label: string;
  title: string;
  body: string[];
  illustration: React.FC;
  illustrationUrl: string;
};

const Services: React.FC = () => {
  const services: Service[] = [
    {
      label: "Strategic networking",
      title: "Leveraging Relationships to Drive Investments",
      body: [
        "Use our expansive network of industry contacts to your advantage. Whether you're looking to purchase or sell a property, we can facilitate introductions to potential partners, buyers, or investors.",
        "Our black books comprises real estate professionals, developers, investors, chartered surveyors, and lawyers. It's smooth sailing on your investment journey, from initial market analysis to final property transactions.",
        "Quorum Strategic Investments helps you build bridges that translate into success.",
      ],
      illustration: Networking,
      illustrationUrl: "networking.svg",
    },
    {
      label: "Pitch and BDM expertise",
      title: "Make your Vision Resonate",
      body: [
        "We are investors ourselves. We have the same concerns and priorities. There is no place for lackadaisical house-flipping in serious business.",
        "In other words, assets mustn't be merely sold to the first willing bidder. Each assets needs just the right kind of an investor. This is why it is paramount that the pitch accurately depicts the potential and value of the investment.",
        "Our Business Development advisors prepare compelling presentations, project briefs, and articulate development visions that accurately represent the investment opportunities.",
        "Quorum Strategic Investments' pitches don't waste time and don't spare detail.",
      ],
      illustration: Pitch,
      illustrationUrl: "pitch.svg",
    },
    {
      label: "Financial modelling",
      title: "Respecting the Bottom Line",
      body: [
        "Founded by an accountant, it's only natural for us to provide an in-depth analysis of historical profits, operational expenditure, and other financial aspects of your property. This process allows us to create a precise and well-grounded financial picture of the asset.",
        "And yet, the history isn't everything. We go beyond historical data and work diligently to draft robust financial models rooted in realistic projections. We evaluate possible revenue streams in conjunction with foreseeable costs to present a comprehensive view of the financial prospects.",
        "But getting the numbers is only part of the process. We also specialise in presenting this data in a clear, intuitive format.",
        "You deserver to make informed decisions about the future of your investments with QSI.",
      ],
      illustration: Finance,
      illustrationUrl: "finance.svg",
    },
    {
      label: "Feasibility studies",
      title: "Real Estate Warrants Real Research",
      body: [
        "In-depth insight is vital when undertaking any real estate venture. We conduct best-practices market research, delving into market trends, pricing, demand, and competition. We not only take a macro view of the market acknowledging the broader economic and industry trends, but also provide micro-level scrutiny specific to your property and locality.",
        "Our feasibility studies complement our market research. By evaluating the practicality of an investment, from logistics and legalities to costs and potential profits, we highlight the technical and economic viability of your venture, ensuring that every potential issue is identified and addressed before you commit.",
        "Quorum Strategic Investments equips you with the complete understanding of the market and the viability of the investment.",
      ],
      illustration: Feasibility,
      illustrationUrl: "feasibility.svg",
    },
    {
      label: "Digital transformation",
      title: "Sell Simpler — Sell Better",
      body: [
        "Quorum took root as a technology company for the finance and hospitality industries. Our core team is made of technologists. We know just how to harness the digital for better results.",
        "Our Virtual Tour services leverage state-of-the-art visualization technology. We create immersive 3D renderings, allowing potential investors to explore your property in striking detail from anywhere in the world. This opens up your property to a global audience and could significantly reduce the property's time on the market.",
        "We prepare roadmaps for business automation, integration of hotel/logistics management systems, and more to streamline and simplify operations in property management. We assess current systems, identify areas of inefficiency, and offer automation solutions.",
        "QSI supports you not just in the traditional realms of finance and real estate but also through digital transformation, enhancing and simplifying the property selling process.",
      ],
      illustration: Digital,
      illustrationUrl: "digital.svg",
    },
  ];
  const [selectedLabel, setSelectedLabel] = useState(services[0]!.label);
  const selectedService = find<Service>(services, { label: selectedLabel });
  return (
    <div className="h-full min-h-[40rem] w-full bg-white py-10 text-black">
      <div className="mx-auto flex h-full w-full max-w-7xl flex-col space-y-5">
        <div className="mx-auto flex w-fit text-center font-header text-3xl font-[600]">
          Our Services
        </div>
        <div className="mx-auto w-fit px-5 font-body text-xl font-[400] lg:px-0">
          We provide consultancy at the most important milestone of the asset&apos;s
          life cycle:{" "}
          <span className="font-[700] text-violet-500">the Exit</span>.
        </div>
        <div className="z-10 flex h-fit w-full flex-row lg:space-x-5">
          <div className="flex w-full flex-col px-5 lg:w-1/3 lg:px-0">
            {services.map((item, index) => {
              return (
                <ServiceOption
                  label={item.label}
                  selectedService={selectedService}
                  key={index}
                  setSelectedLabel={setSelectedLabel}
                />
              );
            })}
          </div>
          <div className="relative z-0 hidden w-full lg:block lg:h-[19rem] lg:w-2/3 ">
            <ServiceDesc
              illustration={selectedService?.illustration}
              title={selectedService?.title}
              body={selectedService?.body}
              illustrationUrl={selectedService?.illustrationUrl}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const ServiceOption: React.FC<{
  label: string;
  selectedService?: Service;
  setSelectedLabel: Function;
}> = (props) => {
  if (!props.selectedService) return <></>;
  const selected = props.label === props.selectedService.label;
  const selectedClass = selected
    ? "flex mx-auto lg:ml-auto lg:mr-0 from-50% lg:from-30% bg-gradient-to-tr lg:bg-gradient-to-r from-transparent to-violet-500 text-violet-500 transition-transform"
    : "flex mx-auto lg:ml-auto lg:mr-0 hover:scale-110 transition-transform hover:text-violet-500";
  return (
    <>
      <div
        className={selectedClass}
        onClick={() => props.setSelectedLabel(props.label)}
      >
        <div className="my-1 ml-auto mr-1 flex cursor-pointer space-x-2 bg-white py-5 pr-5 font-header text-xl font-[600] lg:space-x-0">
          <div>{props.label}</div>
          {selected ? (
            <div className="m-auto lg:hidden">
              <Minus />
            </div>
          ) : (
            <div className="m-auto lg:hidden">
              <Plus />
            </div>
          )}
        </div>
      </div>
      {selected ? (
        <div className="relative z-0 block h-full w-full text-black lg:hidden">
          <ServiceDesc
            illustration={props.selectedService.illustration}
            title={props.selectedService.title}
            body={props.selectedService.body}
            illustrationUrl={props.selectedService.illustrationUrl}
          />
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

const ServiceDesc: React.FC<{
  illustration?: React.FC;
  title?: string;
  body?: string[];
  illustrationUrl?: string;
}> = (props) => {
  return (
    <div className="relative z-10 flex from-transparent from-60% to-violet-500 lg:bg-gradient-to-tl">
      <div className="relative z-20 ml-1 mt-1 flex w-full flex-col bg-white py-5 pl-5">
        {/* backdrop */}
        <div className="absolute bottom-0 right-0 z-30 opacity-50">
          <div className="block lg:hidden">
            {!!props?.illustration ? <props.illustration /> : <></>}
          </div>
          <div className="hidden lg:block">
            <Image
              src={props.illustrationUrl || ""}
              height={300}
              width={300}
              alt={props.title || ""}
              style={{
                width: "auto", // this is to preserve the aspect ratio
              }}
            />
          </div>
        </div>

        {/* texts */}
        <div className="relative z-50 flex flex-col">
          <div className="mb-3 font-body text-2xl font-[600]">
            {props?.title}
          </div>
          <div className="">
            {!!props?.body ? (
              props.body.map((item, index) => {
                return (
                  <p key={index} className="mb-3 font-body text-lg">
                    {item}
                  </p>
                );
              })
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Services;
