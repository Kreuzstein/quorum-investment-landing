/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
const About: React.FC = () => {
    return (
        <div className="h-fit w-full bg-white py-10 text-black">
            <div className="mx-auto flex h-full w-full max-w-7xl flex-col space-y-5">
                <div className="mx-auto flex w-fit text-center font-header text-3xl font-[600]">
                    About QSI
                </div>
                <div className="mx-auto w-fit px-5 font-body text-xl font-[400] lg:px-0">
                    Established as an innovative technology firm, Quorum Group
                    started its journey serving the finance and hospitality
                    sectors. Our venture in real estate investment advisory took
                    flight in 2023 when we were called upon by a client to
                    assist with financial forecasting and sales pitching for a
                    hotel property. The successful execution and deliverance
                    transformed into a continuous pursuit, leading us to create
                    a dedicated division for this service: Quorum Strategic
                    Investments. We are more than just a technology company - we
                    are a partner in navigating the complex world of real estate
                    investment.
                </div>
                <div className="mx-auto w-fit px-5 font-body text-xl font-[400] lg:px-0">
                    Today, QSI is a premier consultancy firm renowned for our
                    strategic expertise in facilitating the sale and management
                    of commercial assets as well as offering residency by
                    investment services. Our robust portfolio embraces a suite
                    of services including property listing, asset management
                    advisory, and valuation services, tailored to optimize
                    investment performance. At the core of QSI's operations lies
                    a commitment to providing expert legal, financial, and
                    strategic guidance, ensuring a seamless experience for
                    clients worldwide. We empower investors with access to a
                    handpicked selection of lucrative real estate deals,
                    in-depth market analysis, and bespoke investment strategies.
                    With a reputation for excellence, innovation, and
                    personalized service, QSI is your trusted ally in navigating
                    the complexities of the global real estate marketplace.
                </div>
            </div>
        </div>
    );
};

export default About;
