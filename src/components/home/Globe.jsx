// @ts-nocheck
import createGlobe from "cobe";
import { useEffect, useRef } from "react";

// Based on https://github.com/shuding/cobe
const Globe = ({ markers, markerColor }) => {
  const canvasRef = useRef();

  useEffect(() => {
    let phi = 180;
    const globe = createGlobe(canvasRef.current, {
      devicePixelRatio: 2,
      width: 600 * 2,
      height: 600 * 2,
      phi: 0,
      theta: -50,
      dark: 1,
      diffuse: 1.2,
      mapSamples: 18000,
      mapBrightness: 6,
      baseColor: [0.3, 0.3, 0.3],
      markerColor,
      glowColor: [1, 1, 1],
      markers,
      onRender: (state) => {
        // Called on every animation frame.
        // `state` will be an empty object, return updated params.
        state.phi = phi;
        phi -= 0.01;
        if (phi <= -360) phi = 180;
      },
    });

    return () => {
      globe.destroy();
    };
  }, [markers, markerColor]);

  return (
    <canvas
      ref={canvasRef}
      style={{ width: 600, height: 600, maxWidth: "100%", aspectRatio: 1 }}
    />
  );
};

export default Globe;
