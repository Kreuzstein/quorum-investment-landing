/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Button from "../atoms/Button";

import { animateScroll } from 'react-scroll';

const Footer: React.FC = () => {   
    const scrollToTop = () => {
        animateScroll.scrollTo(
            0,
            {
                duration: 500,
                smooth: true,
            }
        )
    } 
  return (
    <div className="my-6 flex h-full w-full flex-row bg-black font-body text-white">
      <div className="my-auto flex px-10">
        © 2019-{new Date().getFullYear()} Quorum IO Ltd
      </div>
      <div className="my-auto ml-auto flex px-10 text-green-1000">
        <Button label="Back to top ↑" clickHandler={() => scrollToTop()}/>
      </div>
    </div>
  );
};

export default Footer;
