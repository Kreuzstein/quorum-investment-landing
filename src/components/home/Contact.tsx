/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";
import Button from "../atoms/Button";

import { ElementType } from "~/utils/common";

export const contacts = [
    {
        title: "Interested in selling your assets?",
        name: "Oscar Paskevics",
        image: "/oscar-paskevics.jpg",
        email: "oscar@quorum.ltd",
        locale: "56 Shoreditch High St (The Tea Building), London, UK",
        role: "Head of asset sales | Financial advisory | QSI",
        meeting: "https://calendar.app.google/BgCzxAUZ46QJ4Pwb9",
        linkedin: "https://uk.linkedin.com/in/oscar-paskevics-234389156",
    },
    {
        title: "Looking for investment opportunities?",
        name: "Anton Kreuzstein",
        image: "/anton-kreuzstein.jpg",
        email: "anton@quorum.ltd",
        locale: "39 Marasi Drive (The Prime Tower), Dubai, UAE",
        role: "Client strategy | Valuations | QSI",
        meeting: "https://calendar.app.google/v5v1ovLGY685HFjp7",
        linkedin: "https://uk.linkedin.com/in/kreuzstein",
    },
];
export type ContactName = "oscar" | "anton";
type Contact = ElementType<typeof contacts>;

type Props = {
    blurb?: string;
    header?: string;
};

const defaultBlurb =
    "Contact one of our experts and find out how our team can help.";

const Contact: React.FC<Props> = ({ blurb, header }) => {
    return (
        <div className="h-fit w-full bg-white py-10 pb-20 text-black">
            <div className="mx-auto flex h-full w-full max-w-7xl flex-col space-y-5">
                <div className="z-10 mx-auto flex w-fit text-center font-header text-3xl font-[600]">
                    {header ?? "Let's Talk"}
                </div>
                <div className="z-10 mx-auto w-fit max-w-4xl px-5 pb-5 text-center font-body text-xl font-[400] lg:px-0">
                    {blurb ?? defaultBlurb}
                </div>
                <div className="z-20 mx-auto flex w-full flex-col space-y-10 px-5 lg:flex-row lg:space-x-[10%] lg:space-y-0">
                    {contacts.map((item, index) => {
                        return <ContactCard contact={item} key={index} />;
                    })}
                </div>
            </div>
        </div>
    );
};
export default Contact;

const ContactCard: React.FC<{ contact: Contact }> = ({ contact }) => {
    return (
        <div
            className={
                "flex h-full w-full flex-col gap-2 bg-white text-black" +
                " " +
                "border-4 border-double border-violet-1000 px-5 py-3 shadow-2xl transition-colors"
            }
        >
            <div className="flex flex-row gap-3">
                <div className="relative h-20 w-20 rounded-full border-4 border-violet-500">
                    <Image
                        src={contact.image}
                        alt={contact.name}
                        fill
                        sizes="20vw"
                        style={{
                            objectFit: "cover", // This will have the same effect as bg-cover
                            objectPosition: "center", // This will have the same effect as bg-top
                            borderRadius: "9999px",
                        }}
                    />
                </div>
                <div className="flex w-3/5 flex-col font-body text-sm">
                    <div className="font-lg w-fit bg-violet-500 px-1 text-lg font-semibold text-white">
                        {contact.name}
                    </div>
                    <div className="font-semibold">{contact.role}</div>
                    <div className="text-xs">{contact.locale}</div>
                    <a
                        href={`mailto:${contact.email}`}
                        className="font-semibold text-violet-500 underline"
                    >
                        {contact.email}
                    </a>
                </div>
            </div>
            <a
                href={contact.meeting}
                target="_blank"
                className="flex w-full font-body text-white children:mx-auto children:w-full children:bg-violet-500 children:py-2 children:hover:scale-105"
            >
                <Button label="BOOK A CALL" clickHandler={() => {}} />
            </a>
        </div>
    );
};

const ContactCardOld: React.FC<{ contact: Contact }> = ({ contact }) => {
    return (
        <div className="flex h-full w-full bg-gradient-to-tl from-transparent from-60% to-violet-500 font-body lg:h-[15rem] lg:w-1/3">
            <div className="ml-1 mt-1 flex w-full flex-row space-x-3 bg-white px-5 py-5">
                <div className="min-w-1/3 w-fit">
                    <div className="relative h-20 w-20 rounded-full border-4 border-violet-500">
                        <Image
                            src={contact.image}
                            alt={contact.name}
                            fill
                            sizes="20vw"
                            style={{
                                objectFit: "cover", // This will have the same effect as bg-cover
                                objectPosition: "center", // This will have the same effect as bg-top
                                borderRadius: "9999px",
                            }}
                        />
                    </div>
                </div>
                <div className="flex w-2/3 flex-col space-y-1">
                    <div className="w-fit bg-violet-500 px-1 text-xl font-[700] text-white">
                        {contact.name}
                    </div>
                    <div className="text-sm font-[600]">{contact.role}</div>
                    <div className="text-sm">{contact.locale}</div>
                    <div className="text-sm">
                        Email:{" "}
                        <a
                            href={`mailto:${contact.email}`}
                            className="font-[600] text-violet-500 underline"
                        >
                            {contact.email}
                        </a>
                    </div>
                    <div className="pt-4 text-violet-500">
                        <a href={contact.meeting} target="_blank">
                            <Button
                                label="Book a Call"
                                border
                                clickHandler={() => {
                                    return;
                                }}
                            />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};
