/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Link from "next/link";

import Chevron from "../icons/Chevron";
import Header2 from "./Header2";
import { IllustratedBlockContent } from "~/types/content";
import Tag from "./Tag";

type Props = {
    content: IllustratedBlockContent;
    illustrationPosition: "left" | "right";
};

// TODO add bakcdrop handling for the illstrations on mobile

const IllustratedBlock: React.FC<Props> = ({
    content,
    illustrationPosition,
}) => {
    return (
        <div className="relative flex flex-row gap-x-5 px-2">
            {illustrationPosition === "left" && (
                <div className="hidden scale-[0.9] md:block">
                    <content.illustration />
                </div>
            )}
            <div className="my-auto flex flex-col">
                {content.tag && <Tag label={content.tag} />}
                <Header2 title={content.header} />
                <h3 className="font-body text-2xl font-light">
                    {content.subtitle}
                </h3>
                <p className="my-6 font-body text-lg">{content.body}</p>
                {content.cta ? (
                    <Link href={content.ctaHref}>
                        <button className="group ml-auto flex w-fit flex-row text-2xl font-[600] text-violet-500 hover:underline">
                            <span className="py-auto ml-0 h-fit">
                                {content.ctaLabel}
                            </span>
                            <span className="my-auto h-fit rotate-180 scale-125 transition-transform group-hover:translate-x-1">
                                <Chevron />
                            </span>
                        </button>
                    </Link>
                ) : (
                    <></>
                )}
            </div>
            {illustrationPosition === "right" && (
                <div className="hidden scale-[0.9] md:block">
                    <content.illustration />
                </div>
            )}
        </div>
    );
};

export default IllustratedBlock;
