/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
export type AccordionItem = {
    title: string;
    content: string;
    defaultOpen?: boolean;
};

type Props = {
    data: AccordionItem[];
    accordionId: string;
};

const Accordion: React.FC<Props> = ({ data, accordionId }) => {
    return (
        <div className="w-full pr-10 children:children:pl-0">
            {data.map(({ title, content, defaultOpen }, index) => {
                return (
                    <Item
                        key={index}
                        title={title}
                        content={content}
                        defaultOpen={!!defaultOpen}
                        accordionId={accordionId}
                    />
                );
            })}
        </div>
    );
};

export default Accordion;

const Item: React.FC<AccordionItem & { accordionId: string }> = ({
    title,
    content,
    defaultOpen,
    accordionId,
}) => {
    return (
        <div className="collapse-arrow collapse bg-white">
            <input
                type="radio"
                name={accordionId}
                defaultChecked={!!defaultOpen}
            />
            <div className="collapse-title text-xl font-medium">{title}</div>
            <div className="collapse-content">
                <p>{content}</p>
            </div>
        </div>
    );
};
