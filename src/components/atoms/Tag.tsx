/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
type Props = {
    label: string;
};

const Tag: React.FC<Props> = ({ label }) => {
    return (
        <p className="w-fit bg-violet-500 px-1 text-sm font-semibold uppercase text-white">
            {label}
        </p>
    );
};

export default Tag;
