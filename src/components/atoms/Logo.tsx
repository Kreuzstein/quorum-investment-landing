/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import LogoIcon from "../icons/LogoIcon";
import Image from "next/image";

const Logo: React.FC = () => {
    return (
        <div className="flex h-20 flex-row space-x-1 p-3 text-white">
            <div
                className="flex-0 flex h-[3.5rem] w-[3.5rem] scale-[0.7]"
            >
                <LogoIcon />
            </div>
            <div className="flex-0 m-auto hidden lg:flex h-fit max-h-[3.25rem] w-fit max-w-[18rem]">
                <Image
                    src={"/logotype.png"}
                    alt="Quorum Strategic Investments"
                    width={306}
                    height={56}
                    style={{
                        width: "auto", // this is to preserve the aspect ratio
                    }}
                />
            </div>
        </div>
    );
};

export default Logo;
