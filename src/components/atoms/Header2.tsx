/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
type Props = {
    title: string;
};

const Header2: React.FC<Props> = ({ title }) => {
    return <h2 className="font-header text-3xl font-[600]">{title}</h2>;
};

export default Header2;
