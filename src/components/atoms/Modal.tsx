/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { type ReactNode } from "react";

type Props = {
    open: boolean;
    exiting: boolean;
    children: ReactNode;
    close: () => void;
    toggleExitingAnimation?: () => void;
};

// TODO make exiting optional and fix enter/exiting animation

const Modal: React.FC<Props> = ({
    open: opened,
    exiting,
    children,
    close,
    toggleExitingAnimation,
}) => {
    const handleClose = !!toggleExitingAnimation
        ? async function () {
              toggleExitingAnimation();
              await new Promise((r) => setTimeout(r, 250));
              close();
              toggleExitingAnimation();
              return;
          }
        : close;
    if (!opened) return <></>;
    return (
        <div
            className={
                // "fixed block top-0 left-0 h-screen w-screen bg-black z-[999]" +
                "modal max-w-screen min-h-screen overflow-y-scroll" +
                " " +
                exiting
                    ? "modal-bg-exit"
                    : "modal-bg-enter"
            }
        >
            <div
                className="fixed z-[999] flex h-full w-full bg-black bg-opacity-30"
                onClick={handleClose}
            >
                {children}
            </div>
        </div>
    );
};

export default Modal;
