/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
type Props = {
    label: string,
    border?: boolean,
    fill?: boolean,
    clickHandler: Function,
};

const Button: React.FC<Props> = (props) => {
    const buttonClass = 
        "font-[700] text-lg px-4 py-2 h-full hover:scale-110 transition-transform " + 
        (!!props.border ? "border-current border-4 " : "border-transparent ") +
        (!!props.fill ? "bg-gradient-to-br from-gold-1000 from-15% to-70% via-green-1000 to-violet-1000 " : "");
    return (
        <button className={buttonClass} onClick={() => props.clickHandler()}>
            {props.label}
        </button>
    )
};

export default Button;
