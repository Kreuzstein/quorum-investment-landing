/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import { Filter } from "~/types/properties";
import Chevron from "../icons/Chevron";
import Check from "../icons/Check";

type Props = {
    filter: Filter | undefined;
    selectOption: (optionValue: string) => void;
    deselectOption: (optionValue: string) => void;
};

const MultiSelect: React.FC<Props> = ({
    filter,
    selectOption,
    deselectOption,
}) => {
    if (!filter) return <></>;
    if (filter.type !== "multi") return <></>;
    return (
        <div className="dropdown dropdown-hover w-full rounded-lg border-[1px] border-primary children:m-0">
            <div
                tabIndex={0}
                role="button"
                className="group btn m-1 w-full bg-transparent text-white"
            >
                <span>{filter.label}</span>
                <span className="ml-auto rotate-[-90deg] transition-transform group-hover:rotate-[-90deg]">
                    <Chevron />
                </span>
            </div>
            <ul
                tabIndex={0}
                className="dropdown-content z-[1] w-full rounded-box bg-base-100 p-2 shadow"
            >
                {filter.options.map(({ label, value }, index) => {
                    const selected = filter.selectedOptions.includes(value);
                    return (
                        <li
                            key={index}
                            className="btn btn-ghost inline-flex w-full flex-row p-3"
                            onClick={() => {
                                selected
                                    ? deselectOption(value)
                                    : selectOption(value);
                                return;
                            }}
                        >
                            <span
                                className={
                                    "w-fit" +
                                    " " +
                                    (selected ? "" : "opacity-0")
                                }
                            >
                                <Check />
                            </span>
                            <span className="mr-auto w-fit max-w-[80%] overflow-clip truncate md:text-xs">{label}</span>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};

export default MultiSelect;
