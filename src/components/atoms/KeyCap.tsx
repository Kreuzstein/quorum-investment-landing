/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { useEffect, type ReactNode, useState } from "react";
import { checkForDesktopUseragent } from "~/utils/common";

type Props = {
    children: ReactNode;
    palette?: "light" | "dark";
};
const KeyCap: React.FC<Props> = ({ children, palette }) => {
    const [onDesktop, setDesktop] = useState(false);
    useEffect(() => {
        checkForDesktopUseragent(setDesktop);
        return () => {};
    }, []);

    if (!onDesktop) return <></>;

    return (
        <kbd
            className={
                "!font-regular my-auto inline-block min-w-[1.3rem] rounded-md border-2 !border-opacity-40 px-[0.15rem] pb-[0.05rem] pt-[0.15rem] !text-center !text-xs" +
                " " +
                (palette === "light"
                    ? "!border-gray-800 !bg-gray-200 !text-gray-800"
                    : "!border-gray-200 !bg-gray-800 !text-white")
            }
        >
            {children}
        </kbd>
    );
};

export default KeyCap;
