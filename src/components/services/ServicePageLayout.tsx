/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */

import { useEffect, useState } from "react";
import Head from "next/head";

import { type LandingPageHero } from "~/types/content";
import Contact, { type ContactName } from "../home/Contact";
import Header from "../common/Header";
import ContentHero from "../common/ContentHero";
import StickyCta from "../common/StickyCta";
import Motto from "../home/Motto";
import Footer from "../home/Footer";
import FixedCta from "../common/FixedCta";
import { titleBuilder } from "~/utils/common";

type Props = {
    pageTitle: string;
    hero: LandingPageHero;
    children?: React.ReactNode;
    showMotto: boolean;
    hideMottoWave?: boolean;
    showContacts?: boolean;
    contactsHeader?: string;
    contactsBlurb?: string;
    ctaContact: ContactName;
    ctaTitle: string;
    ctaLabel: string;
    ctaChildren?: React.ReactNode;
};

const ServicePageLayout: React.FC<Props> = (props) => {
    const [showFixedCta, setShowFixedCta] = useState(false);

    useEffect(() => {
        const controlNavbar = () => {
            if (typeof window !== "undefined") {
                if (window.scrollY >= Math.round(window.screen.height / 3)) {
                    setShowFixedCta(true);
                } else {
                    setShowFixedCta(false);
                }
            }
        };

        if (typeof window !== "undefined") {
            window.addEventListener("scroll", controlNavbar);

            // cleanup function
            return () => {
                window.removeEventListener("scroll", controlNavbar);
            };
        }
    }, []);

    return (
        <>
            <Head>
                <title>{titleBuilder(props.pageTitle)}</title>
                <meta
                    name="description"
                    content="Asset Sale | Asset Management | Commercial Investment | EU Residency By Investment | Forging connections between investors and upscale properties."
                />
            </Head>
            <main className="flex min-h-screen flex-col overflow-x-clip bg-white">
                <Header view="content" />
                <ContentHero hero={props.hero} />
                <article
                    id="container"
                    className="mx-auto my-10 flex h-fit min-h-[20rem] w-full max-w-7xl flex-col gap-y-5 font-body text-black"
                >
                    <div className="flex flex-row">
                        {/* Content */}
                        <section className="w-full space-y-5 lg:w-3/4">
                            {props.children}
                        </section>

                        {/* Contact Card */}
                        <section className="hidden w-1/4 min-w-[21rem] lg:block">
                            <StickyCta
                                contact={props.ctaContact}
                                title={props.ctaTitle}
                                ctaLabel={props.ctaLabel}
                            >
                                {props.ctaChildren}
                            </StickyCta>
                        </section>
                    </div>
                </article>
                <div className="z-30">
                    {!!props.showMotto && (
                        <Motto hideWave={!!props.hideMottoWave} />
                    )}
                    {!!props.showContacts && (
                        <Contact blurb={props.contactsBlurb} header={props.contactsHeader} />
                    )}
                    <footer className="z-30 flex bg-black">
                        <Footer />
                    </footer>
                </div>
                <section className="z-20 block lg:hidden">
                    {showFixedCta && (
                        <FixedCta
                            contact={props.ctaContact}
                            ctaLabel={props.ctaLabel}
                        />
                    )}
                </section>
            </main>
        </>
    );
};

export default ServicePageLayout;
