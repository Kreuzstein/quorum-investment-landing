import Image, { StaticImageData } from "next/image";
import { useAlbumStore } from "~/stores/useAlbumStore";

type Props = {
    images: (StaticImageData | string)[]
}

const Collage: React.FC<Props> = ({ images }) => {
    const store = useAlbumStore();
    return (
        <div className="grid h-fit w-full md:grid-cols-6 grid-cols-2 gap-2">
        {images.map((src, index) => {
            if (index >= 5) return;
            return (
                <div
                    key={index}
                    className={
                        "group w-full cursor-pointer overflow-clip" +
                        " " +
                        // for wide-screen and desktop
                        (index === 0 || index === 1
                            ? "md:col-span-3 md:row-span-2 md:h-[20rem]"
                            : "md:col-span-2 md:h-[10rem]") + 
                        " " + 
                        // for mobile
                        (index === 0 
                            ? "col-span-2 row-span-2 h-[20rem]"
                            : "col-span-1 row-span-1 h-[10rem]")
                    }
                    onClick={() => store.openCloseup(index)}
                >
                    <div className="relative flex h-full w-full transition-transform group-hover:scale-110">
                        <Image
                            src={src}
                            alt={index.toString()}
                            sizes="40vw"
                            style={{
                                objectFit: "cover",
                                objectPosition: "center",
                                minHeight: "100%",
                            }}
                        />
                        <div className="absolute z-20 h-full w-full bg-black bg-opacity-0 transition-colors group-hover:bg-opacity-20" />
                    </div>
                </div>
            );
        })}
    </div>
    )
};

export default Collage;
