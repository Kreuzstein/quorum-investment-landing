/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import GalleryIcon from "../icons/Gallery";
import Gallery from "./Gallery";
import Collage from "./Collage";
import { albumViewOptions, useAlbumStore } from "~/stores/useAlbumStore";
import { useEffect } from "react";
import { StaticImageData } from "next/image";

// TODO make viw all and other buttons bigger for touch screen

type Props = {
    images: (StaticImageData | string)[]
};

const Album: React.FC<Props> = ({ images }) => {
    const store = useAlbumStore();
    // useEffect(() => {
    //     console.log((document.getElementById(store.galleryModalId) as HTMLDialogElement)?.open);
    // }, []);

    return (
        <div className="relative m-2 flex flex-col gap-2">
            <Collage images={images}/>
            {/* Show all photos button for wide screen & desktop */}
            <div className="group absolute bottom-5 right-5 z-40 hidden cursor-pointer flex-row gap-1 bg-white p-1 transition-colors hover:bg-violet-500 md:block">
                <div className="flex flex-row border-2 border-violet-500 bg-white p-1 text-violet-500 transition-colors group-hover:bg-violet-500 group-hover:text-white">
                    <div className="scale-[.8]">
                        <GalleryIcon />
                    </div>
                    <button
                        className="font-body font-semibold"
                        onClick={() => {
                            store.showGalleryModal();
                        }}
                    >
                        Show All Photos
                    </button>
                </div>
            </div>
            {/* Show all photos button for mobile */}
            <div
                className="group col-span-1 w-full cursor-pointer rounded-md border-2 border-gray-400 border-opacity-40 font-body hover:bg-gray-100 md:hidden"
                onClick={() => {
                    store.showGalleryModal();
                }}
            >
                <div className="flex w-fit mx-auto flex-row gap-2 p-5 align-middle text-xl font-semibold transition-colors">
                    <div className="my-auto flex scale-[1.2]">
                        <GalleryIcon />
                    </div>
                    <div className="mr-auto flex">Show All Photos</div>
                </div>
            </div>
            {/* <div
                className={
                    "modal h-screen w-screen" +
                    " " +
                    ((store.view === albumViewOptions.closeup)
                        ? "bg-black"
                        : "bg-white")
                }
            >
                <Gallery />
                <form className="fixed right-0 top-0">
                    <button>close</button>
                </form>
            </div> */}
        </div>
    );
};

export default Album;
