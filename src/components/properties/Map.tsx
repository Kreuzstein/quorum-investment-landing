/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { type LegacyRef, useRef, useEffect, MutableRefObject } from "react";
import { type GetServerSideProps, type GetStaticProps } from "next/types";

import { trpc } from "~/utils/trpc";

import {
    Map as MapLibre,
    Marker,
    NavigationControl,
    type StyleSpecification,
} from "maplibre-gl";
import "maplibre-gl/dist/maplibre-gl.css";

type Props = {
    x: number;
    y: number;
    // style: any | string | StyleSpecification;
};

const MAP_STYLE_ID = "winter-v2";

// TODO add loader behind

const Map: React.FC<Props> = ({ x, y }) => {
    const mapRef: MutableRefObject<null | MapLibre> = useRef(null);
    const containerRef = useRef(null);

    const {
        data: style,
        isLoading,
        error,
    } = trpc.helpers.getMapStyle.useQuery({ styleId: MAP_STYLE_ID });

    useEffect(() => {
        if (containerRef.current && !mapRef.current && !isLoading) {
            mapRef.current = new MapLibre({
                container: containerRef.current,
                style,
                center: [x, y],
                zoom: 14,
                boxZoom: false,
                hash: false,
            })
            mapRef.current.scrollZoom.disable();
            const nav = new NavigationControl({
                showZoom: true,
                showCompass: false,
            })
            mapRef.current.addControl(nav, "top-left");
            new Marker({ color: "#8b5cf6"}).setLngLat([x, y]).addTo(mapRef.current);


            // const el = document.createElement('div');
            // el.className = 'marker';
            // el.style.backgroundColor = "black";
            // el.style.width = `50px`;
            // el.style.height = `50px`;
    
            // el.addEventListener('click', () => {
            //     window.alert("foo");
            // });
    
            // new Marker({ element: el }).setLngLat([24.120383, 56.95043173818938]).addTo(mapRef.current);
        }

        return () => {
            if (mapRef.current) {
                mapRef.current.remove();
                mapRef.current = null;
            }
        };
    }, [x, y, isLoading]);

    return (
        <div className="relative h-full w-full overflow-clip">
            <div ref={containerRef} className="h-full w-full absolute" />
        </div>
    );
};

export default Map;
