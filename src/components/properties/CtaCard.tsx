/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import Image from "next/image";
import Link from "next/link";
import { createElement, useEffect } from "react";

import { addMonths, endOfMonth, format } from "date-fns";

import { Opportunity } from "~/types/properties";
import { useShareStore } from "~/stores/useShareStore";
import { albumViewOptions, useAlbumStore } from "~/stores/useAlbumStore";
import KeyCap from "../atoms/KeyCap";
import Button from "../atoms/Button";
import Clock from "../icons/Clock";
import Printer from "../icons/Printer";
import Share from "../icons/Share";
import { ContactName, contacts } from "../home/Contact";
import { userPlaceholder } from "~/utils/imageData";

const oppData: Opportunity[] = [
    {
        title: "Sale opportunity",
        bullets: [
            {
                label: "Total area",
                value: "4,695 m²",
            },
        ],
        priceLabel: "Asking price",
        priceAmount: "€ 7,000,000",
        dealType: "sale",
        residenceOption: "eu-pr",
        priceBracket: "5M-to-10M",
        blurb: "",
    },
    {
        title: "Investment opportunity",
        details: ["Stake in the managing company"],
        bullets: [
            {
                label: "Returns range",
                value: "5% ROI",
            },
        ],
        priceLabel: "Starting at",
        priceAmount: "€ 200,000",
        tags: ["EU Residency"],
        dealType: "stake",
        residenceOption: "eu-pr",
        priceBracket: "under-200K",
        blurb: "",
    },
];

const ctaMeta = {
    propTitle: "House of Benjamins",
    addressLong: "12 Krišjāņa Barona, Rīga",
    contact: "oscar",
};

const expiration = format(endOfMonth(addMonths(new Date(), 2)), "dd MMM yyyy");

// TODO list KYC partners and aprtner nerowtks -- fasnara royal mao toyota ollenpay metamap refinitiv danone easy group housing hand roonyx and that new firm
// TODO add hover tooltip with hotkey

export const ctaCardViewOptions = {
    default: "default",
    compact: "compact",
} as const;
type Props = {
    view?: (typeof ctaCardViewOptions)[keyof typeof ctaCardViewOptions];
    opportunities: Opportunity[]; 
    contactName: ContactName;
    propTitle: string;
    addressLong: string;
};

const CtaCard: React.FC<Props> = ({ view, opportunities, contactName, propTitle, addressLong }) => {
    const shareStore = useShareStore();
    const albumStore = useAlbumStore();

    if (!view) view = ctaCardViewOptions.default;
    const contactObj =
        contacts.filter(
            (c) => c.name.split(" ")[0]?.toLowerCase() === contactName,
        )[0] ?? contacts[0];

    if (!contactObj) return <></>;

    useEffect(() => {
        function handleKeyPress(event: any) {
            // don't check for hotkeys if any modal is opened
            if (
                shareStore.dialogOpened ||
                albumStore.view !== albumViewOptions.none
            )
                return;
            // open share dialog on pressing "S"
            if (event.keyCode === 83) {
                shareStore.open();
            }
            return;
        }
        // Attach event listener to window for key down events
        window.addEventListener("keydown", handleKeyPress);

        // Cleanup: remove event listener when the component unmounts
        return () => {
            window.removeEventListener("keydown", handleKeyPress);
        };
    }, [shareStore.dialogOpened, albumStore.view]);

    return (
        <div
            className={
                "flex h-full w-full flex-col gap-2 bg-white text-black" +
                " " +
                (view === ctaCardViewOptions.default &&
                    "border-4 border-double border-violet-1000 px-5 py-3 shadow-2xl transition-colors") +
                " " +
                (view === ctaCardViewOptions.compact && "mx-2 border-b-2 py-2")
            }
        >
            {view === ctaCardViewOptions.default ? (
                <div className="flex flex-col">
                    <div className="w-fit bg-violet-500 px-1 font-body text-xl font-[600] text-white">
                        {propTitle}
                    </div>
                    <div className="font-body text-sm font-light">
                        {addressLong}
                    </div>
                </div>
            ) : (
                <></>
            )}
            {opportunities.map((opp, index) => (
                <Opportunity key={index} opportunity={opp} />
            ))}
            <div className="flex w-full flex-row gap-3 font-body">
                <div className="relative my-auto flex h-16 w-16 rounded-full border-4 border-violet-500">
                    <Image
                        src={contactObj.image}
                        alt={contactObj.name}
                        fill
                        style={{
                            objectFit: "cover",
                            objectPosition: "center",
                            borderRadius: "9999px",
                        }}
                        placeholder={userPlaceholder}
                    />
                </div>
                <div className="flex flex-col text-sm w-3/4">
                    <div className="font-lg w-fit bg-violet-500 px-1 font-semibold text-white">
                        {contactObj.name}
                    </div>
                    <div className="font-semibold">{contactObj.role}</div>
                    <div className="text-xs">{contactObj.locale}</div>
                    <a
                        href={`mailto:${contactObj.email}`}
                        className="font-semibold text-violet-500 underline"
                    >
                        {contactObj.email}
                    </a>
                </div>
            </div>
            <a
                href={contactObj.meeting}
                target="_blank"
                className="flex w-full border-b-2 pb-2 font-body text-white children:mx-auto children:w-full children:bg-violet-500 children:py-2 children:hover:scale-105"
            >
                <Button label="ENQUIRE NOW" clickHandler={() => {}} />
            </a>
            <div className="mx-auto flex flex-row gap-2 font-body">
                <div className="flex text-violet-1000">
                    <Clock />
                </div>
                <div className="flex font-light">
                    Enquire before:{" "}
                    <span className="ml-1 font-normal">{expiration}</span>
                </div>
            </div>
            <div className="grid w-full grid-cols-2 divide-x-2 font-body children:py-2">
                {[
                    {
                        icon: Share,
                        label: "Share",
                        action: () => {
                            shareStore.open();
                        },
                        hotkey: <KeyCap palette="light">S</KeyCap>,
                    },
                    {
                        icon: Printer,
                        label: "Print",
                        action: () => {
                            window.print();
                        },
                        hotkey: (
                            <></>
                            // <div className="flex gap-[0.1rem]">
                            //     <KeyCap palette="light">⌘</KeyCap>
                            //     <KeyCap palette="light">P</KeyCap>
                            // </div>
                        ),
                    },
                ].map(({ icon, label, action, hotkey }, index) => (
                    <div
                        className="group col-span-1 w-full cursor-pointer"
                        key={index}
                        onClick={action}
                    >
                        <div className="mx-auto flex w-fit flex-row gap-2 transition-colors group-hover:text-violet-1000">
                            {createElement(icon)}
                            <div className="">{label}</div>
                            {/* {hotkey} */}
                        </div>
                    </div>
                ))}
            </div>
            <div className="mx-auto w-fit font-body text-xs font-thin children:mx-1 children:underline">
                <Link href="/about/terms-and-conditions" target="_blank">
                    Terms & Conditions
                </Link>
                and
                <Link href="/about/privacy-policy" target="_blank">
                    Privacy Policy
                </Link>
                apply.
            </div>
            {/* <div className="font-body text-xs font-thin">QSI will never share your data with 3rd party advertisers. <span className="underline cursor-pointer">Learn more.</span></div> */}
        </div>
    );
};

export default CtaCard;

const Opportunity: React.FC<{ opportunity: Opportunity }> = ({
    opportunity,
}) => {
    const { title, bullets, details, priceLabel, priceAmount, tags } =
        opportunity;
    return (
        <div className="flex flex-col border-b-2 pb-2">
            <div className="flex font-body text-lg font-bold">{title}</div>
            {tags?.map((value, index) => (
                <div
                    className="flex w-fit bg-yellow-500 px-1 font-body text-sm font-semibold uppercase text-white"
                    key={index}
                >
                    {value}
                </div>
            ))}
            {details?.map((row, index) => (
                <div className="flex font-body" key={index}>
                    {row}
                </div>
            ))}
            {bullets?.map(({ label, value }, index) => (
                <div className="flex flex-row gap-3" key={index}>
                    <div className="mt-auto flex font-body">{label}</div>
                    <div className="ml-auto flex font-header text-lg font-semibold">
                        {value}
                    </div>
                </div>
            ))}
            <div className="flex flex-row gap-3">
                <div className="mt-auto flex font-body">{priceLabel}</div>
                <div className="ml-auto flex font-header text-xl font-semibold text-violet-1000">
                    {priceAmount}
                </div>
            </div>
        </div>
    );
};
