/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>.
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import {
    type KeyboardEvent,
    useEffect,
    type MouseEvent,
    useState,
    TouchEvent,
} from "react";
import Image, { type StaticImageData } from "next/image";
import { albumViewOptions, useAlbumStore } from "~/stores/useAlbumStore";
import Chevron from "../icons/Chevron";
import { noClickPropagationWrapper } from "~/utils/common";
import KeyCap from "../atoms/KeyCap";
import Cross from "../icons/Cross";
import Share from "../icons/Share";
import { useShareStore } from "~/stores/useShareStore";

// TODO add gallery state into the URL as QS
// TODO don't deform arrow controls
// TODO add hover tooltip for left and right arrow
// TODO fix loader bug

type Props = {
    images: (StaticImageData | string)[];
};

const Gallery: React.FC<Props> = ({ images }) => {
    const store = useAlbumStore();
    const shareStore = useShareStore();

    const [swiping, setSwiping] = useState<
        "takeToLeft" | "stashOnLeft" | "takeToRight" | "stashOnRight" | "none"
    >("none");
    const [hideStash, setHideStash] = useState(false);

    const showPrevImage = async (_event?: MouseEvent<HTMLDivElement>) => {
        let prevImage = store.image - 1;
        if (prevImage < 0) {
            prevImage = images.length - 1;
        }
        store.selectImage(prevImage);
        return;
    };
    // separate function for swipe to add extra time buffers
    const swipeToPrevImage = async (_event?: MouseEvent<HTMLDivElement>) => {
        // swipe the image to the left
        setSwiping("takeToLeft");
        // calc prev image index
        let prevImage = store.image - 1;
        if (prevImage < 0) {
            prevImage = images.length - 1;
        }
        // wait for swiping animation to play
        await new Promise((r) => setTimeout(r, 250));
        // hide the stashed image
        setHideStash(true);
        // transition the stashed image right and buffer to prevent next/image being too clever with its caching
        setSwiping("stashOnRight");
        await new Promise((r) => setTimeout(r, 25));
        // load new image
        store.selectImage(prevImage);
        // buffer time to avoid flickering and making sure the image is on the correct side
        await new Promise((r) => setTimeout(r, 150));
        // unhide the image
        setHideStash(false);
        // reset stashing style so that the image transitions back to the middle
        setSwiping("none");
        return;
    };
    
    const showNextImage = async (_event?: MouseEvent<HTMLDivElement>) => {
        let nextImage = store.image + 1;
        if (nextImage >= images.length) {
            nextImage = 0;
        }
        store.selectImage(nextImage);
        return;
    };
    // separate function for swipe to add extra time buffers
    const swipeToNextImage = async (_event?: MouseEvent<HTMLDivElement>) => {
        // swipe the image to the right
        setSwiping("takeToRight");
        // calc next image index
        let nextImage = store.image + 1;
        if (nextImage >= images.length) {
            nextImage = 0;
        }
        // wait for swiping animation to play
        await new Promise((r) => setTimeout(r, 250));
        // hide the stashed image
        setHideStash(true);
        // transition the stashed image left and buffer to prevent next/image being too clever with its caching
        setSwiping("stashOnLeft");
        await new Promise((r) => setTimeout(r, 25));
        // load new image
        store.selectImage(nextImage);
        // buffer time to avoid flickering and making sure the image is on the correct side
        await new Promise((r) => setTimeout(r, 150));
        // unhide the image
        setHideStash(false);
        // reset stashing style so that the image transitions back to the middle
        setSwiping("none");
        return;
    };

    // clock handlers
    const leftArrowClick = noClickPropagationWrapper(showPrevImage);
    const rightArrowClick = noClickPropagationWrapper(showNextImage);

    // Keyboard prompts
    useEffect(() => {
        function handleKeyPress(event: any) {
            // If share modal opened on top, don't trigger hotkey events
            if (shareStore.dialogOpened) return;
            switch (event.keyCode) {
                case 37: // left arrow key code
                    showPrevImage(event);
                    break;
                case 39: // right arrow key code
                    showNextImage(event);
                    break;
                case 27: // Esc key code
                    store.view === albumViewOptions.closeup
                        ? store.setView(albumViewOptions.list)
                        : store.reset();
                    break;
                case 83: // S key code
                    shareStore.open();
                    break;
                default:
                    break;
            }
            return;
        }

        // Attach event listener to window for key down events
        window.addEventListener("keydown", handleKeyPress);

        // Cleanup: remove event listener when the component unmounts
        return () => {
            window.removeEventListener("keydown", handleKeyPress);
        };
    }, [showPrevImage, showNextImage, store.view]);

    // Swipe handlers
    const [touchStart, setTouchStart] = useState<number | undefined>(undefined);
    const [touchEnd, setTouchEnd] = useState<number | undefined>(undefined);
    const minSwipeDistance = 50;
    const touchStartHandler = (event: TouchEvent<HTMLDivElement>) => {
        setTouchEnd(undefined); // otherwise the swipe is fired even with usual touch events
        setTouchStart(event?.targetTouches[0]?.clientX);
    };
    const touchMoveHandler = (event: TouchEvent<HTMLDivElement>) =>
        setTouchEnd(event?.targetTouches[0]?.clientX);
    const touchEndHandler = () => {
        if (!touchStart || !touchEnd) return;
        const distance = touchStart - touchEnd;
        const isLeftSwipe = distance > minSwipeDistance;
        const isRightSwipe = distance < -minSwipeDistance;
        if (isRightSwipe) swipeToPrevImage();
        if (isLeftSwipe) swipeToNextImage();
        return;
    };

    if (store.view === albumViewOptions.closeup && images[store.image]) {
        return (
            <div
                className="relative z-[999] flex h-full w-full bg-black transition-colors"
                onClick={(event) => {
                    event.stopPropagation();
                    store.showGalleryModal();
                    return;
                }}
            >
                <div className="overflow-none flex h-screen w-full flex-col">
                    <div className="flex w-full flex-row font-header text-white">
                        <div className="my-10 flex w-fit md:w-1/5">
                            <div
                                className="group col-span-1 ml-10 mr-auto cursor-pointer"
                                onClick={() => {}}
                            >
                                <div className="mx-auto flex w-fit flex-row gap-2 font-body transition-colors group-hover:text-violet-1000">
                                    <Chevron />
                                    <div className="">Back</div>
                                    {shareStore.dialogOpened ? (
                                        <></>
                                    ) : (
                                        <KeyCap>Esc</KeyCap>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="mx-auto my-10 flex">
                            {(store.image + 1).toString() +
                                " / " +
                                images.length.toString()}
                        </div>
                        <div className="my-10 flex w-fit md:w-1/5">
                            <div
                                className="group col-span-1 ml-auto mr-10 cursor-pointer"
                                onClick={(event) => {
                                    event.stopPropagation();
                                    shareStore.open();
                                    return;
                                }}
                            >
                                <div className="mx-auto flex w-fit flex-row gap-2 font-body transition-colors group-hover:text-violet-1000">
                                    <Share />
                                    <div className="">Share</div>
                                    <KeyCap>S</KeyCap>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex h-4/5 w-full flex-row">
                        <div
                            className="my-auto ml-10 hidden h-10 w-10 cursor-pointer rounded-full border-2 border-white text-white transition-colors hover:border-violet-1000 hover:text-violet-1000 children:m-auto md:flex"
                            onClick={leftArrowClick}
                        >
                            <Chevron />
                        </div>
                        <div
                            className={
                                "relative mx-auto flex h-full max-w-7xl transition-transform" +
                                " " +
                                (swiping === "takeToLeft"
                                    ? "translate-x-[100vw]"
                                    : "") +
                                " " +
                                (swiping === "takeToRight"
                                    ? "translate-x-[-100vw]"
                                    : "") +
                                (swiping === "stashOnLeft"
                                    ? "translate-x-[100vw] !transition-none"
                                    : "") +
                                " " +
                                (swiping === "stashOnRight"
                                    ? "translate-x-[-100vw] !transition-none"
                                    : "") +
                                " " +
                                (hideStash && "opacity-10")
                            }
                            onTouchStart={touchStartHandler}
                            onTouchMove={touchMoveHandler}
                            onTouchEnd={touchEndHandler}
                        >
                            <Image
                                src={
                                    images[store.image] as
                                        | StaticImageData
                                        | string
                                }
                                alt={store.image.toString()}
                                sizes="100vw"
                                style={{
                                    objectFit: "contain",
                                    objectPosition: "center",
                                }}
                                className="fade-in z-[1000] w-fit"
                                onClick={(event) => {
                                    event.stopPropagation();
                                    return;
                                }}
                            />
                            <div className="absolute z-[999] lg:flex hidden h-full w-full">
                                <div
                                    // bg-gradient-to-r from-gold-1000 via-violet-500 to-green-1000 bg-repeat-x
                                    className="progress m-auto h-3 w-1/2 rounded-full border-2 border-violet-500"
                                />
                            </div>
                        </div>
                        <div
                            className="my-auto mr-10 hidden h-10 w-10 cursor-pointer rounded-full border-2 border-white text-white transition-colors hover:border-violet-1000 hover:text-violet-1000 children:m-auto children:rotate-180 md:flex"
                            onClick={rightArrowClick}
                        >
                            <Chevron />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    if (store.view === albumViewOptions.list) {
        return (
            <div className="relative z-[999] flex h-full w-full flex-col overflow-y-scroll bg-violet-950 transition-colors">
                <div className="fixed z-[1000] flex w-full flex-row bg-inherit font-header text-white lg:bg-transparent">
                    <div className="my-10 flex w-1/2">
                        <div
                            className="group col-span-1 ml-10 mr-auto cursor-pointer"
                            onClick={() => {}}
                        >
                            <div className="mx-auto flex w-fit flex-row gap-2 font-body transition-colors group-hover:text-violet-1000">
                                <Cross />
                                <div className="">Close</div>
                                {shareStore.dialogOpened ? (
                                    <></>
                                ) : (
                                    <KeyCap>Esc</KeyCap>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="my-10 flex w-1/2">
                        <div
                            className="group col-span-1 ml-auto mr-10 cursor-pointer"
                            onClick={(event) => {
                                event.stopPropagation();
                                shareStore.open();
                                return;
                            }}
                        >
                            <div className="mx-auto flex w-fit flex-row gap-2 font-body transition-colors group-hover:text-violet-1000">
                                <Share />
                                <div className="">Share</div>
                                <KeyCap>S</KeyCap>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mx-auto my-20 grid h-fit max-w-7xl grid-cols-6 gap-2">
                    {images.map((src, index) => {
                        return (
                            <div
                                key={index}
                                className={
                                    "group w-full cursor-pointer overflow-clip" +
                                    " " +
                                    // for wide-screen and desktop
                                    (index % 5 === 0 || index % 5 === 1
                                        ? "md:col-span-3 md:row-span-2 md:h-[30rem]"
                                        : "md:col-span-2 md:row-span-1 md:h-[15rem]") +
                                    " " +
                                    // fpr mobile
                                    (index % 3 === 0
                                        ? "col-span-6 row-span-2 h-[16rem]"
                                        : "col-span-3 h-[8rem]")
                                }
                                onClick={(event) => {
                                    event.stopPropagation();
                                    store.openCloseup(index);
                                    return;
                                }}
                            >
                                <div className="relative flex h-full w-full transition-transform group-hover:scale-110">
                                    <Image
                                        src={src}
                                        alt={index.toString()}
                                        sizes="40vw"
                                        style={{
                                            objectFit: "cover",
                                            objectPosition: "center",
                                            minHeight: "100%"
                                        }}
                                    />
                                    <div className="absolute z-20 h-full w-full bg-black bg-opacity-0 transition-colors group-hover:bg-opacity-20" />
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
    return <></>;
};

export default Gallery;
