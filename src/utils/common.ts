/*!
 * Copyright (c) 2023-2024 QUORUM IO LIMITED and Anton Kreuzstein <anton@quorum.ltd>. 
 *
 * This file is part of Quorum Strategic Investment website project (https://gitlab.com/Kreuzstein/quorum-investment-landing).
 *
 * All Rights Reserved. For full license terms, see LICENCE.md in the root directory.
 */
import { type MouseEvent } from "react";

/**
 * Returns the first element of the list which matches the predicate, or undefined if no element matches.
 * Only checks equality of primitive types, i.e. equality of values that are arrays or objects is not reliable. 
 *  
 * @param {Array} array of objects T 
 * @param {Object} predicate a partial object T
 * @returns {Object} an item of from the array
 */
export const find = <T>(array: T[], predicate: Partial<T>): T | undefined => {
    for (const item of array) {
        let matchCount = 0;
        for (const key in predicate) {
            if (item[key] == predicate[key]) matchCount++;
        }
        if (matchCount === Object.keys(predicate).length) return item;
    }
    return undefined;
};

export const rgbToHex = (rgb: [number, number, number]): string => {
    let hex = "#";
    for (let i = 0; i < 3; i++) {
        const val = rgb[i];
        const part = (val !== null && val !== undefined) ? Math.round(val * 255).toString(16) : "00";
        hex += ("00" + part).slice(-2);
    }
    return hex;
};

export const capitalise = (str: string): string => str.charAt(0).toUpperCase() + str.slice(1);

export type ElementType<T extends ReadonlyArray<unknown>> = T extends ReadonlyArray<infer ElementType> ? ElementType : never;

export const disableBodyScroll = (): void => {
    document.documentElement.style.setProperty("--modal-overflow-behav", "hidden");
    return;
}

export const enableBodyScroll = (): void => {
    document.documentElement.style.setProperty("--modal-overflow-behav", "scroll");
    return;
}

type MouseEventHandlingFunction<ElementType> = (event: MouseEvent<ElementType>) => void;

export const noClickPropagationWrapper = <ElementType>(
    innerFunction: MouseEventHandlingFunction<ElementType>
): MouseEventHandlingFunction<ElementType> => {
    return (event: MouseEvent<ElementType>) => {
        event.stopPropagation();
        innerFunction(event);
        return;
    };
};

export const writeToClipboard = async (data: string): Promise<void> => {
    await navigator.clipboard.writeText(data);
    return;
};

export function encodeRFC3986URIComponent(str: string): string {
    return encodeURIComponent(str).replace(
        /[!'()*]/g,
        (c) => `%${c.charCodeAt(0).toString(16).toUpperCase()}`,
    );
};

export function checkForDesktopUseragent(action: (onDesktop: boolean) => any): void {
    // ts lsp doesn't know about https://developer.mozilla.org/en-US/docs/Web/API/Navigator/userAgentData
    const platform = (
        (navigator as any)?.userAgentData?.platform || navigator.platform
    )?.toLowerCase();
    if (
        platform.startsWith("win") ||
        platform.startsWith("mac") ||
        platform.startsWith("linux")
    ) {
        action(true);
    } else {
        action(false);
    }
    return;
};

import { scroller } from "react-scroll";
export function scrollToElement<T extends string = string>(elementName: T): void {
    scroller.scrollTo(elementName, {
        duration: 500,
        delay: 100,
        smooth: true,
        offset: -100,
    });
    return;
};

export function titleBuilder(title: string): string {
    return `${title} | Quorum Strategic Investments`;
}
