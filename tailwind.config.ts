import { type Config } from "tailwindcss";
import { fontFamily } from "tailwindcss/defaultTheme";
import plugin from "tailwindcss/plugin";

export default {
    content: ["./src/**/*.tsx"],
    theme: {
        extend: {
            fontFamily: {
                sans: ["var(--font-sans)", ...fontFamily.sans],
                // base: ["'Inter', sans-serif"],
                header: ["'Montserrat Alternates', sans-serif"],
                body: ["'Open Sans', sans-serif"],
            },
            backgroundImage: {
                "gradient-main":
                    "linear-gradient(91deg, #00FFA3 49.57%, #C362FF 94.97%)",
            },
            colors: {
                "green-1000": "rgba(0, 255, 163, 1)",
                "violet-1000": "#C362FF",
                "gold-1000": "#CBFF67",
                notwhite: "#e6e6e6",
            },
            screens: {
                "wider-than-header": "80rem",
            },
        },
    },
    plugins: [
        plugin(function ({ addVariant }) {
            addVariant("children", "&>*");
        }),
        require("daisyui"),
    ],
    daisyui: {
        logs: false,
    },
} satisfies Config;
